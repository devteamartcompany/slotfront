const fs = require('fs');
const path = require('path');
const walk = require('./walk');


const dirPath = path.join(__dirname, '..', 'src');
const indexPath = path.join(__dirname, '..', 'index.ts');

(() => {
    walk(dirPath, null, (err, files) => {
        if (err) throw err;

        let imports = '';
        let exports = '';

        files
            .filter((file) => ~fs.readFileSync(file, {encoding: 'utf-8'})
                .search(/export (class|interface|function|abstract|let|const)/))
            .forEach((file) => {
                const url = file.split('src/')[1].replace(/\.ts$/, '');
                const arr = url.split('/');
                const key = arr[arr.length - 1];

                imports += `import * as ${key} from './src/${url}';`;
                exports += key + ',';
            });

        const output = imports + `export default {${exports}}`;

        fs.writeFile(indexPath, output, (err) => {
            if (err) throw err;
            console.log('finish exports creator');
        })
    })
})();
