const fs = require('fs');

module.exports = function walkSync(dir, checkList) {
    let results = [];
    let list = fs.readdirSync(dir);
    list.forEach((file) => {
        file = dir + '/' + file;
        let stat = fs.statSync(file);
        if (stat && stat.isDirectory()) {
            results = results.concat(walkSync(file, checkList));
        } else {
            checkList.some((item) => ~file.indexOf(item)) && results.push(file);
        }
    });
    return results;
};