const fs = require('fs');
const path = require('path');

module.exports = function walk(dir, checkList, done) {
    let results = [];
    fs.readdir(dir, (err, list) => {
        if (err) return done(err);
        let pending = list.length;
        if (!pending) return done(null, results);
        list.forEach((file) => {
            file = path.resolve(dir, file);
            fs.stat(file, (err, stat) => {
                if (stat && stat.isDirectory()) {
                    walk(file, checkList, (err, res) => {
                        results = results.concat(res);
                        if (!--pending) done(null, results);
                    });
                } else {
                    (!checkList || checkList.some((item) => ~file.indexOf(item))) && results.push(file);
                    if (!--pending) done(null, results);
                }
            });
        });
    });
};