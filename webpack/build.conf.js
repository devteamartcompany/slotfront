const path = require('path');

const merge = require('webpack-merge');
const CompressionPlugin = require('compression-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const LinkPreparePlugin = require('./plugins/rel-prepare');
const imgMinify = require('./plugins/img-minify');

const baseConfig = require('./base.conf');

const buildWebpackConfig = merge(baseConfig, {
    output: {
        filename: '[name].[hash].js',
        path: process.env.NODE_ENV === 'production'
            ? baseConfig.externals.paths.buildProd
            : baseConfig.externals.paths.build,
    },
    plugins: [
        new CopyPlugin([
            {
                from: baseConfig.externals.paths.data,
                to: 'data'
            },
        ]),
        new MiniCssExtractPlugin({
            filename: '[name].[hash].css',
            chunkFilename: '[id].[hash].css',
        }),
        new HtmlWebpackPlugin({
            favicon: baseConfig.externals.favicon,
            title: baseConfig.externals.title,
            template: baseConfig.externals.paths.src + '/index.html',
            filename: 'index.html',
            minify: {
                collapseWhitespace: true,
                removeComments: true,
                minifyCSS: true,
            },
            inject: true,
        }),
        new LinkPreparePlugin({
            root: baseConfig.externals.paths.root,
        }),
        // process.env.NODE_ENV === 'production' && new CompressionPlugin({
        //     deleteOriginalAssets: true,
        //     filename: (info) => `${info.file}.gz`,
        //     // TODO: remove html after rework link-rel-maker
        //     exclude: /\.(html)$/
        // }),
        new CleanWebpackPlugin(),
    ].filter(Boolean),
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin({
                sourceMap: process.env.NODE_ENV === 'development',
                parallel: true,
                terserOptions: {
                    parse: {
                        ecma: 8,
                    },
                    compress: {
                        ecma: 5,
                        warnings: false,
                        comparisons: false,
                        inline: 2,
                    },
                    mangle: {
                        safari10: true,
                    },
                    output: {
                        ecma: 5,
                        comments: false,
                    },
                },
            }),
            new OptimizeCSSAssetsPlugin(),
        ],
        // splitChunks: {
        //     cacheGroups: {
        //         vendor: {
        //             name: 'vendors',
        //             test: /node_modules/,
        //             chunks: 'all',
        //             enforce: true
        //         },
        //         // styles: {
        //         //     name: 'styles',
        //         //     test: /\.css$/,
        //         //     chunks: 'all',
        //         //     enforce: true,
        //         // },
        //     },
        // },
        /* use for multiple chunks */
        runtimeChunk: 'single',
        splitChunks: {
            chunks: 'all',
            maxInitialRequests: 6,
            minSize: 30000,
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name(module) {
                        const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

                        return `npm.${packageName.replace('@', '')}`;
                    },
                },
            },
        },
    },
    devtool: 'source-map',
});

module.exports = Promise.resolve(buildWebpackConfig);
