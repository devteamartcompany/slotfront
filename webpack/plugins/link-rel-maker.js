const fs = require('fs');
const path = require('path');
const walk = require('../utils/walk');
const PATHS = require('../base.conf').externals.paths;

module.exports = class LinkRelMakerPlugin {
    constructor(options) {
        this.options = options;
    }

    apply(compiler) {
        // TODO: resolve async conflict with compress plugin. Must occur before compress plugin remove original files
        compiler.hooks.done.tap(
            'LinkRelMakerPlugin',
            (compilation) => {
                const bin = process.env.NODE_ENV === 'production' ? PATHS.buildProd : PATHS.build;

                let config = require(path.join(this.options.root, 'data/settings/assets_config')).groups;
                config = [...config.preload, ...config.initial];
                const urlList = config.map((item) => item.jsonUrl.split('/')[0]);
                const htmlPath = path.join(bin, 'index.html');

                walk(path.join(this.options.root, 'data'), urlList, (err, result) => {
                    if (err) throw err;
                    const linksStr = result.reduce((acc, item) => {
                        //\.(png|jpe?g)$/.test
                        let as = '';
                        switch (path.extname(item)) {
                            case '.png':
                            case '.jpeg':
                            case '.jpg':
                                as = 'image';
                                break;
                            case '.json':
                                as = 'fetch';
                                break;
                            case '.wav':
                            case '.mp3':
                                as = 'audio';
                                break;
                        }
                        acc += `<link rel="${this.options.rel}" as="${as}" href="${item.slice(this.options.root.length + 1)}">`;
                        return acc
                    }, '');

                    fs.readFile(htmlPath, {encoding: 'utf-8'}, (err, result) => {
                        if (err) throw err;
                        const outputArr = result.split('</head>');
                        outputArr[0] += linksStr;
                        const output = outputArr.join('</head>');

                        fs.writeFile(htmlPath, output, (err) => {
                            if (err) throw err;
                        })
                    })
                });

                // const urls = walkSync(path.join(this.options.root, 'data'), urlList);
                // const linksStr = urls.reduce((acc, item) => {
                //     acc += `<link rel="${this.options.rel}" href="${item.slice(this.options.root.length)}">`;
                //     return acc
                // }, '');
                //
                // const html = fs.readFileSync(htmlPath, {encoding: 'utf-8'});
                // const outputArr = html.split('</head>');
                // outputArr[0] += linksStr;
                // const output = outputArr.join('</head>');
                //
                // fs.writeFileSync(htmlPath, output);
            }
        );
    }
};