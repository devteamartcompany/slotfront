(function () {
    var assets/*--webpackInsertedVariable--*/;
    if (assets) {
        /**/// lang
        // /^(?|&)lang=[\w]{2}(&\?)$/
        var lang = 'en';
        location.search.slice(1).split('&').forEach(function (item) {
            var items = item.split('=');
            if (items[0] === 'lang') lang = items[1];
        });

        /**/// mob
        var device = (function () {
            var devices = ["Android", "Touch", "Mobile", "webOS", "iPhone", "iPad", "BlackBerry", "IEMobile", "Opera Mini"];
            for (var i = 0; i < devices.length; i++) {
                if (navigator.userAgent.match(devices[i])) {
                    return 'mobile';
                }
            }
            return 'desktop';
        })();

        /**/// dpi
        /*var size = screen.width * screen.height;
        var m = 800 * 600;
        var h = 1366 * 768;
        var x = 2560 * 1920;

        var dpi = (size >= m ? (size >= h ? (size >= x ? 'xh' : 'h') : 'm') : 'l') + 'dpi.';*/
        var dpi = 'hdpi.';

        /**/// insert links
        var fragment = document.createDocumentFragment();
        for (var i = 0; i < assets.length; i++) {
            var link = document.createElement('link');
            link.rel = 'preload';
            link.as = assets[i].as;
            if (assets[i].hasOwnProperty('platform')) {
                if (!~assets[i].platform.indexOf(device)) continue;
                lang = assets[i].isCommon ? "common" : lang;
                link.href = 'data/assets/' + lang + '/' + assets[i].url + '/atlas-' + dpi + assets[i].postfix
            } else {
                link.href = assets[i].url
            }
            fragment.appendChild(link);
        }
        document.head.appendChild(fragment);
    }

})()