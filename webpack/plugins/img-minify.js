const imagemin = require('imagemin');
const imageminPngquant = require('imagemin-pngquant');
const imageminJpegtran = require('imagemin-jpegtran');

module.exports = async (bufferData) => {
    return await imagemin.buffer(bufferData, {
        plugins: [
            imageminJpegtran(),
            imageminPngquant({
                speed: 1,
                strip: true,
                dithering: false,
                quality: [0.5, 1]
            })
        ]
    });
};
