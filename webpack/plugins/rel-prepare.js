const fs = require('fs');
const path = require('path');
const uglify = require('uglify-js');
const PATHS = require('../base.conf').externals.paths;

module.exports = class LinkRelMakerPlugin {
    constructor(options) {
        this.options = options;
    }

    apply(compiler) {
        // TODO: resolve async conflict with compress plugin. Must occur before compress plugin remove original files
        compiler.hooks.done.tap(
            'LinkPreparePlugin',
            (compilation) => {
                const bin = process.env.NODE_ENV === 'production' ? PATHS.buildProd : PATHS.build;
                const assetsPath = path.join(this.options.root, 'data', 'settings', 'assets_config.json');
                const soundsPath = path.join(this.options.root, 'data', 'settings', 'sound_config.json');

                let assets = fs.existsSync(assetsPath) ? require(assetsPath).groups : null;
                let sounds = fs.existsSync(soundsPath) ? require(soundsPath).groups : null;

                if (!(assets || sounds)) return;

                const htmlPath = path.join(bin, 'index.html');
                const jsPath = path.join(__dirname, 'html-link-maker.js');


                assets = assets
                    ? [...assets.preload, ...assets.initial].reduce(
                        (acc, item) => {
                            const base = {
                                url: item.jsonUrl.split('/')[0],
                                platform: item.platform,
                                isCommon: item.isCommon
                            };
                            return [
                                ...acc,
                                {...base, as: 'fetch', postfix: 'json'},
                                {...base, as: 'image', postfix: 'png'},
                            ]
                        }, [])
                    : [];

                sounds = sounds
                    ? [...sounds.preload, ...sounds.initial].map(({url}) => ({url, as: 'audio'}))
                    : [];

                fs.readFile(htmlPath, {encoding: 'utf-8'}, (err, html) => {
                    if (err) throw err;
                    fs.readFile(jsPath, {encoding: 'utf-8'}, (err, js) => {
                        if (err) throw err;
                        const {code} = uglify.minify(
                            js.replace('/*--webpackInsertedVariable--*/', '=' + JSON.stringify([...assets, ...sounds])),
                            {compress: {negate_iife: false}}
                        );

                        const newJs = '<script>' + code + '</script>';
                        const newHtml = html.indexOf('<script') > -1
                            ? html.replace('<script', newJs + '<script')
                            : html.replace('</body>', newJs + '</body>');

                        fs.writeFile(htmlPath, newHtml, (err) => {
                            if (err) throw err;
                        })
                    })
                })
            }
        );
    }
};