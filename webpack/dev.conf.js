const merge = require('webpack-merge');
const baseConfig = require('./base.conf');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');


const devWebpackConfig = merge(baseConfig, {
    devServer: {
        contentBase: [baseConfig.externals.paths.root, baseConfig.externals.paths.src],
        stats: 'minimal',
        host: '0.0.0.0',
        hot: true,
        overlay: {
            warnings: true,
            errors: true
        },
    },
    devtool: 'inline-source-map',
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
        }),
        new HtmlWebpackPlugin({
            favicon: baseConfig.externals.favicon,
            title: baseConfig.externals.title,
            template: baseConfig.externals.paths.src + '/index.html',
            filename: 'index.html',
        }),
    ]
});

module.exports = Promise.resolve(devWebpackConfig);