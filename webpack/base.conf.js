const fs = require('fs');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


const PATHS = {
    root: path.join(__dirname, '..'),
    src: path.join(__dirname, '..', 'src'),
    build: path.join(__dirname, '..', 'bin'),
    buildProd: path.join(__dirname, '..', 'bin_prod'),
    data: path.join(__dirname, '..', 'data'),
    favicon: path.join(__dirname, '..', 'data', 'favicon', 'favicon.ico'),
    packageJson: path.join(__dirname, '..', 'package.json'),
};

module.exports = {
    mode: process.env.NODE_ENV || 'development',
    entry: PATHS.src,
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loaders: ['ts-loader'],
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1
                        }
                    },
                    'postcss-loader',
                    'sass-loader',
                ],
            },
            // {
            //     test: /\.scss$/,
            //     loaders: ['style-loader', 'raw-loader', 'sass-loader']
            // },
            {
                test: /\.(png|jpg)$/,
                use: "file-loader?name=[path][name][hash].[ext]"
            },
            {
                test: /\.(woff2?|eot|ttf|otf|svg)$/,
                loader: 'file-loader',
                options: {
                    // url - filename in hash
                    // resPath - absolutely file name
                    // ctx - project root path
                    publicPath: (url, resPath, ctx) => {
                        if (path.extname(url) === '.svg' && !resPath.includes('fonts')) {
                            return;
                        }
                        return path.join('fonts', url)
                    },
                    outputPath: 'fonts',
                },
            },
            // {
            //     test: /\.(ogg|mp3|wav|mpe?g)$/i,
            //     loader: 'file-loader',
            //     options: {
            //         name: '[path][name].[ext]',
            //     },
            // },
            {
                enforce: 'pre',
                test: /\.js$/,
                loader: "source-map-loader"
            },
            {
                enforce: 'pre',
                test: /\.tsx?$/,
                loader: "source-map-loader"
            }
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js", '.css'],
        alias: {
            '~': 'src',
        }
    },
    externals: {
        pixi: "PIXI",
        paths: PATHS,
        favicon: fs.existsSync(PATHS.favicon) && PATHS.favicon,
        title: require(PATHS.packageJson).name
    },
};

