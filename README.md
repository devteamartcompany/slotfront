   # How to launch
to launch execute in terminal next command lines:
npm i
npm run start

in browser launch http://localhost:8080/ (or your current port from terminal);

    # Application architecture
Application is based on 
    FSM, (BaseState classes + CommonFSMInitializer)
    Command pattern (BaseActions classes)
    Event driven system (only BaseControllers, BaseActions, BaseMediators, BaseStates can use it)
    bind system (all class instances are created in bind system - it helps easily override any class)
    module systems (all class's instances are created in ApplicationModule classes - it helps easily switch on / off needed functionality)
    dynamic view factory (all views are created dynamically after parsing file view_config.json )
    view components (button, checkbox, label, bitmapLabel, etc)
    loading system (load assets, spineAnimations, sounds and bitmapFonts using 3 groups: preload, initial and lazy)
    server service
    
    # Particular game logic
PLease look at src/gameModules directory and src/states directory

