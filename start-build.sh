#!/bin/bash
echo "privet key_id"
cat ~/.ssh/id_rsa

echo "start-build"
npm install>npm-install.log
echo "start-build >> install"

npm audit>npm-audit.log
echo "start-build >> audit"

npm audit fix --force
npm audit>npm-audit-fix.log
echo "start-build >> audit-fix"

npm run build>npm-build.log
echo "start-build >> run build"

npm run prod>npm-build.log
echo "start-build >> run prod"

npm run start>npm-start.log
echo "start-build >> run start"