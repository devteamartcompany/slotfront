export const ConfigPathData = {
    LOADING_CONFIG_PATH: "./data/settings/assets_config.json",
    BITMAP_FONTS_CONFIG_PATH: "./data/settings/bitmap_fonts_config.json",
    VIEW_CONFIG_PATH: "./data/settings/view_config.json",
    SOUND_CONFIG_PATH: "./data/settings/sound_config.json",
    GAME_CONFIG_PATH: "./data/settings/game_config.json",
    GAME_PARAMS_PATH: "./data/settings/game_params.json",
    I18N_PREFIX_PATH: "./data/i18n/"
}