import {BaseState} from "../coreEngine/fsm/BaseState";
import {BaseAction} from "../coreEngine/fsm/BaseAction";
import {createInstance} from "../coreEngine/exports";
import {SerialAction} from "../coreEngine/fsm/SerialAction";
import {InfinitePlayGameAction} from "../gameModules/playGame/actions/InfinitePlayGameAction";

export class PlayGameState extends BaseState {
    public static ID: string = "PlayGameState"

    constructor(permittedStates: string[]) {
        super(PlayGameState.ID, permittedStates);
    }

    addActions(): BaseAction[] {
        return [
            createInstance(SerialAction, [
                createInstance(InfinitePlayGameAction),
            ])
        ];
    }

    getNextState(): string {
        return "";
    }
}