import {BaseState} from "../coreEngine/fsm/BaseState";
import {BaseAction} from "../coreEngine/fsm/BaseAction";
import {createInstance} from "../coreEngine/exports";
import {SerialAction} from "../coreEngine/fsm/SerialAction";
import {AddRootViewAction} from "../commonModules/rootView/actions/AddRootViewAction";
import {ShowGameLogoAction} from "../commonModules/rootView/actions/ShowGameLogoAction";
import {PlayGameState} from "./PlayGameState";
import {HideLoaderViewAction} from "../commonModules/preloader/actions/HideLoaderViewAction";

export class MainGameInitState extends BaseState {
    public static ID: string = "MainGameInitState"

    constructor(permittedStates: string[]) {
        super(MainGameInitState.ID, permittedStates)
    }

    addActions(): BaseAction[] {
        return [
            createInstance(SerialAction, [
                    createInstance(HideLoaderViewAction),
                    createInstance(ShowGameLogoAction),
                    createInstance(AddRootViewAction)
                ]
            )

        ];
    }

    getNextState(): string {
        return PlayGameState.ID;
    }
}