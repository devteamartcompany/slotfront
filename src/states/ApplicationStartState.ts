import {BaseState} from "../coreEngine/fsm/BaseState";
import {createInstance} from "../coreEngine/exports";
import {BaseAction} from "../coreEngine/fsm/BaseAction";
import {SerialAction} from "../coreEngine/fsm/SerialAction";
import {LoadAssetPreloadAction} from "../commonModules/loading/actions/LoadAssetPreloadAction";
import {LoadSoundPreloadAction} from "../commonModules/loading/actions/LoadSoundPreloadAction";
import {LoadAssetInitAction} from "../commonModules/loading/actions/LoadAssetInitAction";
import {LoadSoundInitAction} from "../commonModules/loading/actions/LoadSoundInitAction";
import {LoadAssetLazyAction} from "../commonModules/loading/actions/LoadAssetLazyAction";
import {LoadSoundLazyAction} from "../commonModules/loading/actions/LoadSoundLazyAction";
import {LoadConfigAction} from "../commonModules/loading/actions/LoadConfigAction";
import {InitAppAction} from "../commonModules/loading/actions/InitAppAction";
import {DynamicViewCreateAction} from "../commonModules/loading/actions/DynamicViewCreateAction";
import {LoadLocalizationAction} from "../commonModules/loading/actions/LoadLocalizationAction";
import {MainGameInitState} from "./MainGameInitState";
import {LoadBitmapFontsPreloadAction} from "../commonModules/loading/actions/LoadBitmapFontsPreloadAction";
import {LoadBitmapFontsInitAction} from "../commonModules/loading/actions/LoadBitmapFontsInitAction";
import {LoadBitmapFontsLazyAction} from "../commonModules/loading/actions/LoadBitmapFontsLazyAction";
import {InitLoaderAction} from "../commonModules/loading/actions/InitLoaderAction";
import {ShowProgressBarAction} from "../commonModules/loading/actions/ShowProgressBarAction";
import {LoadWebFontsAction} from "../commonModules/loading/actions/LoadWebFontsAction";


export class ApplicationStartState extends BaseState {

    public static ID: string = "ApplicationStartState"

    constructor(permittedStates: string[]) {
        super(ApplicationStartState.ID, permittedStates)
    }

    public getNextState(): string {
        return MainGameInitState.ID;
    }

    addActions(): BaseAction[] {
        return [
            createInstance(SerialAction, [
                    createInstance(LoadWebFontsAction),
                    createInstance(LoadLocalizationAction),
                    createInstance(LoadConfigAction),
                    createInstance(DynamicViewCreateAction),
                    createInstance(InitAppAction),

                    createInstance(LoadBitmapFontsPreloadAction),
                    createInstance(LoadAssetPreloadAction),
                    createInstance(LoadSoundPreloadAction),
                    createInstance(InitLoaderAction),

                    createInstance(ShowProgressBarAction),
                    createInstance(LoadBitmapFontsInitAction),
                    createInstance(LoadAssetInitAction),
                    createInstance(LoadSoundInitAction),

                    createInstance(LoadAssetLazyAction),
                    createInstance(LoadBitmapFontsLazyAction),
                    createInstance(LoadSoundLazyAction),

                ]
            )

        ];
    }
}