import {BaseView} from "../../../coreEngine/baseClasses/BaseView";
import {BaseComponent} from "../../../coreEngine/viewComponents/interfaces/BaseComponent";
import {BaseLabel} from "../../../coreEngine/viewComponents/BaseLabel";
import {BaseButton} from "../../../coreEngine/viewComponents/BaseButton";
import {LobbyLoginEvents} from "../events/LobbyLoginEvents";
import {BaseInputLabel} from "../../../coreEngine/viewComponents/inputLabel/views/BaseInputLabel";
import {CustomInputEvents} from "../../../coreEngine/viewComponents/inputLabel/events/CustomInputEvents";
import {ILoginViewData} from "../interfaces/ILoginViewData";

export class LobbyLoginView extends BaseView {

    protected loginEmailInputValueLabel: BaseInputLabel;
    protected loginPasswordInputValueLabel: BaseInputLabel;
    protected newEmailInputValue: BaseInputLabel;
    protected newNameInputValue: BaseInputLabel;
    protected confirmPasswordInputValue: BaseInputLabel;
    protected newPasswordInputValue: BaseInputLabel;

    protected loginButton: BaseButton;
    protected createNewUserButton: BaseButton;

    protected emailIsWrongLabel: BaseLabel;
    protected passIsLessT8Label: BaseLabel;
    protected passDoesntMatchLabel: BaseLabel;
    protected loginPassEmailWrongLabel: BaseLabel;

    protected inputLabels: BaseInputLabel[] = [];

    protected initSourceView(source: BaseComponent): void {
        super.initSourceView(source);
        this.loginEmailInputValueLabel = this.addInputLabel("loginEmailInputValue");
        this.loginPasswordInputValueLabel = this.addInputLabel("loginPasswordInputValue");
        this.newEmailInputValue = this.addInputLabel("newEmailInputValue");
        this.newNameInputValue = this.addInputLabel("newNameInputValue");
        this.confirmPasswordInputValue = this.addInputLabel("confirmPasswordInputValue");
        this.newPasswordInputValue = this.addInputLabel("newPasswordInputValue");

        this.loginButton = this.source.getChildByName("loginButton");
        this.loginButton.on("clicked", this.onLoginButtonClicked.bind(this));
        this.createNewUserButton = this.source.getChildByName("createNewUserButton");
        this.createNewUserButton.on("clicked", this.onCreateNewUserButtonClicked.bind(this));

        this.emailIsWrongLabel = this.source.getChildByName("emailIsWrongLabel");
        this.passIsLessT8Label = this.source.getChildByName("passIsLessT8Label");
        this.passDoesntMatchLabel = this.source.getChildByName("passDoesntMatchLabel");
        this.loginPassEmailWrongLabel = this.source.getChildByName("loginPassEmailWrongLabel");

        this.newEmailInputValue.on(CustomInputEvents.CHANGE_CUSTOM_FIELD_VALUE, this.onNewEmailInput.bind(this));
        this.loginEmailInputValueLabel.on(CustomInputEvents.CHANGE_CUSTOM_FIELD_VALUE, this.onLoginEmailInput.bind(this));

    }

    public updateUI(data): void {
        Object.keys(data).forEach((key) => {
            if (data[key].enabled !== undefined && this[key]) {
                (this[key] as any).enable(data[key].enabled);
            }
            if (data[key].visible !== undefined && this[key]) {
                (this[key] as any).setVisible(data[key].visible);
            }
        });
    }

    hide(): void {
        this.inputLabels.forEach(label => {
            label.setVisible(false);
        })
        super.hide();
    }

    show(): void {
        this.inputLabels.forEach(label => {
            label.setVisible(true);
        })
        super.show();
    }

    protected addInputLabel(id: string): BaseInputLabel {
        const result: BaseInputLabel = this.source.getChildByName(id);
        this.inputLabels.push(result);
        return result;
    }

    protected onLoginButtonClicked(): void {
        this.emit(LobbyLoginEvents.LOGIN_BUTTON_CLICKED, {
            email: this.loginEmailInputValueLabel.text,
            password: this.loginPasswordInputValueLabel.text
        });
    }

    protected onCreateNewUserButtonClicked(): void {
        this.emit(LobbyLoginEvents.CREATE_NEW_USER_BUTTON_CLICKED, {
            email: this.newEmailInputValue.text,
            password: this.newPasswordInputValue.text,
            passwordConfirm: this.confirmPasswordInputValue.text,
            name: this.newNameInputValue.text
        });
    }

    protected onNewEmailInput(value: string): void {
        if (value.indexOf("@")=== -1 || value.indexOf(".")=== -1) {
            this.emailIsWrongLabel.setVisible(true);
        } else {
            this.emailIsWrongLabel.setVisible(false);
        }
    }
    protected onLoginEmailInput(value: string): void {
        if (value.indexOf("@")=== -1 || value.indexOf(".")=== -1) {
            this.loginPassEmailWrongLabel.setVisible(true);
        } else {
            this.loginPassEmailWrongLabel.setVisible(false);
        }
    }
}