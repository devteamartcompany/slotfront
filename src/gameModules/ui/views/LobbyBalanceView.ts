import {BaseView} from "../../../coreEngine/baseClasses/BaseView";
import {BaseLabel} from "../../../coreEngine/viewComponents/BaseLabel";
import {BaseInputLabel} from "../../../coreEngine/viewComponents/inputLabel/views/BaseInputLabel";
import {BaseButton} from "../../../coreEngine/viewComponents/BaseButton";
import {ILobbyBalanceData} from "../interfaces/ILobbyBalanceData";
import {MoneyFormatter} from "../../../coreEngine/general/localization/MoneyFormatter";
import {LobbyLoginEvents} from "../events/LobbyLoginEvents";

export class LobbyBalanceView extends BaseView {
    protected userValueLabel: BaseLabel;
    protected balanceValueLabel: BaseLabel;
    protected changeBalanceButton: BaseButton;
    protected changeBalanceInputValue: BaseInputLabel;
    protected playGameButton: BaseButton;

    protected inputLabels: BaseInputLabel[] = [];

    protected initSourceData(): void {
        super.initSourceData();
        this.userValueLabel = this.source.getChildByName("userValueLabel");
        this.balanceValueLabel = this.source.getChildByName("balanceValueLabel");
        this.changeBalanceButton = this.source.getChildByName("changeBalanceButton");
        this.changeBalanceButton.on("clicked", this.onChangeBalanceButtonClicked.bind(this))
        this.changeBalanceInputValue = this.addInputLabel("changeBalanceInputValue");
        this.playGameButton = this.source.getChildByName("playGameButton");
        this.hide();
    }

    public updateValues(data: ILobbyBalanceData): void {
        this.userValueLabel.setText(data.userName);
        this.balanceValueLabel.setText(MoneyFormatter.format(data.balance));
    }

    protected addInputLabel(id: string): BaseInputLabel {
        const result: BaseInputLabel = this.source.getChildByName(id);
        this.inputLabels.push(result);
        return result;
    }


    hide(): void {
        this.inputLabels.forEach(label => {
            label.setVisible(false);
        })
        super.hide();
    }

    show(): void {
        this.inputLabels.forEach(label => {
            label.setVisible(true);
        })
        super.show();
    }

    protected onChangeBalanceButtonClicked(): void {
        this.emit(LobbyLoginEvents.CHANGE_BALANCE_BUTTON_CLICKED, this.changeBalanceInputValue.text);
    }

}