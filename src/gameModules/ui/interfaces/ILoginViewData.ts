export interface ILoginViewData {
    email: string;
    password: string;
}