export interface ICreateUserViewData {
    email: string;
    password: string;
    passwordConfirm: string;
    name: string;
}