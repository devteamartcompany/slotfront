export interface ILobbyBalanceData {
    userName: string;
    balance: number;
}