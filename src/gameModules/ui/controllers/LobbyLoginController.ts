import {BaseController} from "../../../coreEngine/baseClasses/BaseController";
import {LobbyLoginEvents} from "../events/LobbyLoginEvents";
import {ICreateUserViewData} from "../interfaces/ICreateUserViewData";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";
import {ILoginViewData} from "../interfaces/ILoginViewData";
import {AuthorizationModel} from "../../../commonModules/server/models/AuthorizationModel";
import {getModel} from "../../../coreEngine/exports";
import {ServerServiceEvents} from "../../../commonModules/server/events/ServerServiceEvents";
import {UserModel} from "../../../commonModules/server/models/UserModel";
import {ILobbyBalanceData} from "../interfaces/ILobbyBalanceData";
import {AppLoggerMessageType} from "../../../coreEngine/general/utils/loger/AppLoggerMessageType";

export class LobbyLoginController extends BaseController {

    protected authorizationModel: AuthorizationModel;
    protected userModel: UserModel;

    protected addListeners(): void {
        super.addListeners();
        this.dispatcher.addListener(LobbyLoginEvents.CREATE_NEW_USER_BUTTON_CLICKED, this.onCreateNewUserButtonClicked, this);
        this.dispatcher.addListener(LobbyLoginEvents.LOGIN_BUTTON_CLICKED, this.onLoginButtonClicked, this);
        this.dispatcher.addListener(LobbyLoginEvents.CHANGE_BALANCE_BUTTON_CLICKED, this.onChangeBalanceButtonClicked, this);
        this.authorizationModel = getModel(this, AuthorizationModel) as AuthorizationModel;
        this.userModel = getModel(this, UserModel) as UserModel;
    }

    protected onCreateNewUserButtonClicked(data: ICreateUserViewData): void {
        AppLogger.log(JSON.stringify(data));
        this.authorizationModel = getModel(this, AuthorizationModel) as AuthorizationModel;
        this.authorizationModel.signupEmail = data.email;
        this.authorizationModel.signupName = data.name;
        this.authorizationModel.signupPassword = data.password;
        this.authorizationModel.signupPasswordConfirm = data.passwordConfirm;
        this.dispatcher.once(ServerServiceEvents.RECEIVE_CREATE_USER_RESPONSE, () => {
            AppLogger.log("new User Created");
            this.dispatcher.dispatch(LobbyLoginEvents.HIDE_LOBBY_LOGIN);
            this.showLobbyBalance();
        })
        this.dispatcher.dispatch(ServerServiceEvents.SEND_CREATE_USER_REQUEST, data);
    }

    protected onLoginButtonClicked(data: ILoginViewData): void {
        AppLogger.log(JSON.stringify(data));
        this.authorizationModel = getModel(this, AuthorizationModel) as AuthorizationModel;
        this.authorizationModel.loginEmail = data.email;
        this.authorizationModel.loginPassword = data.password;
        this.dispatcher.once(ServerServiceEvents.RECEIVE_LOGIN_RESPONSE, () => {
            AppLogger.log("user login completed");
            this.dispatcher.dispatch(LobbyLoginEvents.HIDE_LOBBY_LOGIN);
            this.showLobbyBalance();
        })
        this.dispatcher.dispatch(ServerServiceEvents.SEND_LOGIN_REQUEST, data);
    }

    protected showLobbyBalance(): void {
        this.userModel = getModel(this, UserModel) as UserModel;
        const data: ILobbyBalanceData = {
            userName: this.userModel.userName,
            balance: this.userModel.balance
        }
        this.dispatcher.dispatch(LobbyLoginEvents.SHOW_LOBBY_BALANCE, data);
    }

    protected onChangeBalanceButtonClicked(value: string): void {
        const numbValue: number = parseInt(value);
        if (isNaN(numbValue)) {
            AppLogger.log("please input number! ", AppLoggerMessageType.ERROR);
        } else {
            this.dispatcher.once(ServerServiceEvents.RECEIVE_BALANCE_RESPONSE, () => {
                AppLogger.log("user balance is changed");
                const data: ILobbyBalanceData = {
                    userName: this.userModel.userName,
                    balance: this.userModel.balance
                }
                this.dispatcher.dispatch(LobbyLoginEvents.UPDATE_BALANCE, data);
                this.showLobbyBalance();
            })
            this.dispatcher.dispatch(ServerServiceEvents.SEND_BALANCE_REQUEST, {balance: numbValue});
        }

    }

}