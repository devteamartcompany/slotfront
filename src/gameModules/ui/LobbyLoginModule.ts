import {ApplicationModule} from "../../coreEngine/baseClasses/ApplicationModule";
import {addServerModel, bindMediator, createInstance} from "../../coreEngine/exports";
import {LobbyLoginMediator} from "./mediators/LobbyLoginMediator";
import {LobbyLoginView} from "./views/LobbyLoginView";
import {LobbyLoginController} from "./controllers/LobbyLoginController";
import {AuthorizationModel} from "../../commonModules/server/models/AuthorizationModel";
import {LobbyBalanceMediator} from "./mediators/LobbyBalanceMediator";
import {LobbyBalanceView} from "./views/LobbyBalanceView";

export class LobbyLoginModule extends ApplicationModule {

    bindClasses(): void {
        super.bindClasses();
        bindMediator(LobbyLoginMediator, LobbyLoginView, "lobbyLoginContainer");
        bindMediator(LobbyBalanceMediator, LobbyBalanceView, "lobbyBalanceContainer");
        createInstance(LobbyLoginController);
    }
}