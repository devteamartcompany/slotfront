import {BaseMediator} from "../../../coreEngine/baseClasses/BaseMediator";
import {LobbyLoginView} from "../views/LobbyLoginView";
import {LobbyLoginEvents} from "../events/LobbyLoginEvents";

export class LobbyLoginMediator extends BaseMediator {

    protected view: LobbyLoginView;

    addListeners(): void {
        super.addListeners();
        this.dispatcher.addListener(LobbyLoginEvents.UPDATE_UI, this.updateUI, this);
        this.dispatcher.addListener(LobbyLoginEvents.SHOW_LOBBY_LOGIN, this.showView, this);
        this.dispatcher.addListener(LobbyLoginEvents.HIDE_LOBBY_LOGIN, this.hideView, this);
        this.registerViewListener(LobbyLoginEvents.CREATE_NEW_USER_BUTTON_CLICKED);
        this.registerViewListener(LobbyLoginEvents.LOGIN_BUTTON_CLICKED);
    }

    protected updateUI(data): void {
        this.view.updateUI(data);
    }

    protected hideView(): void {
        this.view.hide();
    }
    protected showView(): void {
        this.view.show();
    }
}