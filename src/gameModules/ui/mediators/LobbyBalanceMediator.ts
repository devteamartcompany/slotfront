import {BaseMediator} from "../../../coreEngine/baseClasses/BaseMediator";
import {LobbyLoginEvents} from "../events/LobbyLoginEvents";
import {LobbyBalanceView} from "../views/LobbyBalanceView";
import {ILobbyBalanceData} from "../interfaces/ILobbyBalanceData";

export class LobbyBalanceMediator extends BaseMediator {
    protected view: LobbyBalanceView;

    addListeners(): void {
        super.addListeners();
        this.dispatcher.addListener(LobbyLoginEvents.SHOW_LOBBY_BALANCE, this.showView, this);
        this.dispatcher.addListener(LobbyLoginEvents.HIDE_LOBBY_BALANCE, this.hideView, this);
        this.dispatcher.addListener(LobbyLoginEvents.UPDATE_BALANCE, this.updateBalance, this);
        this.registerViewListener(LobbyLoginEvents.CHANGE_BALANCE_BUTTON_CLICKED);
    }

    protected hideView(): void {
        this.view.hide();
    }
    protected showView(data: ILobbyBalanceData): void {
        this.view.show();
        this.view.updateValues(data);
    }
    protected updateBalance(data: ILobbyBalanceData): void {
        this.view.updateValues(data);
    }
}