import {BaseAction} from "../../../coreEngine/fsm/BaseAction";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";

export class InfinitePlayGameAction extends BaseAction {
    execute(): Promise<any> {
        AppLogger.log(`${this.constructor.name}`);
        return new Promise<any>((resolve) => {
            // will never not resolve
        });
    }
}