import {CoreEngine} from "./coreEngine/CoreEngine";
import {CommonModuleInitializer} from "./CommonModuleInitializer";

// if there are any common css files
// insert them here
require('./style/general.css');
if (process.env.NODE_ENV === 'development') {
    const createFps = require('fps-indicator');
    const fps = createFps({"color":"white"});

}

const coreEngine: CoreEngine = new CoreEngine();
coreEngine.init(new CommonModuleInitializer());