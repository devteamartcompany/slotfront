import {FSMContext} from "./coreEngine/fsm/FSMContext";
import {createInstance} from "./coreEngine/exports";
import {ApplicationStartState} from "./states/ApplicationStartState";
import {MainGameInitState} from "./states/MainGameInitState";


import {PlayGameState} from "./states/PlayGameState";

export class CommonFSMInitializer {

    protected fsm: FSMContext;

    public createFSM(): void {
        this.fsm = createInstance(FSMContext);
    }

    /**
     * Add states to fsm and describe possible transitions.
     * The parameter in BaseState constructor is array of possible transitions.     *
     */
    public addStates(): void {
        this.fsm.addState(createInstance(ApplicationStartState, [[MainGameInitState.ID]]));
        this.fsm.addState(createInstance(MainGameInitState, [[PlayGameState.ID]]));
        this.fsm.addState(createInstance(PlayGameState, [[""]]));

    }

    /**
     * Entry point of FSM
     */
    public startFSM(): void {
        this.fsm.goto(ApplicationStartState.ID);
    }

}