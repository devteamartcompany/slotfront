import {DebugUtils} from "./DebugUtils";

export class Kernel {

    private static bindingMap: Map<Function, Function> = new Map<Function, Function>();

    /**
     * Create new entities here.
     * @param constructor - constructor of new Class
     * @param args - constructor params
     * use createInstance(MyClass); instead of new(MyClass);
     */
    public static createInstance(constructor: any, args?: any[]): any {
        let instance: any;
        if (this.bindingMap.has(constructor)) {
            constructor = this.bindingMap.get(constructor);
        }
        if (args) {
            instance = new constructor(...args);
        } else {
            instance = new constructor();
        }

        if (process.env.NODE_ENV !== 'production') {
            // Development tool for create a global link to the class
            const parentName:string = instance.constructor.__proto__["name"];
            if (parentName === "BaseViewComponent") {
                DebugUtils.mapObjectToGlobalId(instance, instance.layoutID, "v");
            } else {
                DebugUtils.mapObjectToGlobalId(instance, instance.constructor["name"], "s");
            }

        }
        return  instance;
    }

    /**
     * You can bind your application classes to a coreEngine classes.
     * So finally your application class will be used instead of coreEngine class.
     * Example:
     * bindTo(CoreClass, ApplicationClass);
     * @param oldConstructor - constructor of coreEngine class
     * @param newConstructor - constructor of application class
     */
    public static bindTo(oldConstructor: Function, newConstructor: Function): void {
        this.bindingMap.set(oldConstructor, newConstructor);
    }

}