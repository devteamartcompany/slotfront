import {IBaseSpriteConfig} from "./interfaces/IBaseSpriteConfig";
import {IViewComponent} from "./interfaces/IViewComponent";
import * as PIXI from "pixi.js"
import {TextureHolder} from "../general/utils/layouts/TextureHolder";

/**
 * Framework logic part
 */
export class BaseSprite extends PIXI.Container implements IViewComponent {
    private _sourceData: IBaseSpriteConfig;
    public groupID: string;
    public layoutID: string;
    protected initIsComplete: boolean = false;

    constructor(data: IBaseSpriteConfig) {
        super();
        this._sourceData = data;
        this.layoutID = data.layoutID;
        this.groupID = data.groupID;
        this.x = data.posX;
        this.y = data.posY;
        this.pivot.x = data.pivotX;
        this.pivot.y = data.pivotY;
        this.groupID = data.groupID;
        this.layoutID = data.layoutID;
        if (data.alpha !== undefined) {
            this.alpha = data.alpha
        }
        if (data.rotation !== undefined) {
            this.rotation = data.rotation
        }
        if (data.scaleX !== undefined) {
            this.scale.x = data.scaleX
        }
        if (data.scaleY !== undefined) {
            this.scale.y = data.scaleY
        }
    }

    public init(): void {
        if (this.initIsComplete) {
            return;
        }
        this.addChild(TextureHolder.getLayoutTextureByName(this._sourceData.image));
        this.initIsComplete = true;
    }

    onAdded(): void {

    }

    get sourceData(): IBaseSpriteConfig {
        return this._sourceData;
    }
}