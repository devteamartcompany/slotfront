
import Container = PIXI.Container;
import {BaseSprite} from "./BaseSprite";
import {BaseCheckBox} from "./BaseCheckBox";
import {BaseButton} from "./BaseButton";
import {BaseSpineComponent} from "./BaseSpineComponent";
import {BaseBitmapLabel} from "./BaseBitmapLabel";
import Rectangle = PIXI.Rectangle;
import {ILayoutContainerConfig} from "./interfaces/ILayoutContainerConfig";
import {BaseViewComponent} from "./BaseViewComponent";
import {IViewComponent} from "./interfaces/IViewComponent";

/**
 * Framework logic part
 */
export class BaseContainer extends BaseViewComponent {
    public static TYPE: string = "container";
    protected _sourceData: ILayoutContainerConfig;
    protected initIsComplete: boolean = false;
    protected alignedContainer: Container;

    constructor(data: ILayoutContainerConfig) {
        super(data);
    }

    onAdded(): void {

    }

    init(): void {
        if (this.initIsComplete) {
            return;
        }
        this.initIsComplete = true;
        this.initSourceView(this);

        if (this.sourceData.align) {
            this.alignedContainer = new Container();

            this.children.forEach( (child) => {
                this.alignedContainer.addChild(child);
            });
            this.alignedContainer.addChild(this.getChildAt(0));
            this.addChild(this.alignedContainer);
            this.align();

        }
        if (this.sourceData.showBound) {
            this.drawBound();
        }

    }

    public align(): void {
        const localRect: Rectangle = this.alignedContainer.getLocalBounds();
        const offsetX:number =  localRect.x;
        switch (this.sourceData.align) {
            case "left":
                this.alignedContainer.x = 0 - offsetX;
                break;
            case "center":
                this.alignedContainer.x = this.sourceData.width / 2 - this.alignedContainer.width / 2  - offsetX;
                break;
            case "right":
                this.alignedContainer.x = this.sourceData.width - this.alignedContainer.width  - offsetX;
                break;
        }
    }

    protected initSourceView(source: IViewComponent): void {
        source.init();
        if (source instanceof BaseContainer) {
            (source as Container).children.forEach((child) => {
                if (child instanceof BaseContainer ||
                    child instanceof BaseSprite ||
                    child instanceof BaseCheckBox ||
                    child instanceof BaseButton ||
                    child instanceof BaseSpineComponent ||
                    child instanceof BaseBitmapLabel)
                    this.initSourceView(child as any);
            })
        }
    }

    get sourceData(): ILayoutContainerConfig {
        return this._sourceData;
    }

    protected drawBound(): void {
        const localRect: Rectangle = this.alignedContainer.getLocalBounds();
        let graphics = new PIXI.Graphics();
        graphics.beginFill(0xFFFF00, 0);
        graphics.lineStyle(1, 0xFF0000);
        const width: number = this.sourceData.width || this.width;
        graphics.drawRect(0, localRect.y, width, this.height);
        graphics.endFill();
        this.addChild(graphics);
    }
}
