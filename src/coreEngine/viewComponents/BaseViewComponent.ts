import Container = PIXI.Container;
import {IViewComponent} from "./interfaces/IViewComponent";
import {IBaseComponentConfig} from "./interfaces/IBaseComponentConfig";

export class BaseViewComponent extends Container implements IViewComponent {

    protected _groupID: string;
    protected _layoutID: string;
    protected _sourceData: IBaseComponentConfig;

    constructor(data: IBaseComponentConfig) {
        super();
        this._sourceData = data;
        this.pivot.x = data.pivotX;
        this.pivot.y = data.pivotY;
        this.x = data.posX;
        this.y = data.posY;
        this._groupID = data.groupID;
        this._layoutID = data.layoutID;
        if (data.alpha !== undefined) {
            this.alpha = data.alpha
        }
        if (data.rotation !== undefined) {
            this.rotation = data.rotation
        }
        if (data.scaleX !== undefined) {
            this.scale.x = data.scaleX
        }
        if (data.scaleY !== undefined) {
            this.scale.y = data.scaleY
        }
        if (data.visible !== undefined) {
            this.visible = data.visible;
        }
        if (data.angle !== undefined) {
            this.rotation = data.angle * (Math.PI / 180);
        }

    }

    init(): void {
    }

    onAdded(): void {
    }

    get sourceData(): IBaseComponentConfig {
        return this._sourceData;
    }

    get layoutID(): string {
        return this._layoutID;
    }

    get groupID(): string {
        return this._groupID;
    }

    public setVisible(value: boolean): void{
        this.visible = value;
    }
}
