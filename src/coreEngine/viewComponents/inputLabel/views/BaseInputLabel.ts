import {ICustomInputBounds} from "../utils/ICustomInputBounds";
import {getInputVisibleStyles} from "../utils/getInputVisibleStyles";
import {CustomInputEvents} from "../events/CustomInputEvents";
import {BaseViewComponent} from "../../BaseViewComponent";
import {ILabelConfig} from "../../interfaces/ILabelConfig";
import Point = PIXI.Point;
import {BaseRectangle} from "../../BaseRectangle";
import {getViewComponent} from "../../../exports";
import {RendererManagerEvents} from "../../../../commonModules/renderModule/events/RendererManagerEvents";
import {SlotEventDispatcher} from "../../../general/utils/eventDispatcher/SlotEventDispatcher";
import {EventDispatcher} from "../../../general/EventDispatcher";
import {IRenderViewParams} from "../../../../commonModules/renderModule/IRenderViewParams";

export class BaseInputLabel extends BaseViewComponent {

    private _width: number;
    protected _height: number;
    protected _sourceData: ILabelConfig;
    protected _boundRect: PIXI.Graphics;

    protected readonly styles = {
        position: 'fixed',
        border: 'none',
        outline: 'none',
        margin: '0',
        padding: '0 4px',
        display: 'none',
        zIndex: '1',
        background: 'rgba(0, 0, 0, 0)',
    };

    constructor(data: ILabelConfig) {
        super(data);
        this._width = data.width;
        this._height = data.height;
        this.drawBound();
        this._boundRect.visible = this.sourceData.showBound;
        this.createInput(data.isTextArea);

        this.setupInput({
            value: this.sourceData.text,
            ...getInputVisibleStyles(
                ['width', 'fontFamily', 'fontWeight', 'fontSize', 'fill'],
                (this._boundRect),
                this.sourceData
            )
        });
        this.interactive = true;
        this.buttonMode = true;
        this.on("click", () => {
            this.inputField.focus();
        });
        if (data.text) {
            this.setText(data.text);
        }
        EventDispatcher.getInstance().addListener(RendererManagerEvents.RESIZE, this.onResize.bind(this))
    }

    protected drawBound(): void {
        let graphics = new PIXI.Graphics();
        graphics.beginFill(0xFFFF00, 0);
        graphics.lineStyle(1, 0xFF0000);
        graphics.drawRect(0, 0, this.width, this.height);
        graphics.endFill();
        this.addChild(graphics);
        this._boundRect = graphics;
    }

    public setText(text: string): void {
        this.inputField.value = this.inputValue = text;
    }

    public focus(): void {
        this.inputField.focus();
    }

    get sourceData(): ILabelConfig {
        return this._sourceData;
    }

    public get height(): number {
        return this._height;
    }

    get width(): number {
        return this._width;
    }

    get text(): string {
        return this.inputField.value;
    }

    public set x(value: number) {
        super.x = value;
        if (this.inputField && this.parent) {
            this.inputField.style.left = value.toString() + "px";
        }
    }

    public set y(value: number) {
        super.y = value;
        if (this.inputField && this.parent) {

            this.inputField.style.top = value.toString() + "px";
            // this.inputField.style.top = (pos.y + value.toString()) + "px";
        }
    }

    public get x(): number {
        return super.x;
    }

    public get y(): number {
        return super.y;
    }

    /**
     * HTML PArt
     */
    protected inputField: HTMLInputElement | HTMLTextAreaElement;
    protected inputValue: string;
    protected isInputShown: boolean;

    public createInput(isTextarea?: boolean): void {
        this.inputField = document.createElement(isTextarea ? 'textarea' :`input`);
        this.parseStyles(this.styles);

        document.getElementsByTagName('body')[0].appendChild(this.inputField);

        // this.inputField.addEventListener('blur', this.hideInput.bind(this));
        this.inputField.addEventListener('keyup', this.onEnterHandle.bind(this));
        this.inputField.addEventListener('input', this.onInputHandle.bind(this));
    }

    public setupInput({value = '', ...styles}: CSSStyleDeclaration & { value?: string }): void {
        this.inputField.value = this.inputValue = value;
        this.parseStyles(styles);
        this.inputField.style.display = 'block';
        // this.inputField.focus();
        this.inputField.setSelectionRange(0, value.length);

        this.isInputShown = true;
    }

    public setVisible(value: boolean): void {
        super.setVisible(value);
        this.inputField.style.display = value ? 'block' : 'none';
        this.isInputShown = value;
    }

    public onEnterHandle(event: KeyboardEvent): void {
        if (event.key === 'Enter' || event.code === 'Enter') {
            this.emit(CustomInputEvents.CHANGE_CUSTOM_FIELD_VALUE, this.inputField.value);
        }
    }

    public onInputHandle(): void {
        let value: string = this.inputField.value;
        // let value: string = this.inputField.value.replace(/\D/g, '');
        // if (value.length > 6) value = value.slice(-6);
        // this.inputField.value = value = parseInt(value) ? parseInt(value) + '' : '';
        if (value !== this.inputValue) {
            this.inputValue = value;
            // this.emit(CustomInputEvents.CHANGE_CUSTOM_FIELD_VALUE, value);
        }
    }

    public resizeAndRepositionInput(data: ICustomInputBounds): void {
        this.parseStyles(data);
    }

    protected parseStyles(styles): void {
        Object.keys(styles).forEach((key) => {
            this.inputField.style[key] = styles[key]
        });
    }

    protected onResize(params: IRenderViewParams): void {
        const pos: Point = this.getGlobalPosition();
        const fieldHeight: number = parseInt(this.inputField.style.height);
        this.inputField.style.left = (pos.x).toString() + "px";
        this.inputField.style.top = (pos.y - fieldHeight*(0.5-params.scale) - fieldHeight * 0.5 * params.scale).toString() + "px";

        this.inputField.style.fontSize = Math.floor(this.sourceData.fontSize * params.scale).toString() + "px";
    }
}
