export interface ICustomInputBounds {
    x?: number;
    y?: number;
    width?: number;
    height?: number;
}

export interface ICustomInputVisualData extends ICustomInputBounds {
    align?: string;
    fontFamily?: string;
    fontWeight?: string;
    fontSize?: number;
    fill?: string;
}