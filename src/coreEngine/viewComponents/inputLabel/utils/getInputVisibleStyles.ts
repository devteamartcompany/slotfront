import {getElementBounds} from "./getElementBounds";
import {convertToCssFromPixi} from "./convertToCssFromPixi";
import {ICustomInputVisualData} from "./ICustomInputBounds";
import {ILabelConfig} from "../../interfaces/ILabelConfig";

type GetInputVisibleStyles = (props: string[], source: PIXI.DisplayObject, sourceData: ILabelConfig) => CSSStyleDeclaration

export const getInputVisibleStyles: GetInputVisibleStyles = (props, source, sourceData) => {
    // const sourceData: ILabelConfig = getViewComponent(groupLabel).sourceData as ILabelConfig;
    const bounds = getElementBounds(['x', 'y', 'width', 'height'], source);

    const canvasStyles: ICustomInputVisualData = {
        ...props.reduce((acc, prop) => {
            acc[prop] = sourceData[prop];
            return acc
        }, {}),
        ...bounds
    };

    canvasStyles.fill && (canvasStyles['caret-color'] = canvasStyles.fill);
    canvasStyles.fontSize && (canvasStyles.fontSize = Math.floor(bounds.width * canvasStyles.fontSize / sourceData.width));

    return convertToCssFromPixi(canvasStyles);
};
