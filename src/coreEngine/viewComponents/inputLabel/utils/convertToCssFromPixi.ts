
// must have full list of possible props
import {ICustomInputVisualData} from "./ICustomInputBounds";

const cssGlossary: { [key: string]: string[] } = {
    x: ['left', 'px'],
    y: ['top', 'px'],
    width: ['width', 'px'],
    height: ['height', 'px'],
    fontFamily: ['fontFamily', ''],
    fontWeight: ['fontWeight', ''],
    fontSize: ['fontSize', 'px'],
    fill: ['color', ''],
    'caret-color': ['caret-color', ''],
};

export const convertToCssFromPixi = (canvasStyles: ICustomInputVisualData): CSSStyleDeclaration => {
    const styles = {} as CSSStyleDeclaration;

    Object.keys(cssGlossary).forEach((key) => {
        if (canvasStyles[key]) {
            styles[cssGlossary[key][0]] = canvasStyles[key] + cssGlossary[key][1];
        }
    });

    return styles
};
