import {ICustomInputBounds} from "./ICustomInputBounds";

type GetElementBounds = (props: string[], source: PIXI.DisplayObject) => ICustomInputBounds

export const getElementBounds: GetElementBounds = (props, source) => {
    const bound = source.getBounds();
    return <ICustomInputBounds>props.reduce((acc, prop) => {
        acc[prop] = bound[prop];
        return acc
    }, {});
};
