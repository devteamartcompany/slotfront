import Sprite = PIXI.Sprite;
import * as PIXI from "pixi.js";
import Graphics = PIXI.Graphics;
import {BaseViewComponent} from "./BaseViewComponent";
import {BaseLabel} from "./BaseLabel";
import {IBaseButtonConfig, IButtonText} from "./interfaces/IBaseButtonConfig";
import {TextureHolder} from "../general/utils/layouts/TextureHolder";
import {DeviceInfoService} from "../general/DeviceInfoService";
import {ILabelConfig} from "./interfaces/ILabelConfig";
import {CoreRenderer} from "../general/utils/renderer/CoreRenderer";
import {PointerEventsConstants} from "./PointerEventsConstants";

/**
 * Framework logic part
 */
export class BaseButton extends BaseViewComponent {
    protected normal: Sprite;
    protected over: Sprite;
    protected disable: Sprite;
    protected pressed: Sprite;

    protected normalLabel: BaseLabel;
    protected overLabel: BaseLabel;
    protected disableLabel: BaseLabel;
    protected pressedLabel: BaseLabel;
    protected commonLabel: BaseLabel;

    protected _isEnabled: boolean;

    protected initIsComplete: boolean = false;

    protected _sourceData: IBaseButtonConfig;

    protected _hitTestObject: Graphics;

    protected hitAreaOpacity: number = 0;
    protected hitAreaPolygon: number[] = [];

    protected _buttonText: IButtonText;

    private readonly AVERAGE_OFFSET: number = 50;

    constructor(data: IBaseButtonConfig) {
        super(data);

        if( data.hitAreaPolygon !== undefined ) {
            this.hitAreaPolygon = data.hitAreaPolygon;
        }

        if( data.angle !== undefined ) {
            this.rotation = data.angle * (Math.PI / 180);
        }

        if( data.hitAreaOpacity !== undefined ) {
            this.hitAreaOpacity = data.hitAreaOpacity;
        }

        if( data.buttonText !== undefined ) {
            this.buttonText = data.buttonText;
        }
    }

    public get isEnabled(): boolean {
        return this._isEnabled;
    }

    public set isEnabled(value: boolean) {
        this._isEnabled = value;
    }

    public get buttonText(): IButtonText {
        return this._buttonText;
    }

    public set buttonText(value: IButtonText) {
        this._buttonText = value;
    }

    public get sourceData(): IBaseButtonConfig {
        return this._sourceData;
    }

    public get hitTestObject(): PIXI.Graphics {
        return this._hitTestObject;
    }

    public init(): void {
        if (this.initIsComplete) {
            return;
        }

        this.normal = TextureHolder.getLayoutTextureByName(this.sourceData.images.normal);
        this.over = TextureHolder.getLayoutTextureByName(this.sourceData.images.over);
        this.disable = TextureHolder.getLayoutTextureByName(this.sourceData.images.disable);
        this.pressed = TextureHolder.getLayoutTextureByName(this.sourceData.images.pressed);

        this.normal.visible = true;
        this.over.visible = false;
        this.disable.visible = false;
        this.pressed.visible = false;

        this.addChild(this.normal);
        this.addChild(this.over);
        this.addChild(this.disable);
        this.addChild(this.pressed);

        if( this.hitAreaPolygon.length > 0 ) {
            this._hitTestObject = this.drawPoly();
        } else {
            this._hitTestObject = this.drawRectangle();
        }

        this.addChild(this._hitTestObject);

        this._hitTestObject.interactive = true;
        this._hitTestObject.buttonMode = true;

        this._hitTestObject.on(PointerEventsConstants.POINTERDOWN, this.onButtonDown.bind(this));
        this._hitTestObject.on(PointerEventsConstants.CLICK, this.onButtonUp.bind(this));
        this._hitTestObject.on(PointerEventsConstants.POINTEROVER, this.onButtonOver.bind(this));
        this._hitTestObject.on(PointerEventsConstants.POINTEROUT, this.onButtonOut.bind(this));

        if( DeviceInfoService.isMobile ) {
            this._hitTestObject.on(PointerEventsConstants.POINTERUP, this.onButtonUp.bind(this))
        }

        if( this._buttonText ) {
            this.createButtonTextLabels();
        }

        this.initIsComplete = true;
    }

    private drawPoly(): Graphics {
        let graphics = new PIXI.Graphics();

        graphics.beginFill(0xffffff, this.hitAreaOpacity);
        graphics.drawPolygon(this.hitAreaPolygon);
        graphics.endFill();

        return graphics;
    }

    private drawRectangle(): Graphics {
        let graphics = new PIXI.Graphics();

        graphics.beginFill(0xffffff, this.hitAreaOpacity);
        graphics.drawRect(0, 0, this.normal.width, this.normal.height);
        graphics.endFill();

        return graphics;
    }

    public onAdded(): void {
        this.normal.visible = true;

        if( this._buttonText && !this.commonLabel ) {
            this.normalLabel.visible = true;
        }
    }

    protected onButtonDown(): void {
        this.pressed.visible = true;
        this.normal.visible = this.over.visible = this.disable.visible = false;
        this.emit(PointerEventsConstants.POINTERDOWN);

        if( this._buttonText && !this.commonLabel ) {
            this.pressedLabel.visible = true;
            this.overLabel.visible = this.normalLabel.visible = this.disableLabel.visible = false;
        }
    }

    protected onButtonUp(): void {
        this.checkOver();

        this.emit(PointerEventsConstants.CLICKED);
    }

    protected onButtonOver(): void {
        this.over.visible = true;
        this.normal.visible = this.disable.visible = this.pressed.visible = false;

        if( !DeviceInfoService.isMobile ) {
            this.emit(PointerEventsConstants.MOUSE_OVER);
        }

        if( this._buttonText && !this.commonLabel ) {
            this.overLabel.visible = true;
            this.disableLabel.visible = this.normalLabel.visible = this.pressedLabel.visible = false;
        }
    }

    protected onButtonOut(): void {
        this.normal.visible = true;
        this.over.visible = this.disable.visible = this.pressed.visible = false;

        if( !DeviceInfoService.isMobile ) {
            this.emit(PointerEventsConstants.MOUSE_OUT);
        }

        if( this._buttonText && !this.commonLabel ) {
            this.normalLabel.visible = true;
            this.overLabel.visible = this.disableLabel.visible = this.pressedLabel.visible = false;
        }
    }

    protected createButtonTextLabels(): void {
        if( !this._buttonText.over && !this._buttonText.pressed &&
            !this._buttonText.normal && !this._buttonText.disable ) {
            this.commonLabel = this.newBaseLabel(this._buttonText.common, this.normal.parent);

            this.commonLabel.visible = true;

            return;
        }

        this.overLabel = this.newBaseLabel(this._buttonText.over, this.over.parent);
        this.pressedLabel = this.newBaseLabel(this._buttonText.pressed, this.pressed.parent);
        this.disableLabel = this.newBaseLabel(this._buttonText.disable, this.disable.parent);
        this.normalLabel = this.newBaseLabel(this._buttonText.normal, this.normal.parent);

        this.normalLabel.visible = true;
    }

    protected newBaseLabel(labelConfig: ILabelConfig, buttonParent: PIXI.Container): BaseLabel {
        let baseLabel: BaseLabel = new BaseLabel(this.addCommonParameters(labelConfig, this._buttonText.common));
        baseLabel.visible = false;

        this.repositionTextField(buttonParent, baseLabel);
        this.addChild(baseLabel);

        return baseLabel;
    }

    protected addCommonParameters(target: ILabelConfig, common: ILabelConfig): ILabelConfig {
        for( let key of Object.keys(common) ) {
            if( !target[key] ) {
                target[key] = common[key];
            }
        }

        return target;
    }

    protected repositionTextField(button: PIXI.Container, label: BaseLabel): void {
        if( button.width > label.width ) {
            label.x = (button.width - label.width) / 2;
        }

        label.alignText();
    }

    public enable(value: boolean): void {
        this._hitTestObject.interactive = value;
        this._hitTestObject.buttonMode = value;

        this.isEnabled = value;

        if( !value ) {
            this.disable.visible = true;
            this.over.visible = this.normal.visible = this.pressed.visible = false;

            if( this._buttonText && !this.commonLabel ) {
                this.disableLabel.visible = true;
                this.overLabel.visible = this.normalLabel.visible = this.pressedLabel.visible = false;
            }
        } else {
            this.checkOver();
        }
    }

    public setVisible(value: boolean): void {
        this.visible = value;

        if( this.isEnabled ) {
            this.checkOver();
        }
    }

    protected checkOver(): void {
        let mousePosition = CoreRenderer.renderer.plugins.interaction.mouse.global;

        const hitObject = CoreRenderer.renderer.plugins.interaction.hitTest(mousePosition);

        if( hitObject === this.hitTestObject ) {
            this.over.visible = true;
            this.normal.visible = this.disable.visible = this.pressed.visible = false;

            if( this._buttonText && !this.commonLabel ) {
                this.overLabel.visible = true;
                this.disableLabel.visible = this.normalLabel.visible = this.pressedLabel.visible = false;
            }
        } else {
            this.normal.visible = true;
            this.over.visible = this.disable.visible = this.pressed.visible = false;

            if( this._buttonText && !this.commonLabel ) {
                this.normalLabel.visible = true;
                this.overLabel.visible = this.disableLabel.visible = this.pressedLabel.visible = false;
            }
        }
    }

    public reposition(x: number, y: number) {
        this.position.set(x, y);
    }
}
