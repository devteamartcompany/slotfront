import SkeletonData = PIXI.spine.core.SkeletonData;
import {EnterFrameManager, IFrameListener} from "../general/utils/renderer/EnterFrameManager";
import {IViewComponent} from "./interfaces/IViewComponent";
import {IBaseSpineConfig} from "./interfaces/IBaseSpineConfig";
import {TextureHolder} from "../general/utils/layouts/TextureHolder";
import {SpineFactory} from "./spine/SpineFactory";
import Event = PIXI.spine.core.Event;

export class BaseSpineComponent extends PIXI.Container implements PIXI.spine.core.AnimationStateListener2, IFrameListener, IViewComponent {
    get isActive(): boolean {
        return this._isActive;
    }

    public layoutID?: string;
    public groupID: string;
    public sourceData: any;
    protected _skeletonData: PIXI.spine.core.SkeletonData;
    protected initIsComplete: boolean = false;
    private _clip: PIXI.spine.Spine;
    private onCompleteCallback: Function;
    private _isActive: boolean = false;
    private _listeners: Object = {};

    constructor(data: IBaseSpineConfig) {
        super();
        this.sourceData = data;
        this.pivot.x = data.pivotX;
        this.pivot.y = data.pivotY;
        this.x = data.posX;
        this.y = data.posY;
        this.groupID = data.groupID;
        this.layoutID = data.layoutID;
        if (data.alpha !== undefined) {
            this.alpha = data.alpha
        }
    }

    public init(): void {
        if (this.initIsComplete) {
            return;
        }
        this._skeletonData = this.createSkeletonData();
        this.createSpineMovie(this._skeletonData);
        this.initIsComplete = true;
    }

    private createSkeletonData(): SkeletonData {
        const textures = TextureHolder.getTextureMap(SpineFactory.getInstance().getSkeletonData(this.layoutID), PIXI.utils.TextureCache);
        const atlas = new PIXI.spine.core.TextureAtlas(null, null, null);

        atlas.addTextureHash(textures, true);

        const atlasLoader = new PIXI.spine.core.AtlasAttachmentLoader(atlas);
        const jsonParser = new PIXI.spine.core.SkeletonJson(atlasLoader);
        const data: SkeletonData = jsonParser.readSkeletonData(SpineFactory.getInstance().getSkeletonData(this.layoutID));

        return data;
    }

    public createSpineMovie(data: SkeletonData): void {
        if (!data) {
            data = this.createSkeletonData();
        }
        this._clip = new PIXI.spine.Spine(data);
        this._clip.autoUpdate = false;
    }

    public onEnterFrame(deltaTime: number): void {
        if (this._clip && this._clip.skeleton) {
            this._clip.update(deltaTime);
            if (!this.parent) {
                this.removeClip();
            }
        }
    }

    public removeClip(): void {
        if (this._clip.state) {
            this._clip.state.removeListener(this);
        }

        // set timeout cause of PIXI Spine trouble. It first update it's state (and dispatch event that clip is ended) then
        // works with it's skeleton.
        // So we should destroy Spine after it finishes work with skeleton but before next tick
        setTimeout( () => {
            if (this._clip.skeleton) {
                this._clip.destroy() ;
            }
        }, 1);
        this._isActive = false;
        this.removeChildren();

        EnterFrameManager.instance.removeListener(this);
    }


    public playAnimation(name: string, isLoop: boolean, callback?: Function): void {
        if (!this._isActive) {
            this.activateAnimation();
        }
        this.onCompleteCallback = null;
        if (callback) {
            this.onCompleteCallback = callback;
        }
        this._clip.skeleton.setToSetupPose();
        this._clip.state.setAnimation(0, name, isLoop);
        this._clip.update(0);
    }

    public pause(): void {
        this._clip.autoUpdate = false;
    }

    public resume(): void {
        this._clip.autoUpdate = true;
    }

    public addSpineEventListener(event: string, callback: Function): void {
        this._listeners[event] = callback;
    }

    public removeSpineEventListener(event: string): void {
        delete this._listeners[event];
    }

    protected activateAnimation() {
        this._isActive = true;
        this._clip.state.addListener(this);
        EnterFrameManager.instance.addListener(this);
        this._clip.state.onEvent = (i: number, event: Event) => {
            if (this._listeners[event.stringValue]) {
                this._listeners[event.stringValue]();
            }
        };
        this.addChild(this._clip);

    }

    start(entry: PIXI.spine.core.TrackEntry): void {

    }

    interrupt(entry: PIXI.spine.core.TrackEntry): void {
    }

    end(entry: PIXI.spine.core.TrackEntry): void {
    }

    dispose(entry: PIXI.spine.core.TrackEntry): void {
    }

    complete(entry: PIXI.spine.core.TrackEntry): void {
        if (this.onCompleteCallback) {
            this.onCompleteCallback();
        }
    }

    get skeletonData(): PIXI.spine.core.SkeletonData {
        return this._skeletonData;
    }

    get clip(): PIXI.spine.Spine {
        return this._clip;
    }

    event(entry: PIXI.spine.core.TrackEntry, event: PIXI.spine.core.Event): void {
    }

    onAdded(): void {
        throw new Error("Method not implemented.");
    }

    destroy(): void {
        throw new Error("Method not implemented.");
    }

}