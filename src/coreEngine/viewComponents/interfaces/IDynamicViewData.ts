export interface IDynamicViewData {
    mediatorClass: Function,
    viewClass: Function,
    layoutID: string,
    holderID: string
}
