import {IBaseComponentConfig} from "./IBaseComponentConfig";

export interface IViewComponent {
    layoutID?: string,
    groupID: string,
    sourceData: IBaseComponentConfig,

    onAdded(): void,

    destroy(): void,
    init(): void
}
