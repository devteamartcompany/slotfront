import {IBaseComponentConfig} from "./IBaseComponentConfig";
import {ILabelConfig} from './ILabelConfig';

export interface IBaseButtonConfig extends IBaseComponentConfig {
    "images": {
        "over": string,
        "pressed": string,
        "normal": string,
        "disable": string
    },
    "hitAreaPolygon"?: number[],
    "hitAreaOpacity"?: 0,
    "buttonText"?: IButtonText
}

export interface IButtonText {
    "common": ILabelConfig,
    "over"?: ILabelConfig,
    "pressed"?: ILabelConfig,
    "normal"?: ILabelConfig,
    "disable"?: ILabelConfig
}
