import {IBaseComponentConfig} from "./IBaseComponentConfig";

export interface IBaseSpineConfig  extends IBaseComponentConfig {
    skeleton: string
}