export interface IRadioButtonData {
    columns?: number,
    vOffset?: number,
    hOffset?: number,
    rectNormalTemplate: string,
    labelNormalTemplate: string,
    rectCheckedTemplate: string,
    labelCheckedTemplate: string,
    textOffset?: number
    textColour?: IColour
}

export interface IColour {
    normal: string,
    hover: string,
    checked: string
}