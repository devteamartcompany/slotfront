import {IBaseComponentConfig} from "./IBaseComponentConfig";

export interface IBaseSpriteAnimationConfig extends IBaseComponentConfig {
    "extension": string,
    "frames": string[],
    "animationSpeed"?: number
}