import {IBaseComponentConfig} from "./IBaseComponentConfig";

export interface ILayoutListConfig {
    "layoutList": IBaseComponentConfig[]
}