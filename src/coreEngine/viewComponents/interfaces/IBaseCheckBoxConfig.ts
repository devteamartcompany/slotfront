import {IBaseComponentConfig} from "./IBaseComponentConfig";

export interface IBaseCheckBoxConfig extends IBaseComponentConfig{
    "images": {
        "on": string,
        "off": string
    }
    "hitAreaPolygon"?: number[]
}
