export interface IBaseComponentConfig {
    "type": string,
    "groupID"?: string,
    "layoutID"?: string,
    "platform"?: string,
    "alpha"?: number,
    "posX": number,
    "posY": number,
    "pivotX"?: number,
    "pivotY"?: number,
    "scaleX"?: number,
    "scaleY"?: number,
    "rotation"?: number,
    "visible"?: boolean,
    "angle"?: number,
    "useCheats"?: boolean
}
