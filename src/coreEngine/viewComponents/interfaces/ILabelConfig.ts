import {IBaseComponentConfig} from "./IBaseComponentConfig";

export interface ILabelConfig extends IBaseComponentConfig {
    text: string,
    width: number,
    height?: number,
    align?: string,
    vAlign?: string,
    showBound?: boolean,
    scaleToWidth?: boolean,
    scaleToHeight?: boolean,
    fontFamily?: string,
    fontStyle?: string,
    fontSize?: number,
    fill?: string | number,
    stroke?: string | number,
    strokeThickness?: number,
    dropShadow?: boolean,
    dropShadowColor?: string | number,
    dropShadowBlur?: number,
    dropShadowAngle?: number,
    dropShadowDistance?: number,
    localKey?: string,
    multiStyle?: {},
    isTextArea?: boolean
}
