export interface IRangeComponentData {
    increase: boolean,
    decrease: boolean,
    labelValue: string,
    progressPercent: number
}