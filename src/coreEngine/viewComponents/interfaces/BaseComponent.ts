import {IViewComponent} from "./IViewComponent";

export class BaseComponent extends PIXI.Container implements IViewComponent {
    layoutID: string;
    groupID: string;
    private _sourceData: any;

    constructor(data: any) {
        super();
        this.layoutID = data.layoutID;
        this.groupID = data.groupID;
    }

    onAdded(): void {
    }

    destroy(): void {
    }

    init(): void {

    }
    get sourceData(): any {
        return this._sourceData;
    }

}
