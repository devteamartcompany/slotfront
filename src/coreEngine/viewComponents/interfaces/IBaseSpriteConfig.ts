import {IBaseComponentConfig} from "./IBaseComponentConfig";

export interface IBaseSpriteConfig extends  IBaseComponentConfig{
    "image": string,
    "width"?: number,
    "height"?: number,
}
