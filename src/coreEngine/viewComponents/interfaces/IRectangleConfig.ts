import {IBaseComponentConfig} from "./IBaseComponentConfig";

export interface IRectangleConfig extends IBaseComponentConfig{

    width: number,
    height: number,
    color?: number,
    cornerRadius?: number,
    lineWidth?: number,
    lineAlpha?: number,
    lineColor?: number,
    fillTransparency?: number,
    interactive?: boolean

}
