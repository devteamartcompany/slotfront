import {IBaseComponentConfig} from "./IBaseComponentConfig";

export interface ILayoutContainerConfig extends IBaseComponentConfig {
    "childHolder": IBaseComponentConfig[],
    "width"?: number,
    "showBound"?: boolean,
    "scaleToWidth"?: boolean,
    "align"?: string
}
