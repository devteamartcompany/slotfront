import {ILayoutListConfig} from "../interfaces/ILayoutListConfig";
import {IViewComponentMap} from "../interfaces/IViewComponentMap";

import {SpineFactory} from "./SpineFactory";
import {IViewComponent} from "../interfaces/IViewComponent";
import {ILayoutContainerConfig} from "../interfaces/ILayoutContainerConfig";
import {BaseContainer} from "../BaseContainer";
import {IBaseComponentConfig} from "../interfaces/IBaseComponentConfig";
import {ViewTypeConst} from "./ViewTypeConst";
import {BaseViewComponent} from "../BaseViewComponent";
import {createInstance} from "../../exports";
import {DeviceInfoService} from "../../general/DeviceInfoService";

export class ViewComponentsFactory {
    private static _instance: ViewComponentsFactory = new ViewComponentsFactory();
    public viewMap: IViewComponentMap<IViewComponent> = {};

    constructor() {
        if (ViewComponentsFactory._instance) {
            throw new Error("Error: Instantiation failed: Use ViewComponentsFactory.getInstance() instead of new.");
        }
        ViewComponentsFactory._instance = this;
    }

    static getInstance(): ViewComponentsFactory {
        return this._instance;
    }

    public parseViewList(data: ILayoutListConfig) {
        for (let i: number = 0; i < data.layoutList.length; i++) {
            let item = data.layoutList[i];
            if (item.platform && item.platform !== this.getPlatform()) {
                continue;
            }
            this.parseViewComponent(item);
        }
    }

    private parseContainer(data: ILayoutContainerConfig, parentContainer?: BaseContainer): BaseContainer {
        let currentContainer: BaseContainer = new BaseContainer(data);
        currentContainer.name = data.layoutID;
        if (parentContainer) {
            parentContainer.addChild(currentContainer);
        }
        for (let i: number = 0; i < data.childHolder.length; i++) {
            let item = data.childHolder[i];
            if (item.platform && item.platform !== this.getPlatform()) {
                continue;
            }
            this.parseViewComponent(item, currentContainer);
        }
        return currentContainer;
    }

    private parseViewComponent(item: IBaseComponentConfig, parentContainer?: BaseContainer): void {
        switch (item.type) {
            case BaseContainer.TYPE:
                this.mapData(this.parseContainer(item as ILayoutContainerConfig, parentContainer));
                break;
            default:
                this.createViewComponent(item, ViewTypeConst[item.type], parentContainer);
                break;
        }
    }

    private createViewComponent(data: IBaseComponentConfig, classConstructor: Function, parentContainer?: BaseContainer) {
        let viewComponent: BaseViewComponent = createInstance(classConstructor, [data])
        viewComponent.name = data.layoutID;
        if (parentContainer) {
            parentContainer.addChild(viewComponent);
        }
        this.mapData(viewComponent);
        if (process.env.NODE_ENV !== 'production' && data.type === "spineAnimation") {
            if (data.useCheats) {
                SpineFactory.getInstance().setUseCheatSpineClipName(viewComponent.name);
            }
        }
    }

    protected mapData(data: IViewComponent) {
        if (data.layoutID) {
            this.viewMap[data.layoutID] = data;
        }
    }

    public getViewComponent(layoutID: string): IViewComponent {
        return this.viewMap[layoutID];
    }

    public createNewView(layoutID: string): IViewComponent {
        const sourceView: IViewComponent = this.viewMap[layoutID];
        const data: IBaseComponentConfig = sourceView.sourceData;
        const className: Function = ViewTypeConst[sourceView.sourceData.type];
        if (sourceView.sourceData.type === BaseContainer.TYPE) {
            return this.parseContainer(data as ILayoutContainerConfig);
        }
        const view: IViewComponent = createInstance(className, [data])
        return view;
    }

    private getPlatform(): string {
        if (DeviceInfoService.isMobile) {
            return "mobile";
        }
        return "desktop";
    }
}
