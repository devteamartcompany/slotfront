export interface IViewComponentMap<T> {
    [key: string]: T
}