import {BaseContainer} from "../BaseContainer";
import {BaseButton} from "../BaseButton";
import {BaseSprite} from "../BaseSprite";
import {BaseCheckBox} from "../BaseCheckBox";
import {BaseLabel} from "../BaseLabel";
import {BaseBitmapLabel} from "../BaseBitmapLabel";
import {BaseRectangle} from "../BaseRectangle";
import {BaseSpriteAnimation} from "../BaseSpriteAnimation";
import {BaseSpineComponent} from "../BaseSpineComponent";
import {BaseInputLabel} from "../inputLabel/views/BaseInputLabel";

export const ViewTypeConst = {
    "container": BaseContainer,
    "button": BaseButton,
    "image": BaseSprite,
    "checkBox": BaseCheckBox,
    "label": BaseLabel,
    "bitmapLabel": BaseBitmapLabel,
    "rectangle": BaseRectangle,
    "spriteAnimation": BaseSpriteAnimation,
    "spineAnimation": BaseSpineComponent,
    "inputLabel": BaseInputLabel
}
