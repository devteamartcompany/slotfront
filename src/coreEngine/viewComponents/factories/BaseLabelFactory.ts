import {BaseLabel} from "../BaseLabel";
import {createNewView} from "../../exports";


export class BaseLabelFactory {
    public static create(text: string, x: number, y:number, template: string): BaseLabel {
        const label: BaseLabel = createNewView(template) as  BaseLabel;
        label.x = x;
        label.y = y;
        label.setText(text);
        return label;
    }
}
