import {BaseSpineData} from "../interfaces/BaseSpineData";
import {IAssociative} from "../../interfaces/IAssociative";
import {BaseSpineComponent} from "../BaseSpineComponent";
import {getViewComponent} from "../../exports";

export class SpineFactory {


    private static _instance: SpineFactory = new SpineFactory();
    private _spineMap: IAssociative<BaseSpineData> = {};
    private _spineCheatList: string[] = [];

    constructor() {
        if (SpineFactory._instance) {
            throw new Error("Error: Instantiation failed: Use SpineFactory.getInstance() instead of new.");
        }
        SpineFactory._instance = this;
    }

    static getInstance(): SpineFactory {
        return this._instance;
    }

    public mapSpine(dataParse: any, id: string) {
        let baseSpineData: BaseSpineData = new BaseSpineData();
        baseSpineData.id = id;
        baseSpineData.skeletonData = dataParse;
        this._spineMap[baseSpineData.id] = baseSpineData;
    }

    public getSkeletonData(id): any {
        return this._spineMap[id].skeletonData;
    }

    public getSpineClip(id: string): BaseSpineComponent {
        return this.createSpineMovie(id);
    }

    public setUseCheatSpineClipName(name: string): void {
        this._spineCheatList.push(name);
    }

    private createSpineMovie(id: string): BaseSpineComponent {
        let baseData = getViewComponent(id) as BaseSpineComponent;
        let baseSpineClip: BaseSpineComponent = new BaseSpineComponent(baseData.sourceData);
        baseSpineClip.createSpineMovie(baseSpineClip.skeletonData);
        return baseSpineClip;
    }

    public get spineCheatList(): string[] {
        return this._spineCheatList;
    }

}
