import {IDynamicViewData} from "../interfaces/IDynamicViewData";
import {createInstance, getViewComponent} from "../../exports";

export class DynamicViewCreator {
    private static mediatorMap: IDynamicViewData[] = [];

    /**
     * method for binding Mediator class with View class with its layout
     * @param mediatorClass - class, extended from BaseMediator
     * @param viewClass - class, extended from BaseView
     * @param layoutID - is layoutID for a container with All assets.
     * @param holderID - is layoutID for an empty container in a root container (not obligatory).
     */
    public static bindMediator(mediatorClass: Function, viewClass: Function, layoutID: string, holderID: string): void {
        this.mediatorMap.push({mediatorClass, viewClass, layoutID, holderID})
    }

    /**
     * method for creating class instances
     */
    public static createViews(): void {
        this.mediatorMap.forEach((item) => {
            let viewComponent = getViewComponent(item.layoutID);
            let holderView = getViewComponent(item.holderID);
            let view = createInstance(item.viewClass, [viewComponent, holderView]);
            createInstance(item.mediatorClass, [view]);
        })
    }
}
