export interface IDinamicViewData {
    mediatorClass: Function,
    viewClass: Function,
    layoutID: string,
    holderID: string
}