import {IViewComponent} from "./interfaces/IViewComponent";
import * as PIXI from "pixi.js"
import {IBaseSpriteAnimationConfig} from "./interfaces/IBaseSpriteAnimationConfig";
import Texture = PIXI.Texture;
import ObjectRenderer = PIXI.ObjectRenderer;

/**
 * Framework logic part
 */
export class BaseSpriteAnimation extends PIXI.Container implements IViewComponent {
    private _sourceData: IBaseSpriteAnimationConfig;
    public groupID: string;
    public layoutID: string;

    /*public sequenceName: string;
    public extension: string;*/
    public frames: string[];
    public framesTotal: number;
    protected textures: any[] = [];
    public animation: PIXI.extras.AnimatedSprite;
    protected initIsComplete: boolean = false;

    constructor(data: IBaseSpriteAnimationConfig) {
        super();
        this._sourceData = data;
        this.layoutID = data.layoutID;
        this.groupID = data.groupID;
        this.x = data.posX;
        this.y = data.posY;
        this.pivot.x = data.pivotX;
        this.pivot.y = data.pivotY;
        this.groupID = data.groupID;
        this.layoutID = data.layoutID;
        this.frames = data.frames;
        this.framesTotal = data.frames.length;
        if (data.alpha !== undefined) {
            this.alpha = data.alpha
        }
    }

    public init(): void {
        if (this.initIsComplete) {
            return;
        }
        this.frames.forEach((frame) => {
            let texture: Texture = PIXI.Texture.fromImage(frame + this.sourceData.extension);
            this.textures.push(texture)
        })
        this.animation = new PIXI.extras.AnimatedSprite(this.textures);
        if (this.sourceData.animationSpeed) {
            this.animation.animationSpeed = this.sourceData.animationSpeed;
        }
        this.addChild(this.animation);
        this.initIsComplete = true;
    }

    public set animationSpeed(value: number) {
        this.animation.animationSpeed = value;
    }

    public play(loop: boolean = false): Promise<void> {
        return new Promise<void>((resolve) => {
            this.animation.loop = loop;
            this.animation.play();
            if (!loop) {
                this.animation.onComplete = () => {
                    this.animation.gotoAndStop(0);
                    resolve();
                };
            }

        })
    }

    public stop(): void {
        this.animation.stop();
    }

    onAdded(): void {

    }

    get sourceData(): IBaseSpriteAnimationConfig {
        return this._sourceData;
    }
}