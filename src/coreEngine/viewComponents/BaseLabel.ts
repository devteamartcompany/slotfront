import {IViewComponent} from "./interfaces/IViewComponent";
import {ILabelConfig} from "./interfaces/ILabelConfig";
import TextStyle = PIXI.TextStyle;
import MultiStyleText from "pixi-multistyle-text";
import {LocalUtil} from "../general/localization/LocalUtil";

export class BaseLabel extends PIXI.Container implements IViewComponent {
    private _groupID: string;
    private _layoutID: string;
    private _sourceData: ILabelConfig;

    private _textField: PIXI.Text;
    private _sourceText: String;
    private _scaleToWidth: boolean = false;
    private _isMultiStyle: boolean = false;
    private dataTextStyle: {};
    private _width: number;
    private _height: number;
    private _localKey: string;
    private _isCustom: boolean;

    private _align: string;
    private _vAlign: string;

    constructor(data: ILabelConfig) {
        super();
        this._sourceData = data;
        this._width = data.width;
        this.pivot.x = data.pivotX;
        this.pivot.y = data.pivotY;
        this.x = data.posX;
        this.y = data.posY;
        this._groupID = data.groupID;
        this._layoutID = data.layoutID;
        this._scaleToWidth = data.scaleToWidth;
        if (data.alpha !== undefined) {
            this.alpha = data.alpha
        }

        if (data.localKey) {
            this._localKey = data.localKey;
            data.text = LocalUtil.text(this._localKey);
        }
        if (data.multiStyle) {
            this._isMultiStyle = true;
            this.dataTextStyle = data.multiStyle;
            this._textField = new MultiStyleText(data.text, data.multiStyle);
            if (Object.values(data.multiStyle).find( (elem) => {
                return elem["link"] !== undefined;
            })) {
                this.findMultiStyleTextPosition(this._textField as MultiStyleText);
            }


        } else {
            let style = new TextStyle(data);
            this._textField = new PIXI.Text(data.text, style);
        }
        if (data.height ) {
            this._height = data.height;
        } else {
            this._height = this._textField.height;
        }


        this._sourceText = data.text;

        if (this.sourceData.showBound) {
            this.drawBound();
        }
        this.align = this.sourceData.align;
        this.vAlign = this.sourceData.vAlign;
        this.alignText();
        this.addChild(this._textField);
    }

    protected drawBound(): void {
        let graphics = new PIXI.Graphics();
        graphics.beginFill(0xFFFF00, 0);
        graphics.lineStyle(1, 0xFF0000);
        graphics.drawRect(0, 0, this.width, this.height);
        graphics.endFill();
        this.addChild(graphics);
    }

    init(): void {
    }

    onAdded(): void {
    }

    get groupID(): string {
        return this._groupID;
    }

    get sourceData(): ILabelConfig {
        return this._sourceData;
    }

    get layoutID(): string {
        return this._layoutID;
    }

    get textField(): PIXI.Text {
        return this._textField;
    }

    get width(): number {
        return this._width;
    }

    set width(value: number) {
        this._width = value;
    }

    get align(): string {
        return this._align;
    }

    set align(value: string) {
        this._align = value;
    }

    get vAlign(): string {
        return this._vAlign;
    }

    set vAlign(value: string) {
        this._vAlign = value;
    }

    get isCustom(): boolean {
        return this._isCustom;
    }

    set isCustom(value: boolean) {
        this._isCustom = value;
    }
    get scaleToWidth(): boolean {
        return this._scaleToWidth;
    }

    set scaleToWidth(value: boolean) {
        this._scaleToWidth = value;
    }

    public setText(value: string): void {
        this._textField.text = this._sourceText + value;
        this.alignText();
    }

    public alignText(): void {
        if (!this._isMultiStyle) {
            if (this._scaleToWidth && this._textField.width > this.width) {
                this.selectFontSize();
            }
            switch (this.align) {
                case "left":
                    this._textField.x = 0;
                    break;
                case "center":
                    this._textField.x = this.width / 2 - this.textField.width / 2;
                    break;
                case "right":
                    this._textField.x = this.width - this.textField.width;
                    break;

            }
        }

        switch (this.vAlign) {
            case "top":
                this._textField.y = 0;
                break;
            case "center":
                this._textField.y = this.height / 2 - this._textField.height / 2;
                break;
            case "bottom":
                this._textField.y = this.height  - this._textField.height ;
                break;
        }
    }

    public setVisible(value: boolean): void{
        this.visible = value;
    }

    protected selectFontSize(): void {
        this._textField.style.fontSize = Number(this._textField.style.fontSize) - 1;
        this._textField.y = this._height / 2 - this._textField.height / 2;
        if (this._scaleToWidth && this._textField.width > this.width && Number(this._textField.style.fontSize) > 7) {
            this.selectFontSize();
        }
    }

    public get height(): number {
        return this._height;
    }

    protected findMultiStyleTextPosition(textField: MultiStyleText): void {

        let outputText;
        if(textField["_style"].wordWrap) {
            outputText = textField["wordWrap"](textField.text);
        }

        // split text into lines
        let lines = outputText.split(/(?:\r\n|\r|\n)/);

        // get the text data with specific styles
        let outputTextData = textField["_getTextDataPerLine"](lines);

        // calculate text width and height
        let lineWidths: number[] = [];
        let lineYMins: number[] = [];
        let lineYMaxs: number[] = [];
        let maxLineWidth = 0;

        for (let i = 0; i < lines.length; i++) {
            let lineWidth = 0;
            let lineYMin = 0;
            let lineYMax = 0;
            for (let j = 0; j < outputTextData[i].length; j++) {
                let sty = outputTextData[i][j].style;


                // save the width
                outputTextData[i][j].width = textField.context.measureText(outputTextData[i][j].text).width;

                if (outputTextData[i][j].text.length !== 0) {
                    outputTextData[i][j].width += (outputTextData[i][j].text.length - 1) * sty.letterSpacing;

                    if (j > 0) {
                        lineWidth += sty.letterSpacing / 2; // spacing before first character
                    }

                    if (j < outputTextData[i].length - 1) {
                        lineWidth += sty.letterSpacing / 2; // spacing after last character
                    }
                }

                lineWidth += outputTextData[i][j].width;

                // save the font properties
                outputTextData[i][j].fontProperties = PIXI.TextMetrics.measureFont(textField.context.font);

                // save the height
                outputTextData[i][j].height = outputTextData[i][j].fontProperties.fontSize;

                if (typeof sty.valign === "number") {
                    lineYMin =
                        Math.min(
                            lineYMin,
                            sty.valign
                            - outputTextData[i][j].fontProperties.descent);
                    lineYMax =
                        Math.max(
                            lineYMax,
                            sty.valign
                            + outputTextData[i][j].fontProperties.ascent);
                } else {
                    lineYMin =
                        Math.min(
                            lineYMin,
                            -outputTextData[i][j].fontProperties.descent);
                    lineYMax =
                        Math.max(
                            lineYMax,
                            outputTextData[i][j].fontProperties.ascent);
                }
            }

            lineWidths[i] = lineWidth;
            lineYMins[i] = lineYMin;
            lineYMaxs[i] = lineYMax;
            maxLineWidth = Math.max(maxLineWidth, lineWidth);
        }

        let reducedY: number = 0;

        // Compute the drawing data
        for (let i = 0; i < outputTextData.length; i++) {

            let line = outputTextData[i];

            let finalX: number = 0;
            let finalHeight: number = 0;

            for (let j = 0; j < line.length; j++) {
                let {style, text, fontProperties, width, height, tag} = line[j];

                if (style["link"]) {
                    const graphics = new PIXI.Graphics();
                    graphics.beginFill( 0xFFFF00, 0);
                    graphics.lineStyle(1,  0xFF0000,  0);
                    graphics.drawRect(finalX, reducedY, line[j]["width"], line[j]["height"]);
                    graphics.endFill();
                    graphics.interactive = true;
                    graphics.buttonMode = true;
                    graphics.on("pointerup", () => {
                        const win = window.open(style["link"], '_blank');
                        win.focus();
                    })
                    textField.addChild(graphics);
                }
                finalHeight = height;
                finalX += width + style.letterSpacing / 2;

            }
            reducedY += finalHeight;

        }

    }
}