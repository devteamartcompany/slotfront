export interface IBaseSpine<T> {
    [key: string]: T
}