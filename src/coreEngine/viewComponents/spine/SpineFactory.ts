import {BaseSpineClipTest} from "./BaseSpineClipTest";
import {IBaseModel} from "../../interfaces/IBaseModel";
import {IAssociative} from "../../interfaces/IAssociative";
import {TextureHolder} from "../../general/utils/layouts/TextureHolder";
import SkeletonData = PIXI.spine.core.SkeletonData;
import {IViewComponentMap} from "../factories/IViewComponentMap";
import {IViewComponent} from "../interfaces/IViewComponent";
import {BaseSpineData} from "./BaseSpineData";
import {getViewComponent} from "../../exports";
import {BaseSpineComponent} from "../BaseSpineComponent";


export class SpineFactory {
    private static _instance: SpineFactory = new SpineFactory();
    private _spineMap: IAssociative<BaseSpineData> = {};

    constructor() {
        if (SpineFactory._instance) {
            throw new Error("Error: Instantiation failed: Use DeviceManager.getInstance() instead of new.");
        }
        SpineFactory._instance = this;
    }

    static getInstance(): SpineFactory {
        return this._instance;
    }

    public mapSpine(dataParse: any, id: string) {
        let baseSpineData: BaseSpineData = new BaseSpineData();
        baseSpineData.id = id;
        baseSpineData.skeletonData = dataParse;
        this._spineMap[baseSpineData.id] = baseSpineData;
    }

    public getSkeletonData(id): any {
        return this._spineMap[id].skeletonData;
    }

    /* public mapSpine(spine: BaseSpineClip): void {
         this._spineMap[spine.id] = spine;
     }*/

    public getSpineClip(id: string): BaseSpineComponent {
        return this.createSpineMovie(id);
    }

    private createSpineMovie(id: string): BaseSpineComponent {
        let baseData = getViewComponent(id) as BaseSpineComponent;
        let baseSpineClip: BaseSpineComponent = new BaseSpineComponent(baseData.sourceData);
        baseSpineClip.createSpineMovie(baseSpineClip.skeletonData);
        return baseSpineClip;
    }


}