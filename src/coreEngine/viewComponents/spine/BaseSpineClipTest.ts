import 'pixi-spine/bin/pixi-spine'

import SkeletonData = PIXI.spine.core.SkeletonData;
import {EnterFrameManager, IFrameListener} from "../../general/utils/renderer/EnterFrameManager";
import {AppLogger} from "../../general/utils/loger/AppLogger";
import {AppLoggerMessageType} from "../../general/utils/loger/AppLoggerMessageType";

export class BaseSpineClipTest extends PIXI.Container implements PIXI.spine.core.AnimationStateListener2, IFrameListener {

    public _id: string;
    private _isCreated: boolean;
    private _skeletonData: PIXI.spine.core.SkeletonData;
    private _clip: PIXI.spine.Spine;
    private onCompleteCallback: Function;
    private _isActive: boolean = false;

    public createSpineMovie(data: SkeletonData): BaseSpineClipTest {
        this._clip = new PIXI.spine.Spine(data);
        this._isCreated = true;
        this.addClip();
        return this;
    }

    public onEnterFrame(deltaTime: number): void {
        this._clip.update(deltaTime);
        if (!this.parent) {
            this.removeClip();
        }
        AppLogger.log(this._id, AppLoggerMessageType.ERROR);

    }

    protected addClip(): void {

        //this._clip.autoUpdate = false;
        AppLogger.log(this._id, AppLoggerMessageType.SUCCESS);
    }

    public removeClip(): void {
        this._clip.state.removeListener(this);
        EnterFrameManager.instance.removeListener(this);
        this._isActive = false;
        this.removeChildren();
        AppLogger.log(this._id, AppLoggerMessageType.EVENT);
    }


    public playAnimation(name: string, isLoop: boolean, callback?: Function): void {
        if (!this._isActive) {
            this.activateAnimation();
        }
        this.onCompleteCallback = null;
        if (callback) {
            this.onCompleteCallback = callback;
        }
        this._clip.skeleton.setToSetupPose();
        this._clip.state.setAnimation(0, name, isLoop);
        this._clip.update(0);
    }

    public pause(): void {
        this._clip.autoUpdate = false;
    }

    public resume(): void {
        this._clip.autoUpdate = true;
    }

    protected activateAnimation() {
        this._isActive = true;
        this._clip.state.addListener(this);
        EnterFrameManager.instance.addListener(this);
        this.addChild(this._clip);

    }

    start(entry: PIXI.spine.core.TrackEntry): void {

    }

    interrupt(entry: PIXI.spine.core.TrackEntry): void {
    }

    end(entry: PIXI.spine.core.TrackEntry): void {
    }

    dispose(entry: PIXI.spine.core.TrackEntry): void {
    }

    complete(entry: PIXI.spine.core.TrackEntry): void {
        if (this.onCompleteCallback) {
            this.onCompleteCallback();
        }
    }

    event(entry: PIXI.spine.core.TrackEntry, event: PIXI.spine.core.Event): void {
    }

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get isCreated(): boolean {
        return this._isCreated;
    }

    get skeletonData(): PIXI.spine.core.SkeletonData {
        return this._skeletonData;
    }

    set skeletonData(value: PIXI.spine.core.SkeletonData) {
        this._skeletonData = value;
    }
}