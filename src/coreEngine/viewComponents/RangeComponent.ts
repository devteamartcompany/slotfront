import {BaseContainer} from "./BaseContainer";
import {BaseButton} from "./BaseButton";
import {BaseLabel} from "./BaseLabel";
import {IRangeComponentData} from "./interfaces/IRangeComponentData";
import Container = PIXI.Container;
import {BaseRectangle} from "./BaseRectangle";
export class RangeComponent extends Container {

    public static INCREASE_CLICKED: string = "INCREASE_CLICKED";
    public static DECREASE_CLICKED: string = "DECREASE_CLICKED";

    protected increaseButton: BaseButton;
    protected decreaseButton: BaseButton;
    protected valueLabel: BaseLabel;

    protected  progressBar: BaseRectangle;

    constructor(protected source:BaseContainer) {
        super();
        this.increaseButton = source.getChildByName("increaseButton");
        this.increaseButton.on("clicked", (value: boolean) => {
            this.emit(RangeComponent.INCREASE_CLICKED);
        });
        this.decreaseButton = source.getChildByName("decreaseButton");
        this.decreaseButton.on("clicked", (value: boolean) => {
            this.emit(RangeComponent.DECREASE_CLICKED);
        });
        this.valueLabel = source.getChildByName("valueLabel");
        this.progressBar = source.getChildByName("progressBar");
    }

    public updateComponent(data: IRangeComponentData): void {
        if (data.decrease !== undefined) {
            this.decreaseButton.enable(data.decrease);
            if (data.decrease) {
                this.decreaseButton.setVisible(true);
            }
        }
        if (data.increase !== undefined) {
            this.increaseButton.enable(data.increase);
            if (data.increase) {
                this.increaseButton.setVisible(true);
            };
        }
        data.labelValue !== undefined && this.valueLabel.setText(data.labelValue);
        this.progressBar.rect.scale.x = data.progressPercent;
    }

    public setVisible(value: boolean): void {
        this.source.visible = value;
    }
}