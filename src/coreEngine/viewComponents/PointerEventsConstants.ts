export const PointerEventsConstants = {
    POINTERDOWN :'pointerdown',
    CLICK :'click',
    POINTEROVER :'pointerover',
    POINTEROUT :'pointerout',
    POINTER_UP_OUTSIDE :'pointerupoutside',
    POINTERMOVE :'pointermove',
    POINTERUP :'pointerup',

    CLICKED: 'clicked',
    MOUSE_OVER: 'mouseOver',
    MOUSE_OUT: 'mouseOut',
    MOUSE_DOWN: 'mouseDown'
}
