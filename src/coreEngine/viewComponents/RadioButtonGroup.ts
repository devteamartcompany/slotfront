import {RadioButton} from "./RadioButton";
import {IRadioButtonData} from "./interfaces/IRadioButtonData";

export class RadioButtonGroup extends PIXI.Container {
    protected _currentRadio: RadioButton;
    protected radioButtons: RadioButton[] = [];
    private _defaultValue: string;

    constructor(radioValues: string[], data: IRadioButtonData, defaultValue?: string) {
        super();
        this._defaultValue = defaultValue;
        this.create( radioValues, data);
    }

    protected create(radioValues: string[], data:IRadioButtonData): void {
        for (let i: number = 0; i < radioValues.length; i++) {
            const elem: string = radioValues[i].toString();
            const radioButton: RadioButton = new RadioButton(elem, data);
            this.addChild(radioButton);
            radioButton.y = (radioButton.height + data.vOffset) * Math.floor(i / data.columns);
            radioButton.x = (radioButton.getWidth() + data.hOffset) * (i % data.columns);
            this.addCustomRadioButton(radioButton);
            this.radioButtons.push(radioButton);
        }
    }

    public addCustomRadioButton(radioButton: RadioButton): void {
        radioButton.interactive = true;
        radioButton.addListener("pointerup", (event) => {
            this.setRadioButton(event.currentTarget as RadioButton);
            this.emit(radioButton.isCustom ? "RadioCheckedCustom" : "RadioChecked");
        });
        if (radioButton.text === this._defaultValue) {
            this.setRadioButton(radioButton);
        }
        this.radioButtons.push(radioButton);
    }

    public setValue(value: string): void {
        const radioButton: RadioButton = this.radioButtons.find( (elem) => {
            return elem.text.toString() === value;
        });
        if (radioButton) {
            this.setRadioButton(radioButton);
            this.emit("RadioChecked");
        }
    }


    public setRadioButton(radioButton: RadioButton): void {
        this._currentRadio && this._currentRadio.setChecked(false);
        this._currentRadio = radioButton;
        this._currentRadio.setChecked(true);
    }

    public clean(): void {
        if (this._defaultValue) {
            const defaultRadioButton: RadioButton = this.radioButtons.find((elem)=>{
                return elem.text === this._defaultValue;
            })
            this.setRadioButton(defaultRadioButton);
        } else {
            this._currentRadio && this._currentRadio.setChecked(false);
            this._currentRadio = null;
        }
    }

    public get currentValue(): string {
        return this._currentRadio ? this._currentRadio.text : null;
    }
    get defaultValue(): string {
        return this._defaultValue;
    }

    public setCurrentValue( value: string ): void {
        this._currentRadio.text = value;
    }
}