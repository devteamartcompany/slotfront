import {IViewComponent} from "./interfaces/IViewComponent";
import {IRectangleConfig} from "./interfaces/IRectangleConfig";
import Graphics = PIXI.Graphics;

/**
 * Framework logic part
 */
export class BaseRectangle extends PIXI.Sprite implements IViewComponent {
    public groupID: string;
    public layoutID: string;
    public sourceData: IRectangleConfig;
    public rect: Graphics;
    protected initIsComplete: boolean = false;

    constructor(data: IRectangleConfig) {
        super();
        this.layoutID = data.layoutID;
        this.groupID = data.groupID;
        this.x = data.posX;
        this.y = data.posY;
        this.pivot.x = data.pivotX;
        this.pivot.y = data.pivotY;
        this.groupID = data.groupID;
        this.layoutID = data.layoutID;
        if (data.alpha !== undefined) {
            this.alpha = data.alpha
        }
        this.sourceData = data;

        this.init();

    }

    public init(): void {
        if (this.initIsComplete) {
            return;
        }
        const graphics = new PIXI.Graphics();
        graphics.beginFill(this.sourceData.color || 0xFFFF00, this.sourceData.fillTransparency != undefined ? this.sourceData.fillTransparency : 1);
        if (this.sourceData.lineWidth && this.sourceData.lineWidth > 0) {
            graphics.lineStyle(this.sourceData.lineWidth, this.sourceData.lineColor || 0xFF0000, this.sourceData.lineAlpha || 1);
        }
        if (this.sourceData.cornerRadius) {
            graphics.drawRoundedRect(0, 0, this.sourceData.width, this.sourceData.height, this.sourceData.cornerRadius);
        } else {
            graphics.drawRect(0, 0, this.sourceData.width, this.sourceData.height);
        }
        graphics.endFill();
        this.rect = graphics;
        this.addChild(graphics);
        this.initIsComplete = true;
    }

    onAdded(): void {

    }
}