import Container = PIXI.Container;
import {BaseRectangle} from "./BaseRectangle";
import {createNewView} from "../exports";
import {BaseLabel} from "./BaseLabel";
import {IRadioButtonData} from "./interfaces/IRadioButtonData";
import {BaseSprite} from "./BaseSprite";
import {LocalUtil} from "../general/localization/LocalUtil";

export class RadioButton extends PIXI.Container {
    protected normalState: Container;
    protected checkedState: Container;
    protected isChecked: boolean;
    protected textField: BaseLabel;
    protected textFieldActive: BaseLabel;

    private _rectWidth: number;

    constructor(public text: string, data: IRadioButtonData) {
        super();
        this.create(text, data);
    }

    protected create(text: string, data: IRadioButtonData): void {
        const offset: number = data.textOffset !== undefined ? data.textOffset : 12;
        this.normalState = new Container();
        let graphics = createNewView(data.rectNormalTemplate);
        let height: number;
        if (graphics instanceof BaseRectangle) {

            this._rectWidth = graphics.rect.width;
            height = graphics.rect.height;
        } else if (graphics instanceof BaseSprite) {
            graphics.init();
            this._rectWidth = graphics.width;
            height = graphics.height;
        }


        let textField: BaseLabel = createNewView(data.labelNormalTemplate) as BaseLabel;
        this.textField = textField;
        textField.width = this._rectWidth - offset;
        textField.x = offset / 2;

        textField.setText(text);
        this.normalState.addChild(graphics as any);
        this.normalState.addChild(textField);
        textField.y = height / 2 - textField.height / 2;

        this.addChild(this.normalState);

        this.checkedState = new Container();

        graphics = createNewView(data.rectCheckedTemplate) as BaseRectangle;
        textField = createNewView(data.labelCheckedTemplate) as BaseLabel;
        this.textFieldActive = textField;
        textField.width = this._rectWidth - offset;
        textField.x = offset / 2;

        textField.setText(text);

        if (text === LocalUtil.text('Custom')) this.textFieldActive.isCustom = true;

        this.checkedState.addChild(graphics as any);
        this.checkedState.addChild(textField);
        textField.y = height / 2 - textField.height / 2;

        this.addChild(this.checkedState);

        this.setChecked(false);

        this.buttonMode = true;
    }

    public setChecked(value: boolean): void {
        this.isChecked = value;
        this.normalState.visible = !value;
        this.checkedState.visible = value;
    }

    getWidth(): number {
        return this._rectWidth;
    }

    public setText(text: string): void {
        this.textField.setText(text);
        this.textFieldActive.setText(text);
    }

    public get isCustom(): boolean {
        return !!this.textFieldActive.isCustom;
    }
}