import {IViewComponent} from "./interfaces/IViewComponent";
import {ILabelConfig} from "./interfaces/ILabelConfig";

export class BaseBitmapLabel extends PIXI.Container implements IViewComponent {
    private _groupID: string;
    private _layoutID: string;
    private _sourceData: ILabelConfig;
    private _scaleToWidth: boolean = false;
    private _textField: PIXI.extras.BitmapText;
    private _width: number;
    private _align: string;

    constructor(data: ILabelConfig) {
        super();
        this._sourceData = data;
        this._width = data.width;
        this.pivot.x = data.pivotX;
        this.pivot.y = data.pivotY;
        this._scaleToWidth = data.scaleToWidth;
        this.x = data.posX;
        this.y = data.posY;
        this._groupID = data.groupID;
        this._layoutID = data.layoutID;
        if (data.alpha !== undefined) {
            this.alpha = data.alpha
        }
        this.align = this.sourceData.align;
    }

    init(): void {
        this._textField = new PIXI.extras.BitmapText(this._sourceData.text, {
            font: `${this._sourceData.fontSize} ${this._sourceData.fontFamily}`,
            align: `${this._sourceData.align}`
        });
        this.alignText();
        this.addChild(this._textField);
    }

    onAdded(): void {
    }

    get groupID(): string {
        return this._groupID;
    }

    get sourceData(): ILabelConfig {
        return this._sourceData;
    }

    get layoutID(): string {
        return this._layoutID;
    }

    get textField(): PIXI.extras.BitmapText {
        return this._textField;
    }

    get width(): number {
        return this._width;
    }

    get align(): string {
        return this._align;
    }

    set align(value: string) {
        this._align = value;
    }

    set width(value: number) {
        this._width = value;
    }

    public setText(value: string): void {
        this._textField.text = value;
        this.alignText();
    }

    public alignText(): void {
        if (this._scaleToWidth && this._textField.width > this.width) {
            let ratio: number = this._textField.width / this._textField.height;
            let newWidth = Math.min(this._textField.width, this.width);
            let newHeight = newWidth / ratio;
            this._textField.height = newHeight;
            this._textField.width = newWidth;
        }
        switch (this.align) {
            case "left":
                this.textField.anchor = 0;
                break;
            case "center":
                this.textField.anchor = .5;
                break;
            case "right":
                this.textField.anchor = 1;
                break;
        }

    }

    public setVisible(value: boolean): void{
        this.visible = value;
    }
}