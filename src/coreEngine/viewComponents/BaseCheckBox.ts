import {IBaseCheckBoxConfig} from "./interfaces/IBaseCheckBoxConfig";
import Sprite = PIXI.Sprite;
import {TextureHolder} from "../general/utils/layouts/TextureHolder";
import {IViewComponent} from "./interfaces/IViewComponent";
import * as PIXI from "pixi.js"
import Graphics = PIXI.Graphics;
import {DeviceManager} from "../general/utils/deviceManager/DeviceManager";

/**
 * Framework logic part
 */
export class BaseCheckBox extends PIXI.Container implements IViewComponent {
    protected onState: Sprite;
    protected offState: Sprite;

    private _isEbabled: boolean;
    private _isChecked: boolean = true;
    private _sourceData: IBaseCheckBoxConfig;
    public groupID: string;
    public layoutID: string;
    protected hitAria: number[] = [];
    private _hiTestObject: Graphics;

    constructor(data: IBaseCheckBoxConfig) {
        super();
        this._sourceData = data;
        this.x = data.posX;
        this.y = data.posY;
        this.pivot.x = data.pivotX;
        this.pivot.y = data.pivotY;
        this.groupID = data.groupID;
        this.layoutID = data.layoutID;
        if (data.alpha !== undefined) {
            this.alpha = data.alpha
        }
        if (data.hitAreaPolygon !== undefined) {
            this.hitAria = data.hitAreaPolygon;
        }
    }

    public init(): void {
        if (this.onState) {
            return;
        }
        this.onState = TextureHolder.getLayoutTextureByName(this.sourceData.images.on);
        this.offState = TextureHolder.getLayoutTextureByName(this.sourceData.images.off);
        this.onState.visible = true;
        this.offState.visible = false;

        this.addChild(this.onState);
        this.addChild(this.offState);
        if (this.hitAria.length > 0) {
            this._hiTestObject = this.drawPoly();
        } else {
            this._hiTestObject = this.drawRectangle();
        }
        this.addChild(this._hiTestObject);

        this._hiTestObject.interactive = true;
        this._hiTestObject.buttonMode = true;

        this._hiTestObject.on('pointerdown', this.onButtonDown.bind(this));
        this._hiTestObject.on('click', this.onButtonUp.bind(this));
        this._hiTestObject.on('pointerover', this.onButtonOver.bind(this));
        this._hiTestObject.on('pointerout', this.onButtonOut.bind(this));
        if (DeviceManager.getInstance().isMobile) {
            this._hiTestObject.on('pointerup', this.onButtonUp.bind(this))
        }

    }

    private drawPoly(): Graphics {
        let graphics = new PIXI.Graphics();
        graphics.beginFill(0xffffff, 0);
        graphics.drawPolygon(this.hitAria);
        graphics.endFill();
        return graphics
    }

    private drawRectangle(): Graphics {
        let graphics = new PIXI.Graphics();
        graphics.beginFill(0xffffff, 0);
        graphics.drawRect(0, 0, this.onState.width, this.onState.height);
        graphics.endFill();
        return graphics
    }

    public onAdded(): void {
        this.onState.visible = true;
    }

    public onButtonDown(): void {

    }

    public onButtonUp(): void {
        if (this._isChecked) {
            this._isChecked = false;
        } else {
            this._isChecked = true;
        }
        this.updateButton();
        //TODO: promises for events
        this.emit("checked", this.isChecked);
    }

    public onButtonOver(): void {

    }

    public onButtonOut(): void {

    }

    public enable(value: boolean): void {
        this._hiTestObject.interactive = value;
        this._hiTestObject.buttonMode = value;
        this.isEnabled = value;
    }

    public setVisible(value: boolean): void {
        this.visible = value;
    }

    public get isEnabled(): boolean {
        return this._isEbabled;
    }

    public set isEnabled(value: boolean) {
        this._isEbabled = value;
    }

    public get isChecked(): boolean {
        return this._isChecked;
    }

    public set isChecked(value: boolean) {
        this._isChecked = value;
        this.updateButton();
    }

    get sourceData(): IBaseCheckBoxConfig {
        return this._sourceData;
    }

    private updateButton(): void {
        this.onState.visible = this._isChecked;
        this.offState.visible = !this._isChecked;
    }
}