import {BaseAction} from "./BaseAction";

/**
 * This action is resolved after the last included action is resolved.
 * The actions start step by step in a queue order.
 */
export class SerialAction extends BaseAction{

    execute(): Promise<any> {
        return new Promise<void>(resolve => {
            this.queue().then(() => {
                resolve();
            })
        });
    }

    async queue() {
        for (const action of this.actions) {
            await action.execute();
            await action.end();
        }
    }

}