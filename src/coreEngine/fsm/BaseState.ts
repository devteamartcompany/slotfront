import {SlotEventDispatcher} from "../general/utils/eventDispatcher/SlotEventDispatcher";
import {getDispatcher} from "../exports";
import {BaseAction} from "./BaseAction";
import {AppLogger} from "../general/utils/loger/AppLogger";
import {AppLoggerMessageType} from "../general/utils/loger/AppLoggerMessageType";

export abstract class BaseState {
    protected dispatcher: SlotEventDispatcher;
    protected actions: BaseAction[] = [];

    constructor(protected _id: string, protected permittedStates: string[]) {
        this.dispatcher = getDispatcher(this);
        this.actions = this.addActions();
    }

    /**
     * This method is called just after FSM switches to this state
     */
    public start(): Promise<string> {
        AppLogger.log(`${this.constructor.name}`);
        return new Promise<string>(resolve => {
            this.execute().then(() => {
                const nextState: string = this.getNextState();
                if (this.permittedStates.indexOf(nextState) !== -1) {
                    resolve(nextState);
                } else {
                    AppLogger.log(`you can't switch to  ${nextState} from  ${this.id}`, AppLoggerMessageType.ERROR)
                }

            })
        });
    }

    /**
     * Fill states with actions.
     * Use Serial and Parallel actions to wrap your actions.
     * Example:
     * return[
     *    createInstance(SerialAction, [
     *      createInstance(LoadConfigAction),
     *      createInstance(InitAppAction),
     *      createInstance(LoadAssetPreloadAction),
     *      createInstance(LoadSoundPreloadAction),
     *      createInstance(SendInitServerRequestAction),
     *      createInstance(LoadAssetInitAction),
     *      createInstance(LoadSoundInitAction),
     *      createInstance(LoadAssetLazyAction),
     *      createInstance(LoadSoundLazyAction)
     *  ]
     * )
     * ]
     */
    public abstract addActions(): BaseAction[];

    /**
     * Describe logic for switching to next state.
     *
     */
    public  abstract getNextState(): string;

    /**
     * return state id
     */
    public get id(): string {
        return this._id;
    }

    public skip(): void {

    }

    public onEnd(): void {

    }

    protected async execute(): Promise<void> {
        if (this.isSkipped()) {
            return Promise.resolve();
        } else {
            for (const action of this.actions) {
                await action.execute();
            }
        }
    }

    protected isSkipped(): boolean {
        return false;
    }


}