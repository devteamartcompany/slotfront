import {SlotEventDispatcher} from "../general/utils/eventDispatcher/SlotEventDispatcher";
import {getDispatcher} from "../exports";

/**
 * Action is a main logic structure. It creates promise and resolve after some logic action
 * (e.g. play sound, play movie clip, show image, load assets etc) is finished.
 *
 * Use dispatcher to communicate with mediators and controllers.
 *
 */
export abstract class BaseAction {

    protected dispatcher: SlotEventDispatcher;
    protected actions: BaseAction[];

    constructor(...args) {
        this.dispatcher = getDispatcher(this);
        this.actions = args;
    }

    /**
     * We dispatch events here for some logic actions (e.g. play sound, play movie clip, show image, load assets etc)
     * and listens about their finish
     */
    public abstract execute(): Promise<any>

    /**
     * End action, remove all listeners if exists
     */
    public end(): void {
    }

}