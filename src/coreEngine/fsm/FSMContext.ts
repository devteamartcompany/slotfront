import {BaseState} from "./BaseState";
import {AppLogger} from "../general/utils/loger/AppLogger";
import {AppLoggerMessageType} from "../general/utils/loger/AppLoggerMessageType";

export class FSMContext {

    private _currentState: BaseState;
    protected states: Map<string, BaseState> = new Map<string, BaseState>();

    public goto(stateId: string): void {
        if (!this.states[stateId]) {
            AppLogger.log(`${stateId} is not added to FSM`, AppLoggerMessageType.ERROR);
        }
        AppLogger.log(`=> FSM switched to ${stateId}`);
        this._currentState = this.states[stateId];
        this._currentState.start().then( (state) => {
            this.goto(state);
        })
    }

    public addState(state: BaseState): void {
        this.states[state.id] = state;
    }

    get currentState(): BaseState {
        return this._currentState;
    }

}