import {BaseAction} from "./BaseAction";

/**
 * This action is resolved after the last included action is resolved.
 * All actions start in the one moment and work in parallel thread.
 */
export class ParallelAction extends BaseAction{

    execute(): Promise<any> {
        return Promise.all(
            this.actions.map((action) => {
                return action.execute();
            })
        );
    }

}