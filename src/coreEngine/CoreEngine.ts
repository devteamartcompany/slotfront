import {CommonModuleInitializer} from "../CommonModuleInitializer";
import {DeviceManager} from './general/utils/deviceManager/DeviceManager';
import {DeviceInfoService} from "./general/DeviceInfoService";

export class CoreEngine {

    public init (moduleInitializer: CommonModuleInitializer):void {
        DeviceManager.getInstance().init();
        DeviceInfoService.init();
        moduleInitializer.addModules();
        moduleInitializer.bindClasses();
        moduleInitializer.invokeModules();
        moduleInitializer.createFSM();
        moduleInitializer.manageMobile();
    }

}