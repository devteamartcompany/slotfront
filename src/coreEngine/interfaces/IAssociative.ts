export interface IAssociative<T> {
    [key: string]: T
}