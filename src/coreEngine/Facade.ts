import {BaseState} from "./fsm/BaseState";
import {AppLogger} from "./general/utils/loger/AppLogger";
import {AppLoggerMessageType} from "./general/utils/loger/AppLoggerMessageType";
import {IBaseModel} from "./interfaces/IBaseModel";
import {BaseController} from "./baseClasses/BaseController";
import {BaseAction} from "./fsm/BaseAction";
import {BaseServerService} from "../commonModules/server/services/BaseServerService";
import {IServerModel} from "../commonModules/server/interfaces/IServerModel";

export class Facade {

    private static _instance: Facade;
    private _serverModels: IServerModel[] = [];
    private _allModels: IBaseModel[] = [];

    static instance(): Facade {
        if (!Facade._instance) {
            Facade._instance = new Facade();
        }
        return Facade._instance;
    }

    public addServerModel(model: IServerModel): void {
        this._serverModels.push(model);
        this.addModel(model);
    }
    public addModel(model: IBaseModel): void {
        this._allModels.push(model);
    }

    public getServerModels(classInstance: any): IServerModel[] {
        if (classInstance instanceof BaseServerService) {
            return this._serverModels;
        }
    }

    public getModel(classInstance: any, modelConstructor: Function): IBaseModel {
        if (classInstance instanceof BaseState ||
            classInstance instanceof BaseController ||
            classInstance instanceof BaseAction) {
            const result: IBaseModel = this._allModels.find( (model) => {
                return model instanceof modelConstructor;
            });
            return result ;

        } else {
            AppLogger.log("only classes, derived from BaseState, or BaseController, can access to IBaseModel", AppLoggerMessageType.ERROR);
        }
    }
}