import * as EventEmitter from "eventemitter3";
import {AppLogger} from "./utils/loger/AppLogger";
import {AppLoggerMessageType} from "./utils/loger/AppLoggerMessageType";

export class EventDispatcher extends EventEmitter {
    private static _instance: EventDispatcher = new EventDispatcher();

    /**
     * Creates an instance of event dispatcher.
     */
    constructor() {
        super();
        if (EventDispatcher._instance) {
            throw new Error("Error: Instantiation failed: Use EventDispatcher.getInstance() instead of new.");
        }
        EventDispatcher._instance = this;
    }

    /**
     * Gets instance
     * @returns instance
     */
    static getInstance(): EventDispatcher {
        return this._instance;
    }

    /**
     * Dispatchs event dispatcher
     * @param event
     * @param [options]
     * @param log
     */
    public dispatch(event: string, options?: any, log: boolean = true): void {
        if (log) {
            AppLogger.log(event, AppLoggerMessageType.EVENT, options);
        }
        this.emit(event, options);
    }

}
