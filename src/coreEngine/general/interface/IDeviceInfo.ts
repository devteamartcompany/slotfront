/**
 * Information about device
 * label stands for device name
 * pattern stands for device model
 */
export interface IDeviceInfo {
    label: string,
    pattern: string
} 