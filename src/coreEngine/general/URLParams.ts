export class URLParams {

  private static urlParams: URLSearchParams;

  private static parseURL(): void {
    const url: URL = new URL(document.URL);
    this.urlParams = new URLSearchParams(url.search);
  }

  /**
   * get URL parameter by its key
   * @param key
   */
  public static getParam(key: string): string {
    if (!this.urlParams) {
      this.parseURL();
    }
    return this.urlParams.get(key);
  }

  /**
   * Returns a string representation of an url parameters.
   * Just a common util method
   * */
  public static toString(): any {
    if (!this.urlParams) {
      this.parseURL();
    }
    return this.urlParams.toString();
  }

}