import {URLParams} from './URLParams';

export class GameInfo {


    private static _title: string;
    private static _url: string;
    private static _lang: string;
    private static _currency: string;
    private static _token: string;
    private static _defaultLang: string = 'en';

    /**
     * Inits game information storage
     * Sets values from get params
     * Fetches token and notifies upon resolving
     */
    public static init(title: string) {
        this._title = title;
        const origin = window.location.origin;
        this._url = /localhost/.test(origin) || /\d+\.\d+\.\d+\.\d+/.test(origin) ? URLParams.getParam("url") : origin;
        this._token = URLParams.getParam("platform_token");

        this._currency = URLParams.getParam("platform_currency") || 'EUR';
        this._lang = (URLParams.getParam("lang") || this._defaultLang).toLowerCase();
    }

    public static get currency(): string {
        return this._currency;
    }

    public static set currency(value: string) {
        this._currency = value;
    }

    public static get lang(): string {
        return this._lang;
    }

    public static set lang(value: string) {
        this._lang = value;
    }

    public static get defaultLang(): string {
        return this._defaultLang;
    }

    public static get title(): string {
        return this._title;
    }

    public static get url(): string {
        return this._url;
    }

    public static get token(): string {
        return this._token;
    }

}
