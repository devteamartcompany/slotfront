import {GameInfo} from '../GameInfo';

export class MoneyFormatter {

    private static precision: number;
    private static currencySign: string;

    /**
     * format money value
     * @param value - money value as number
     * @param showCurrency - true as default
     * return string 2500 => $2,500.00
     */
    public static format(value: number, showCurrency: boolean = true): string {
        let result: string;
        if (showCurrency) {
            result = this.checkCurrency(value);
        } else {
            result = value.toLocaleString('en-US');
        }

        result = this.checkPrecision(result);
        return result;
    }

    /**
     * set precision - numbers of digits after dot
     * @param value
     */
    public static setPrecision(value: number): void {
        this.precision = value;
    }

    /**
     * set sign for currency
     * @param value
     */
    public static setCurrencySign(value: string): void {
        this.currencySign = value;
    }

    private static checkPrecision(value: string): string {
        if (this.precision) {
            if (value.split(".").length === 1) {
                const additionalZeros: string = "0".repeat(this.precision);
                value = value.concat("." + additionalZeros);
            } else {
                const arr: string[] = value.split(".");
                if (arr[1].length >= this.precision) {
                    arr[1].substring(0, this.precision);
                } else {
                    const additionalZerosNumber: number = this.precision - arr[1].length;
                    const additionalZeros: string = "0".repeat(additionalZerosNumber);
                    arr[1] = arr[1].concat(additionalZeros);
                }
                if (arr[1].length > 0) {
                    value = arr.join(".");
                } else {
                    value = arr[0];
                }
            }
        }
        return value;
    }

    private static checkCurrency(value: number): string {
        let result: string;
        if (this.currencySign) {
            result = value.toLocaleString('en-US');
            const letterRegExp: RegExp = /^[a-zA-Z]+$/;
            let space:string;
            if (letterRegExp.test(this.currencySign)) {
                space = " ";
            } else {
                space = "";
            }
            result = this.currencySign + space + result;
        } else {
            result = value.toLocaleString('en-US', {
                style: 'currency',
                currency: GameInfo.currency,
            });
        }
        return result;
    }

}
