export class LocalUtil {

    private static dictionary: Map<string, string>;

    /**
    * create Dictionary from loaded data
    * @param data - loaded data
    */
    public static setDictionary(data: any): void {
        const arr: any[] = Object.keys(data).map((key) => {
            return [key, data[key]];
        });
        this.dictionary = new Map<string, string>(arr);
    }

    /**
     * add data to the Dictionary
     * @param data
     */
    public static addDictionary(data: any): void {
        Object.keys(data).forEach((key) => {
            this.dictionary.set(key, data[key]);
        })
    }

    /**
    * get text from Dictionary buy key
    * @param key - key in Dictionary
    */
    public static text(key: string): string {
        let res: string = '';
        if (this.dictionary && this.dictionary.get(key)) {
            res = this.dictionary.get(key);
        }
        return res;
    }

}
