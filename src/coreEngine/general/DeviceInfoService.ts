import {IDeviceInfo} from './interface/IDeviceInfo';
import * as MobileDetect from "mobile-detect";

export class DeviceInfoService {
    private static mobileDetect: MobileDetect;
    private static _dpi: string = 'hdpi';
    private static iOS13: string = 'Macintosh';
    private static _isCanvas: boolean;
    private static _deviceInfo: IDeviceInfo;

    /**
     * Inits device info service
     */
    public static init(): void {
        this.mobileDetect = new MobileDetect(window.navigator.userAgent)
    }

    /**
     * Gets whether is landscape
     */
    public static get isLandscape(): boolean {
        return screen.height < screen.width;
    }

    /**
     * Gets whether is full screen
     */
    public static get isFullScreen(): boolean {
        return (window.innerWidth === screen.width && window.innerHeight === screen.height) || !!document.fullscreenElement;
    }

    /**
     * Gets whether is IOs
     */
    public static get isIOs(): boolean {
        return this.mobileDetect.os() == 'iOS';
    }

    /**
     * Gets whether is desktop
     */
    public static get isDesktop(): boolean {
        return !this.isMobile;
    }

    /**
     * Gets whether is tablet
     */
    public static get isTablet(): boolean {
        return !!this.mobileDetect.tablet();
    }

    /**
     * Gets whether is mobile
     */
    public static get isMobile(): boolean {
        let isMobile: boolean = false;
        isMobile = !!this.mobileDetect.mobile();
        if (window.navigator.maxTouchPoints > 1 && this.userAgent.indexOf(this.iOS13) !== -1) {
            isMobile = true;
        }
        return isMobile;
    }

    /**
     * Gets platform
     */
    public static get platform(): string {
        return this.isMobile ? 'mobile' : 'desktop';
    }

    /**
     * Gets dpi
     */
    public static get dpi(): string {
        return this._dpi;
    }

    /**
     * Gets userAgent string data
     */
    public static get userAgent(): string {
        return window.navigator.userAgent;
    }



    /**
     * Check if browser use webGL and set @param isCanvas:boolean
     */
    public static detectWebGL(): void {
        let canvas = document.createElement("canvas");
        let webGL = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
        if (webGL && webGL instanceof WebGLRenderingContext) {
            this._isCanvas = false;
        } else {
            this._isCanvas = true;
        }
    }

    /**
     * Gets whether its canvas-based
     */
    public static isCanvas(): boolean {
        return this._isCanvas;
    }

    /**
     * Gets device info data
     */
    public static getDeviceInfo(): IDeviceInfo {
        return this._deviceInfo;
    }
}
