import {DeviceManager} from "../deviceManager/DeviceManager";
import {getDeviceManager} from "../../../exports";
import {LoadEvent} from "../../../../commonModules/loading/events/LoadEvents";
import {BaseController} from "../../../baseClasses/BaseController";
import {CoreTimer} from "../renderer/CoreTimer";


export abstract class ScreenMobileManager extends BaseController {
    protected deviceManager: DeviceManager = getDeviceManager();
    protected screen: Screen | any = window.screen;
    protected canvas: HTMLElement = document.getElementById('canvas-wrap');
    protected btnFullScreen: HTMLElement = document.getElementById('full-screen-btn');
    protected lastWindowHeight: number;
    protected hideTimeout: CoreTimer;

    protected constructor() {
        super();
        this.dispatcher.addListener(LoadEvent.INIT_MOBILE_MANAGER, () => {
                this.lastWindowHeight = window.innerHeight;
                if (this.deviceManager.isMobile) {
                    if (this.deviceManager.isIOs) {
                        this.checkIframe();
                        this.btnFullScreen.addEventListener("touchend", () => {
                            this.onResize();
                            ScreenMobileManager.turnOnFullScreen();
                        });
                    } else {
                        this.btnFullScreen.addEventListener("touchend", () => {
                            this.hideBtn();
                            ScreenMobileManager.turnOnFullScreen();
                        });
                        this.btnFullScreen.addEventListener("click", () => {
                            this.hideBtn();
                            ScreenMobileManager.turnOnFullScreen();
                        });
                    }


                    if (this.screen.orientation) {
                        // only for chrome, firefox
                        // automatically enable full screen
                        this.screen.orientation.addEventListener("change", () => {
                            this.toggleFullScreen();
                        });
                    } else {
                        window.addEventListener("resize", () => {
                            this.onResize();
                        });
                    }
                    this.handleFullScreenBtn();
                }
            }
        );
    }

    protected checkIframe(): void {
        if (window.location !== window.parent.location) {
            this.hideBtn();
        }

    }

    protected onResize(): void {
        if (this.deviceManager.isIOs) {
            this.handleIOsFullScreen();
            this.checkIframe();
        } else {
            this.handleFullScreenBtn();
        }
        this.lastWindowHeight = window.innerHeight;
    }

    private handleIOsFullScreen() {
        if (this.lastWindowHeight < window.innerHeight) {
            this.hideBtn();
        }
        if (this.lastWindowHeight > window.innerHeight) {
            this.showBtn();
        }
    }

    protected handleFullScreenBtn(): void {
        if (this.deviceManager.isFullScreen) {
            this.hideBtn();
        } else {
            this.deviceManager.isLandscape ? this.showBtn() : this.hideBtn()
        }
    }

    protected showBtn() {
        this.canvas.style.pointerEvents = 'none';
        this.btnFullScreen.style.display = 'block';
    }

    protected hideBtn() {
        this.canvas.style.pointerEvents = 'auto';
        this.btnFullScreen.style.display = 'none';
    }

    protected toggleFullScreen(): void {
        this.deviceManager.isLandscape ? ScreenMobileManager.turnOnFullScreen() : ScreenMobileManager.turnOffFullScreen()
    }

    public static turnOnFullScreen(): void {
        const elem: any = document.documentElement;

        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) {
            elem.webkitRequestFullscreen();
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
    }

    public static turnOffFullScreen(): void {
        if (document.exitFullscreen) {
            document.exitFullscreen();
            // @ts-ignore
        } else if (document.mozCancelFullScreen) {
            // @ts-ignore
            document.mozCancelFullScreen();
        } else if ((document as any).webkitExitFullscreen) {
            (document as any).webkitExitFullscreen();
            // @ts-ignore
        } else if (document.msExitFullscreen) {
            // @ts-ignore
            document.msExitFullscreen();
        }
    }
}