import Sprite = PIXI.Sprite;
import * as PIXI from "pixi.js"
import {IAssociative} from "../../../interfaces/IAssociative";
import {AppLogger} from "../loger/AppLogger";
import {AppLoggerMessageType} from "../loger/AppLoggerMessageType";

export class TextureHolder {
    private static _layoutConfig: string[] = [];

    public static getLayoutTextureByName(value: string): any {
        for (let i: number = 0; i < this._layoutConfig.length; i++) {
            let configName: string = this._layoutConfig[i];
            let texture: any = PIXI.loader.resources[configName];
            if (!texture || !texture.textures || !texture.textures[value]) {
                continue;
            }
            let result: Sprite = new Sprite(texture.textures[value]);
            if (result) {
                return result;
            }
        }
        return null;
    }

    public static getTextureByName(value: string): any {
        for (let i: number = 0; i < this._layoutConfig.length; i++) {
            let configName: string = this._layoutConfig[i];
            let texture: any = PIXI.loader.resources[configName];
            let result: any = texture.textures[value];
            if (!texture.textures[value]) {
                continue;
            }
            if (result) {
                return result;
            }
        }
        return null;
    }

    public static getTextureMap(json: any, texturesCache: any): IAssociative<PIXI.Texture> {
        const resources: IAssociative<PIXI.Texture> = {};
        const imagesNames = this.getImageNames(json);

        for (const name of imagesNames) {
            const t = this.findTexture(name, texturesCache);
            if (t) {
                resources[name] = t;
            }
        }

        return resources;
    }

    static findTexture(imageName: string, texturesCache: any): PIXI.Texture {
        const exts = ["", ".png", ".jpg", ".jpeg"];

        let texture: PIXI.Texture;

        for (const ext of exts) {
            texture = texturesCache[imageName + ext];
            if (!texture) {
                texture = texturesCache[imageName + ext.toLocaleUpperCase()];
            }
            if (texture) {
                return texture;
            }
        }

        AppLogger.log(`texture "${imageName}" not found for spine`, AppLoggerMessageType.ERROR);
        return null;
    }

    static getImageNames(json: any): Array<string> {
        const images: Array<string> = [];
        if (json.hasOwnProperty("skins")) {
            const skins: any = json["skins"];

            for (const skinName of Object.keys(skins)) {
                const skin = skins[skinName];

                for (const slotName of Object.keys(skin)) {
                    const slot = skin[slotName];

                    for (const attachmentName of Object.keys(slot)) {
                        const attachment = slot[attachmentName];
                        const type: string = attachment["type"] || "region";
                        if (type === "region" || type === "mesh") {
                            let imageName: string =
                                attachment["type"]
                                || attachment["name"]
                                || attachmentName;
                            if (type === "mesh") {
                                imageName = attachmentName;
                            }
                            images.push(imageName);
                        }
                    }
                }
            }
        }
        return images;
    }

    public static setLayoutConfig(value: string): void {
        this._layoutConfig.push(value);
    }
}