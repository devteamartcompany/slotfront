import * as EventEmitter from "eventemitter3";
import {AppLogger} from "../loger/AppLogger";
import {AppLoggerMessageType} from "../loger/AppLoggerMessageType";
import {BaseState} from "../../../fsm/BaseState";
import {BaseController} from "../../../baseClasses/BaseController";
import {BaseAction} from "../../../fsm/BaseAction";
import {BaseMediator} from "../../../baseClasses/BaseMediator";

export class SlotEventDispatcher extends EventEmitter {
    private static _instance: SlotEventDispatcher = new SlotEventDispatcher();

    constructor() {
        super();
        if (SlotEventDispatcher._instance) {
            throw new Error("Error: Instantiation failed: Use SlotEventDispatcher.getInstance() instead of new.");
        }
        SlotEventDispatcher._instance = this;

    }

    static getInstance(classInstance: any): SlotEventDispatcher {
        if (classInstance instanceof BaseState ||
            classInstance instanceof BaseController ||
            classInstance instanceof BaseAction ||
            classInstance instanceof BaseMediator) {
            return this._instance;
        } else {
            AppLogger.log("only classes, derived from BaseState, BaseController, BaseAction or BaseMediator, can access to SlotEventDispatcher",
                AppLoggerMessageType.ERROR);
        }
    }

    public dispatch(event: string, options?: any): void {
        this.emit(event, options);
    }

    public emit(event: string, options?: any): boolean {
        if (!AppLogger.isProd) {
            this.logEvent(event, options);
        }
        return super.emit(event, options);
    }

    private logEvent(event: string, options?: any) {
        AppLogger.log(event, AppLoggerMessageType.EVENT, options);
    }
}