import {EnterFrameManager, IFrameListener} from "../renderer/EnterFrameManager";
import {BaseController} from "../../../baseClasses/BaseController";

export class CoreCountUp extends BaseController implements IFrameListener {

    protected _endValue: number = 0;
    protected _callback: Function;
    protected _currentTickUp: number = 0;
    protected _startValue: number;
    protected _startTime: number;
    protected _tickUpTime: number;
    protected _timeOut: number;
    protected _showCurrency: boolean = false;
    private _updateEvent: string;

    public startCountUp(start: number, end: number, callback?: Function): void {
        this._endValue = end;
        this._callback = callback;
        this._currentTickUp = start;
        if (start === end) {
            this.onComplete();
        } else {
            this._startTime = new Date().getTime();
            this.tickUpValue = start;
            this._startValue = start;
            EnterFrameManager.instance.addListener(this);
        }
    }

    public setTickUpTime(value: number): void {
        this._tickUpTime = value;
    }

    public onEnterFrame(deltaTime: number): void {
        const currentTime: number = new Date().getTime();
        const progress: number = Math.min(1, (currentTime - this._startTime) / this._tickUpTime);

        this._currentTickUp = this._startValue + ((this._endValue - this._startValue) * progress);
        this.tickUpValue = this._currentTickUp;

        if (progress === 1) {
            this.onComplete();
        }
    }

    public set showCurrency(value: boolean) {
        this._showCurrency = value;
    }

    public set updateEvent(value: string) {
        this._updateEvent = value;
    }

    protected onComplete() {
        this.killTimer();
        this.tickUpValue = this._endValue;
        if (this._callback) {
            this._callback();
        }
    }

    protected killTimer(): void {
        if (this._timeOut) {
            clearTimeout(this._timeOut);
        }
        EnterFrameManager.instance.removeListener(this);
    }

    protected set tickUpValue(value: number) {
        this.dispatcher.dispatch(this._updateEvent, value);
    }

}