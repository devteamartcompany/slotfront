import * as PIXI from "pixi.js"
import {FX} from 'revolt-fx'
import {EffectSequence, ParticleEmitter} from "revolt-fx/lib";
import {EnterFrameManager, IFrameListener} from "../renderer/EnterFrameManager";

export class ParticlEmitterContainer extends PIXI.Container  implements IFrameListener{

    protected isActive: boolean;
    protected revolt: FX;
    public emitter: ParticleEmitter;
    public sequence: EffectSequence;
    protected particlesContainer: PIXI.Container;
    private _afterTime: number = 2000;
    private _onEmitterCompletedDelegate: Function;
    private _subEmitters: ParticleEmitter[] = [];

    public create(data: any, emitterName: string, startSubEmitterName?: string, endSubEmitterName?: string): void {
        this.revolt = new FX();
        this.revolt.initBundle(data);
        this.emitter = this.revolt.getParticleEmitter(emitterName);
        this.particlesContainer = new PIXI.Container();
        this.emitter.init(this.particlesContainer);

        this.emitter.on.particleSpawned.add(particle => {

            //Register for an update signal for that particle
            particle.on.updated.add(particle => {
                if (particle.time === 0 && startSubEmitterName) {
                    const startSubEmitter: ParticleEmitter = this.revolt.getParticleEmitter(startSubEmitterName);
                    startSubEmitter.init(this.particlesContainer);
                    startSubEmitter.x = particle.x;
                    startSubEmitter.y = particle.y;
                    this._subEmitters.push(startSubEmitter);
                }
            });

            //Register for a died signal for that particle
            particle.on.died.add(particle => {
                if (endSubEmitterName) {
                    const endSubEmitter: ParticleEmitter = this.revolt.getParticleEmitter(endSubEmitterName);
                    endSubEmitter.init(this.particlesContainer);
                    endSubEmitter.x = particle.x;
                    endSubEmitter.y = particle.y;
                    this._subEmitters.push(endSubEmitter);
                }
            });
        });
        this.addChild(this.particlesContainer);
    }

    public updateEmitter() {
        this.isActive = true;
        this.emitter.update(0);
        this._subEmitters.forEach( (subEmitter) => {
            subEmitter.update(0);
        })
        EnterFrameManager.instance.addListener(this);
    }

    public updatePosition(): void {

    }

    public stopEmitter(): void {
        this.isActive = false;

        this.emitter.stop();

        this._subEmitters.forEach( (subEmitter) => {
            subEmitter.stop();
        })
        setTimeout(() => {
            EnterFrameManager.instance.addListener(this);
            this.emitter.dispose();

            this._subEmitters.forEach( (subEmitter) => {
                subEmitter.dispose();
            })
            this.revolt = null;
            this.removeChild(this.particlesContainer);
        }, this._afterTime);

    }

    public onEnterFrame(deltaTime: number): void {
        this.updatePosition();
        this.emitter.update(deltaTime);
        this._subEmitters.forEach( (subEmitter) => {
            subEmitter.update(deltaTime);
        })
    }

    private onEmitterCompleted(): void {
        if (this.onEmitterCompletedDelegate) {
            this.onEmitterCompletedDelegate();
        }
    }

    private onEmitterExhausted(): void {
        if (this.onEmitterCompletedDelegate) {
            this.onEmitterCompletedDelegate();
        }
    }

    private onParticleDied(): void {
        if (this.onEmitterCompletedDelegate) {
            this.onEmitterCompletedDelegate();
        }
    }

    get afterTime(): number {
        return this._afterTime;
    }
    set afterTime(value: number) {
        this._afterTime = value;
    }
    get onEmitterCompletedDelegate(): Function {
        return this._onEmitterCompletedDelegate;
    }

    set onEmitterCompletedDelegate(value: Function) {
        this._onEmitterCompletedDelegate = value;
    }
}
