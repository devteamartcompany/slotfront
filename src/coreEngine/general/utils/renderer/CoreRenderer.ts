/**
 * Framework logic part
 */
import {AppLogger} from "../loger/AppLogger";

export class CoreRenderer {
    public static renderer: any;
    public static stage: PIXI.Container;
    public static create(): void {
        if (document.readyState === 'complete' || document.readyState === 'interactive') {
            this.onDOMLoaded();
        } else {
            document.addEventListener('DOMContentLoaded', () => {
                this.onDOMLoaded()
            });
        }
    }

    public static pause(): void {
        PIXI.ticker.shared.stop();
    }

    public static resume(): void {
        PIXI.ticker.shared.start();
    }

    private static onDOMLoaded(): void {
        if (CoreRenderer.renderer) return;
        let app = new PIXI.Application({
            autoResize: true,
            resolution: devicePixelRatio
           // antialias: true
        });
        app.renderer.view.style.position = "absolute";
        app.renderer.view.style.display = "block";
        app.renderer.autoResize = true;
        app.renderer.resize(window.innerWidth, window.innerHeight);
        CoreRenderer.stage = app.stage;
        CoreRenderer.renderer = app.renderer;

        //Render the stage
        PIXI.ticker.shared.add(() => {
            app.renderer.render(app.stage);
        });
        app.renderer.render(app.stage);
        // this.enableFullScreen();

        document.body.querySelector('.canvas-wrap').appendChild(CoreRenderer.renderer.view);
    }

    public static onResize(): void {
        CoreRenderer.renderer.resize(window.innerWidth, window.innerHeight);
    }

    public static enableFullScreen(){
        let isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||    // alternative standard method
            ((document as any).mozFullScreen || (document as any).webkitIsFullScreen);

        let elem:any = document.documentElement as any;
        if (!isInFullScreen) {

            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.mozRequestFullScreen) { /* Firefox */
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
                elem.webkitRequestFullscreen();
            } else if (elem.msRequestFullscreen) { /* IE/Edge */
                elem.msRequestFullscreen();
            }
        }
    }

}