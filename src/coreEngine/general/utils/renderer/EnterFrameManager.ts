import {AppLogger} from "../loger/AppLogger";

export interface IFrameListener {
    onEnterFrame(deltaTime: number): void
}

export class EnterFrameManager {

    public static readonly MIN_FPS: number = 30;

    private static _instance: EnterFrameManager;
    private _listeners: Array<IFrameListener> = [];
    private _toRemove: Array<IFrameListener> = [];
    private _prevTime: number;
    private _active: boolean = false;

    static get instance(): EnterFrameManager {
        this.ensure();
        return EnterFrameManager._instance;
    }

    static activate(value: boolean): void {
        this.instance.active = value;
    }

    /**
     * Add listener to EnterFrame event. If real fps is slower, then 30,
     * then EnterFrame is invoked obligatorily with FPS 30.
     * Using requestAnimationFrame is forbiden
     * @param l
     */
    public addListener(l: IFrameListener): void {
        const removeIndex: number = this._toRemove.indexOf(l);
        if (removeIndex >= 0) {
            this._toRemove.splice(removeIndex, 1);
        }

        const listenerIndex: number = this._listeners.indexOf(l);
        if (listenerIndex === -1) {
            this._listeners.push(l);
        }
    }

    public removeListener(l: IFrameListener): void {
        const index = this._toRemove.indexOf(l);
        if (index === -1) {
            this._toRemove.push(l);
        }
    }

    private onEnterFrame(): void {
        if (!this._active) {
            return;
        }

        const currentTime = performance.now();
        const frameTime: number = (currentTime - this._prevTime) / 1000;
        this._prevTime = currentTime;

        this.animateLoop(
            frameTime,
            EnterFrameManager.MIN_FPS,
            stepTime => this.animateListeners(stepTime));
    }

    private animateLoop(frameTime: number, minFps: number, callback: (stepTime: number) => void): void {
        const minDelta: number = 1 / minFps;

        let count: number = 0;
        let deltaRest: number = frameTime;

        if (frameTime > minDelta) {
            count = Math.floor(frameTime / minDelta);
            deltaRest = frameTime - count * minDelta;
        }

        for (let c = 0; c < count; c++) {
            callback(minDelta);
        }

        if (deltaRest > 0) {
            callback(deltaRest);
        }
    }

    private animateListeners(deltaTime: number): void {
        for (let i = this._toRemove.length - 1; i >= 0; i--) {
            const index = this._listeners.indexOf(this._toRemove[i]);
            if (index >= 0) {
                this._listeners.splice(index, 1);
            }
        }
        this._toRemove.length = 0;

        for (const l of this._listeners) {
            l.onEnterFrame(deltaTime);
        }
    }

    private set active(value: boolean) {
        if (this._active !== value) {
            this._active = value;
            AppLogger.log("EnterFrameManager set active value: " + String(value));
            if (this._active) {
                this._prevTime = performance.now();
            }
        }
    }

    private static ensure(): void {
        if (!this._instance) {
            this._instance = new EnterFrameManager();

            PIXI.ticker.shared.add(() => {
                this._instance.onEnterFrame();
            });
        }
    }
}
