import {freezeApp, unfreezeApp} from "../../../exports";
import {AppLogger} from "../loger/AppLogger";
import {SoundEvents} from "../../../../commonModules/sound/events/SoundEvents";
import {EventDispatcher} from "../../EventDispatcher";

export class ChangeVisibillityUtil {
    public static activate(): void {

        // Set the name of the hidden property and the change event for visibility
        let hidden, visibilityChange;
        if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support
            hidden = "hidden";
            visibilityChange = "visibilitychange";
        } else if (typeof document["msHidden"] !== "undefined") {
            hidden = "msHidden";
            visibilityChange = "msvisibilitychange";
        } else if (typeof document["webkitHidden"] !== "undefined") {
            hidden = "webkitHidden";
            visibilityChange = "webkitvisibilitychange";
        }

        if (typeof document.addEventListener === "undefined" || hidden === undefined) {
            console.log("This demo requires a browser, such as Google Chrome or Firefox, that supports the Page Visibility API.");
        } else {
            // Handle page visibility change
            document.addEventListener(visibilityChange, () => {
                if (document[hidden]) {
                    freezeApp();
                    EventDispatcher.getInstance().dispatch(SoundEvents.FREEZE_ALL_SOUNDS, true);
                    AppLogger.log("ChangeVisibillityUtil, freezeApp");
                } else {
                    unfreezeApp();
                    EventDispatcher.getInstance().dispatch(SoundEvents.FREEZE_ALL_SOUNDS, false);
                }
            }, false);
        }
    }
}