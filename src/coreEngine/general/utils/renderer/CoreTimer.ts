import { AppLogger } from '../loger/AppLogger';

/**
 * Framework logic part
 */
export class CoreTimer {

    private static timers: CoreTimer[] = [];
    private static isPaused: boolean = false;
    private timerId;
    private startId: number;
    private remainingId: number;

    constructor(private callback: Function, delay: number, private args:any[]) {
        this.startId = this.remainingId = delay;
        this.resume();
    }

    private destroy(): void {
        this.pause();
    }

    private pause(): void {
        this.timerId && clearTimeout(this.timerId);
        this.remainingId -= Date.now() - this.startId;
    }

    private resume(): void {
        this.startId = Date.now();
        this.timerId && clearTimeout(this.timerId);
        AppLogger.log(` COreTimer started: ${this.args.toString()}`);
        this.timerId = setTimeout( (...args) => {
            CoreTimer.kill(this);

            AppLogger.log(` COreTimer ended: ${this.args.toString()}`);
            this.callback(args)
            }, this.remainingId, this.args);
    }

    /**
     * create new CoreTimer instance and start it immediately
     * setTimeout or setInterval is forbidden
     * @param callback - callback function
     * @param delay - delay in ms
     * @param args
     */
    public static  start(callback: Function, delay: number, ...args): CoreTimer {
        const timer: CoreTimer = new CoreTimer(callback, delay, args);
        this.timers.push(timer);
        return timer;
    }

    /**
     * pause all timers
     */
    public static pauseAll(): void {
        if (CoreTimer.isPaused) {
            return;
        }
        CoreTimer.isPaused = true;
        this.timers.forEach( (timer) => {
            timer.pause();
        })
    }

    /**
     * resume all timers
     */
    public static resumeAll(): void {
        if (!CoreTimer.isPaused) {
            return;
        }
        CoreTimer.isPaused = false;
        this.timers.forEach( (timer) => {
            timer.resume();
        })
    }

    /**
     * kill timer, clear memory
     * @param timer
     */
    public static kill(timer: CoreTimer): void {
        const index: number = this.timers.indexOf(timer);
        this.timers.splice(index, 1);
        timer.destroy();
    }

    /**
     * skip timer, call its callback
     * @param timer
     */
    public static skip(timer: CoreTimer): void {
        timer.callback();
        this.kill(timer);
    }

}