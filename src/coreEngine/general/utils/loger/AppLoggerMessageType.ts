export const AppLoggerMessageType = {
    CLEAR: "clear",
    ERROR: "error",
    EVENT: "event",
    SUCCESS: "success",
    DEFAULT: "default",
    BackgroundOptions: {
        error: "background: red;color: white",
        event: "background: blue;color: white",
        success: "background: green; color: white",
        default: ""
    }
}