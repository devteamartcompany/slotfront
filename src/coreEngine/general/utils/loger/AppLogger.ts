import {AppLoggerMessageType} from "./AppLoggerMessageType";

export class AppLogger {
    public static isProd: boolean = false;

    public static log(message: string, messageType?: string, obj?: any): void {
        if (this.isProd) {
            return;
        }
        switch (messageType) {
            case AppLoggerMessageType.CLEAR:
                console.clear();
                break;
            case AppLoggerMessageType.ERROR:
                this.sendMessage(message, AppLoggerMessageType.BackgroundOptions.error, obj);
                break;
            case AppLoggerMessageType.EVENT:
                this.sendMessage(message, AppLoggerMessageType.BackgroundOptions.event, obj);
                break;
            case AppLoggerMessageType.SUCCESS:
                this.sendMessage(message, AppLoggerMessageType.BackgroundOptions.success, obj);
                break;
            default:
                this.sendMessage(message, AppLoggerMessageType.BackgroundOptions.default, obj);
                break;
        }
    }

    private static sendMessage(message: string, options, obj: any) {
        if (obj) {
            console.debug(message, options, obj);
        } else {
            console.log('%c' + message, options);
        }
    }
}

