import {freezeApp, unfreezeApp} from "../../../exports";
import {AppLogger} from "../loger/AppLogger";

export class DeviceManager {

    readonly mobileDetectedRules: string[] = ["Android", "Touch", "Mobile", "webOS", "iPhone", "iPad", "BlackBerry", "IEMobile", "Opera Mini"];
    readonly iDevices = [
        'iPad Simulator',
        'iPhone Simulator',
        'iPod Simulator',
        'iPad',
        'iPhone',
        'iPod'
    ];
    private static _instance: DeviceManager = new DeviceManager();
    private _isMobile: boolean = false;
    private _isDesktop: boolean = false;
    private _isFirefox: boolean = false;
    private _isChrome: boolean = false;
    private _isIOs: boolean = false;
    private _isSamsungBrowser: boolean = false;
    private _dpi: string = "hdpi";
    // private gameLogo: any;
    private _isFreezed: boolean;

    constructor() {
        if (DeviceManager._instance) {
            throw new Error("Error: Instantiation failed: Use DeviceManager.getInstance() instead of new.");
        }
        DeviceManager._instance = this;
    }

    static getInstance(): DeviceManager {
        return this._instance;
    }

    public init() {
        this._isMobile = this.detectMobile();
        this._isDesktop = !this._isMobile;
        this._isIOs = this.detectIOs();
        this._isChrome = this.detectChrome();
        this._isFirefox = this.detectFirefox();
        this._isSamsungBrowser = this.detectSamsungBrowser();
    }

    private detectMobile(): boolean {
        for (let i = 0; i < this.mobileDetectedRules.length; i++) {
            if (window.navigator.userAgent.match(this.mobileDetectedRules[i])) {
                return true;
            }
        }
        return false;
    }

    private detectIOs(): boolean {
        if (!!navigator.platform) {
            while (this.iDevices.length) {
                if (navigator.platform === this.iDevices.pop()) {
                    return true;
                }
            }
        }

        return false;
    }

    private detectChrome(): boolean {
        return (/Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor)) || /CriOS/.test(navigator.userAgent);
    }

    private detectFirefox(): boolean {
        return navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    }

    private detectSamsungBrowser(): boolean {
        return navigator.userAgent.toLowerCase().indexOf('samsung') > -1;
    }

    /*public setGameLogoVisibility(value: boolean): void {
        let element = document.getElementById("gameLogo");
        element.hidden = !value;
    }*/

    public showRotateDeviceMessage(): void {
        AppLogger.log("showRotateDeviceMessage, isLandscape: " + String(this.isLandscape));
        if (this.isLandscape) {
            if (this._isFreezed) {
                this._isFreezed = false;
                unfreezeApp();
                AppLogger.log("showRotateDeviceMessage, unfreezeApp");
            }

        } else {
            if (!this._isFreezed) {
                this._isFreezed = true;
                freezeApp();
                AppLogger.log("showRotateDeviceMessage, freezeApp");
            }
        }
    }

    public get isSamsungBrowser(): boolean {
        return this._isSamsungBrowser;
    }

    public get isLandscape(): boolean {
        if (window.orientation) {
            return window.orientation === 90 || window.orientation === -90;
        } else {
            return screen.height < screen.width;
        }

    }

    public get isFullScreen(): boolean {
        return (window.innerWidth === screen.width && window.innerHeight === screen.height) || !!document.fullscreenElement;
    }

    public get isIOs(): boolean {
        return this._isIOs;
    }

    public get isChrome(): boolean {
        return this._isChrome;
    }

    public get isFirefox(): boolean {
        return this._isFirefox;
    }

    public get isDesktop(): boolean {
        return this._isDesktop;
    }

    public get isMobile(): boolean {
        return this._isMobile;
    }

    public get dpi(): string {
        return this._dpi;
    }

    public getWidth() {
        return window.innerWidth;
    }

    public getHeight() {
        return window.innerHeight;
    }
}