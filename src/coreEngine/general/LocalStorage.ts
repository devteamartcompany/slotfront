import {AppLogger} from "./utils/loger/AppLogger";
import {AppLoggerMessageType} from "./utils/loger/AppLoggerMessageType";

export class LocalStorage {
    /**
     * Empties the list associated with the object of all key/value pairs, if there are any.
     */
    public static clear(): void {
        this.storage && this.storage.clear();
    }

    /**
     * value = storage[key]
     */
    public static getItem(key: string): string | null {
        return this.storage ?
            this.storage.getItem(key) :
            null;
    }

    /**
     * Returns the name of the nth key in the list, or null if n is greater
     * than or equal to the number of key/value pairs in the object.
     */
    public static key(index: number): string | null {
        return this.storage ?
            this.storage.key(index) :
            null;
    }

    /**
     * delete storage[key]
     */
    public static removeItem(key: string): void {
        this.storage && this.storage.removeItem(key);
    };

    /**
     * storage[key] = value
     */
    public static setItem(key: string, value: string): void {
        try {
            this.storage.setItem(key, value);
        } catch (domException) {
            if (domException.name === 'QuotaExceededError' ||
                domException.name === 'NS_ERROR_DOM_QUOTA_REACHED') {
                AppLogger.log("Browser local storage capacity is full", AppLoggerMessageType.ERROR);
            }
        }
    };

    /**
     * Gets storage
     */
    public static get storage(): Storage {
        return this.supportsLocalStorage() ? window.localStorage : null;
    }

    private static supportsLocalStorage(): boolean {
        try {
            return 'localStorage' in window && window['localStorage'] !== null;
        } catch (e) {
            return false;
        }
    }

}
