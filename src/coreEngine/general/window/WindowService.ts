import { WindowEvents } from './WindowEvents';
import { EventDispatcher } from '../EventDispatcher';

declare const document: any;

export class WindowService {

    private static dispatcher = EventDispatcher.getInstance();

    /**
     * Inits window service
     */
    public static init(): void {
        if (document.readyState === 'complete' || document.readyState === 'interactive') {
            this.dispatcher.dispatch(WindowEvents.DOM_ELEMENT_LOADED);
        } else {
            document.addEventListener('DOMContentLoaded', () => this.dispatcher.dispatch(WindowEvents.DOM_ELEMENT_LOADED));
        }
        const resize = () => this.dispatcher.dispatch(WindowEvents.RESIZE, {
            width: this.getWidth(),
            height: this.getHeight()
        });
        resize();
        window.addEventListener("resize", () => setTimeout(resize, 250));
    }

    /**
     * Enables full screen
     */
    public static enableFullScreen() {
        const elem: any = document.documentElement;
        if (!this.isFullScreen) {
            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.mozRequestFullScreen) { /* Firefox */
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
                elem.webkitRequestFullscreen();
            } else if (elem.msRequestFullscreen) { /* IE/Edge */
                elem.msRequestFullscreen();
            }
        }
    }

    /**
     * Disables full screen
     */
    public static disableFullScreen() {

    }

    /**
     * Gets whether is full screen
     */
    public static get isFullScreen(): boolean {
        return (document.fullscreenElement && document.fullscreenElement !== null) || (document.mozFullScreen || document.webkitIsFullScreen);
    }

    /**
     * Gets window width
     * @returns width
     */
    public static getWidth(): number {
        return window.innerWidth;
    }

    /**
     * Gets window height
     * @returns height
     */
    public static getHeight(): number {
        return window.innerHeight;
    }
}
