export const WindowEvents = {
  /**
   * @DOM_ELEMENT_LOADED
   * DOM Element loaded event
   */
  DOM_ELEMENT_LOADED: "WindowEvents.DOM_ELEMENT_LOADED",
  /**
   * @WINDOW_RESIZE
   * Window resize event
   */
  RESIZE: "WindowEvents.WINDOW_RESIZE",
}