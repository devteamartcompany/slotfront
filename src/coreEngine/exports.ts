import {Kernel} from "./bindSystem/Kernel";
import {Facade} from "./Facade";
import {IBaseModel} from "./interfaces/IBaseModel";
import {SlotEventDispatcher} from "./general/utils/eventDispatcher/SlotEventDispatcher";
import {DeviceManager} from "./general/utils/deviceManager/DeviceManager";
import {CoreTimer} from "./general/utils/renderer/CoreTimer";
import {CoreRenderer} from "./general/utils/renderer/CoreRenderer";
import {EnterFrameManager} from "./general/utils/renderer/EnterFrameManager";
import {TweenMax} from "gsap";
import {ViewComponentsFactory} from "./viewComponents/factories/ViewComponentsFactory";
import {DynamicViewCreator} from "./viewComponents/factories/DynamicViewCreator";
import {IViewComponent} from "./viewComponents/interfaces/IViewComponent";
import {SpineFactory} from "./viewComponents/spine/SpineFactory";
import {BaseSpineComponent} from "./viewComponents/BaseSpineComponent";
import {IServerModel} from "../commonModules/server/interfaces/IServerModel";

export function createInstance(constructor: Function, args?: any[]): any {
    return Kernel.createInstance(constructor, args);
}

export function bindTo(oldConstructor: Function, newConstructor: Function): void {
    Kernel.bindTo(oldConstructor, newConstructor);
}

export function bindMediator(mediatorClass: Function, viewClass: Function, layoutID: string, holderID: string = ""): void {
    DynamicViewCreator.bindMediator(mediatorClass, viewClass, layoutID, holderID);
}

export function getModel(classInstance: any, modelConstructor: Function): IBaseModel {
    return Facade.instance().getModel(classInstance, modelConstructor);
}

export function addModel(model: IBaseModel): void {
    Facade.instance().addModel(model);
}

export function addServerModel(model: IServerModel): void {
    Facade.instance().addServerModel(model);
}

export function getDispatcher(instance: any): SlotEventDispatcher {
    return SlotEventDispatcher.getInstance(instance);
}

export function getDeviceManager(): DeviceManager {
    return DeviceManager.getInstance();
}

export function getViewComponent(layoutID: string): IViewComponent {
    return ViewComponentsFactory.getInstance().getViewComponent(layoutID);
}

export function createNewView(layoutID: string): IViewComponent {
    return ViewComponentsFactory.getInstance().createNewView(layoutID);
}

export function getSpineClip(spineID: string): BaseSpineComponent {
    return SpineFactory.getInstance().getSpineClip(spineID);
}

export function freezeApp() {
   /* CoreTimer.pauseAll();
    CoreRenderer.pause();
    EnterFrameManager.activate(false);
    TweenMax.pauseAll();*/
}

export function unfreezeApp() {
    CoreTimer.resumeAll();
    CoreRenderer.resume();
    EnterFrameManager.activate(true);
    TweenMax.resumeAll();
}
