import {SlotEventDispatcher} from "../general/utils/eventDispatcher/SlotEventDispatcher";
import {getDispatcher} from "../exports";

/**
 * Base controller for calculating game data or for coreEngine job.
 * Only controllers, states have access to models.
 * Use dispatcher to work with actions.
 * Use getModel() to get access to models.
 */
export class BaseController  {
    protected dispatcher: SlotEventDispatcher;
    constructor() {
        this.dispatcher = getDispatcher(this);
        this.addListeners();
    }

    protected addListeners(): void {

    }
}