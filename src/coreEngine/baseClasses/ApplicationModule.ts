/**
 * Base application module.
 * We use class instead of interface cause typescript instanceof works only with classes
 * You should create or bind new classes only here.
 * Then add module in ApplicationModuleInitializer class.
 */
export class ApplicationModule {

    /**
     * Create new entities here.
     * use createInstance(MyClass); instead of new(MyClass);
     */
    public execute(): void {

    }

    /**
     * You can bind your application classes to a coreEngine classes.
     * So finally your application class will be used instead of coreEngine class.
     * Example:
     * bindTo(CoreClass, ApplicationClass);
     */
    public bindClasses(): void {

    }
}