import {SlotEventDispatcher} from "../general/utils/eventDispatcher/SlotEventDispatcher";
import {getDispatcher} from "../exports";
import {BaseView} from "./BaseView";
import {LoadEvent} from "../../commonModules/loading/events/LoadEvents";

/**
 * Mediator is a header for views.
 * The Mediator is used to work with actions.
 * Use dispatcher for it.
 *
 */
export class BaseMediator {
    protected dispatcher: SlotEventDispatcher;
    protected view: BaseView;
    protected isInitialized: boolean = false;

    constructor(view: BaseView) {
        this.dispatcher = getDispatcher(this);
        this.view = view;
        this.addListeners();
    }

    public addListeners(): void {
        this.dispatcher.addListener(LoadEvent.ASSET_LOADED, this.init.bind(this));
    }

    private init(groupID: string): void {
        if (this.view.groupID.indexOf(groupID) !== -1 && !this.isInitialized) {
            this.view.init();
            this.isInitialized = true;
        }
    }

    protected registerViewListener(uiEvent: string): void {
        this.view.addListener(uiEvent, (args) => {
            this.dispatcher.dispatch(uiEvent, args);
        });
    }

    protected removeListeners(): void {

    }
}