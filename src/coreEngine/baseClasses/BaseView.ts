import * as PIXI from "pixi.js"
import * as EventEmitter from "eventemitter3";
import {BaseComponent} from "../viewComponents/interfaces/BaseComponent";

export class BaseView extends EventEmitter {
    public groupID: string;
    public sourceType: string;
    public layoutID: string;

    constructor(protected source: BaseComponent, protected holder: BaseComponent) {
        super();
        this.groupID = source.groupID;
        this.layoutID = source.layoutID;
        this.sourceType = source.sourceData.type;
    }

    public init(): void {
        this.initSourceView(this.source);
        this.initSourceData();
    }

    public show(): void {
        this.source.visible = true;
    }

    public hide(): void {
        this.source.visible = false;
    }

    protected initSourceData(): void {

    }

    protected initSourceView(source: BaseComponent): void {
        source.init();
        if (this.holder) {
            this.holder.addChild(this.source);
        }
    }

    protected destroy(): void {

    }
}
