import {ApplicationModule} from "./coreEngine/baseClasses/ApplicationModule";
import {LoadingModule} from "./commonModules/loading/LoadingModule";
import {SoundModule} from "./commonModules/sound/SoundModule";
import {CommonFSMInitializer} from "./CommonFSMInitializer";
import {createInstance} from "./coreEngine/exports";
import {PreloaderModule} from "./commonModules/preloader/PreloaderModule";
import {RootViewModule} from "./commonModules/rootView/RootViewModule";
import {RenderModule} from "./commonModules/renderModule/RenderModule";
import {SpineModule} from "./coreEngine/viewComponents/spine/SpineModule";
import {ScreenMobileManager} from "./coreEngine/general/utils/ScreenMobileManager";
import {LobbyLoginModule} from "./gameModules/ui/LobbyLoginModule";
import {ServerModule} from "./commonModules/server/ServerModule";

export class CommonModuleInitializer {

    protected _modules: ApplicationModule[] = [];

    /**
     * Add application logic commonModules. All new application entities should be
     * created only in module execute() method.
     */
    public addModules(): void {
        this.addModule(new LoadingModule());
        this.addModule(new RenderModule());
        this.addModule(new ServerModule());
        this.addModule(new SoundModule());
        this.addModule(new SpineModule());
        this.addModule(new PreloaderModule());
        this.addModule(new RootViewModule());
        this.addModule(new LobbyLoginModule());
    }

    protected addModule(module: ApplicationModule): void {
        this._modules.push(module);
    }

    /**
     * You can bind your application classes to a coreEngine classes.
     * So finally your application class will be used instead of coreEngine class.
     */
    public bindClasses(): void {
        this._modules.forEach((module) => {
            module.bindClasses();
        });
    }

    public invokeModules(): void {
        this._modules.forEach((module) => {
            module.execute();
        });
    }

    public createFSM(): void {
        const commonFSMInitializer: CommonFSMInitializer = createInstance(CommonFSMInitializer);
        commonFSMInitializer.createFSM();
        commonFSMInitializer.addStates();
        commonFSMInitializer.startFSM();
    }

    public manageMobile(): void {
        createInstance(ScreenMobileManager);
    }

}