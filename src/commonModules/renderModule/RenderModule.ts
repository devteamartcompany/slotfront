import {ApplicationModule} from "../../coreEngine/baseClasses/ApplicationModule";
import {bindMediator, createInstance} from "../../coreEngine/exports";
import {RenderManager} from "./RenderManager";
import {SafeAreaView} from "./SafeAreaView";
import {SafeAreaMediator} from "./SafeAreaMediator";

export class RenderModule extends ApplicationModule {

    public execute(): void {
        super.execute();
        bindMediator(SafeAreaMediator, SafeAreaView, "safeArea");
        createInstance(RenderManager);
    }
}