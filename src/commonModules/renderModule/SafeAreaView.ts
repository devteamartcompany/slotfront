import {BaseView} from "../../coreEngine/baseClasses/BaseView";
import {BaseRectangle} from "../../coreEngine/viewComponents/BaseRectangle";
import {createNewView} from "../../coreEngine/exports";

export class SafeAreaView extends BaseView {
    private safeArea: BaseRectangle;

    public init(): void {
        super.init();
        this.safeArea = createNewView("safeAreaRectangle") as BaseRectangle;
    }

    public getSafeArea(): BaseRectangle {
        return this.safeArea;
    }

}