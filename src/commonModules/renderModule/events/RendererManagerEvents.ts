export const RendererManagerEvents = {
    RESIZE: "CoreRendererEvents.RESIZE",
    INIT_SAFE_AREA: "CoreRendererEvents.INIT_SAFE_AREA",
    GET_SAFE_AREA_VIEW: "CoreRendererEvents.GET_SAFE_AREA_VIEW",
    SET_SAFE_AREA_VIEW: "CoreRendererEvents.SET_SAFE_AREA_VIEW"
}