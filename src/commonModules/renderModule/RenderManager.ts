import {BaseController} from "../../coreEngine/baseClasses/BaseController";
import {RendererManagerEvents} from "./events/RendererManagerEvents";
import {DeviceManager} from "../../coreEngine/general/utils/deviceManager/DeviceManager";
import {getDeviceManager} from "../../coreEngine/exports";
import {CoreRenderer} from "../../coreEngine/general/utils/renderer/CoreRenderer";
import {BaseRectangle} from "../../coreEngine/viewComponents/BaseRectangle";
import {IRenderViewParams} from "./IRenderViewParams";
import {LoadEvent} from "../loading/events/LoadEvents";
import {AppLogger} from "../../coreEngine/general/utils/loger/AppLogger";
import {EventDispatcher} from "../../coreEngine/general/EventDispatcher";

export class RenderManager extends BaseController {
    protected deviceManager: DeviceManager = getDeviceManager();

    constructor() {
        super();
        this.dispatcher.addListener(RendererManagerEvents.SET_SAFE_AREA_VIEW, this.calculateViewPosition.bind(this));

        this.dispatcher.addListener(LoadEvent.INITIAL_LOADING_COMPLETE, () => {

            this.dispatcher.dispatch(RendererManagerEvents.GET_SAFE_AREA_VIEW);
        });
        this.dispatcher.addListener(LoadEvent.INIT_LOADER, () => {
            this.addWindowResizeListener();
        });
    }

    protected addWindowResizeListener(): void {
        window.addEventListener("resize", () => {
            this.dispatcher.dispatch(RendererManagerEvents.GET_SAFE_AREA_VIEW);
        });
    }

    private calculateViewPosition(safeArea: BaseRectangle): void {
        CoreRenderer.onResize();
        if(this.deviceManager.isMobile) {
            AppLogger.log('calculateViewPosition ' )
            this.deviceManager.showRotateDeviceMessage();
        }
        let params: IRenderViewParams = new class implements IRenderViewParams {
            height: number;
            positionX: number;
            positionY: number;
            scale: number;
            width: number;
        };

        let newWidth: number;
        let newHeight: number;
        let maxWidth: number = safeArea.sourceData.width;
        let maxHeight: number = safeArea.sourceData.height;
        let currentWidth: number = this.deviceManager.getWidth();
        let currentHeight: number = this.deviceManager.getHeight();
        const ratio: number = maxWidth / maxHeight;
        const currRatio: number = currentWidth / currentHeight;
        let scaleTo: number;

        let newX: number = 0;
        let newY: number = 0;

        if (currRatio < ratio) {
            newWidth = Math.max(maxWidth, currentWidth);
            newHeight = newWidth / ratio;
            scaleTo = currentWidth / maxWidth;
            newY = (currentHeight - maxHeight * scaleTo) / 2;
        } else {
            newHeight = Math.max(maxHeight, currentHeight);
            newWidth = newHeight * ratio;
            scaleTo = currentHeight / maxHeight;
            newX = (currentWidth - maxWidth * scaleTo)/2 ;
        }
        params.height = newWidth;
        params.width = newHeight;
        params.scale = scaleTo;

        newX = scaleTo * safeArea.sourceData.posX / 2 + newX;
        newY = scaleTo * safeArea.sourceData.posY / 2 + newY;

        params.positionX = newX;
        params.positionY = newY;
        this.dispatcher.dispatch(RendererManagerEvents.RESIZE, params);
        EventDispatcher.getInstance().dispatch(RendererManagerEvents.RESIZE, params);
    }
}