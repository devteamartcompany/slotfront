export interface IRenderViewParams {
    scale: number,
    positionX: number,
    positionY: number,
    width: number,
    height: number
}