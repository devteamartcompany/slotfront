import {BaseMediator} from "../../coreEngine/baseClasses/BaseMediator";
import {SafeAreaView} from "./SafeAreaView";
import {RendererManagerEvents} from "./events/RendererManagerEvents";

export class SafeAreaMediator extends BaseMediator {
    protected view: SafeAreaView;

    addListeners(): void {
        super.addListeners();
        this.dispatcher.addListener(RendererManagerEvents.GET_SAFE_AREA_VIEW, this.getSafeArea.bind(this));
        this.dispatcher.addListener(RendererManagerEvents.INIT_SAFE_AREA, this.view.init.bind(this));
    }

    private getSafeArea(): void {
        this.dispatcher.dispatch(RendererManagerEvents.SET_SAFE_AREA_VIEW, this.view.getSafeArea());
    }
}