import {BaseAction} from "../../../coreEngine/fsm/BaseAction";
import {CoreRenderer} from "../../../coreEngine/general/utils/renderer/CoreRenderer";
import {unfreezeApp} from "../../../coreEngine/exports";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";
import {ChangeVisibillityUtil} from "../../../coreEngine/general/utils/renderer/ChangeVisibillityUtil";
import {LoadEvent} from "../events/LoadEvents";

export class InitAppAction extends BaseAction {
    execute(): Promise<any> {
        AppLogger.log(`${this.constructor.name}`);
        CoreRenderer.create();
        ChangeVisibillityUtil.activate();
        this.dispatcher.dispatch(LoadEvent.INIT_MOBILE_MANAGER);
        unfreezeApp();
        return Promise.resolve();
    }
}