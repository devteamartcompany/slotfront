import {BaseAction} from "../../../coreEngine/fsm/BaseAction";
import {DynamicViewCreator} from "../../../coreEngine/viewComponents/factories/DynamicViewCreator";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";

export class DynamicViewCreateAction extends BaseAction{
    execute(): Promise<any> {
        AppLogger.log(`${this.constructor.name}`);
        DynamicViewCreator.createViews();
        return Promise.resolve();
    }
}