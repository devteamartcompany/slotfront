import {BaseAction} from "../../../coreEngine/fsm/BaseAction";
import {LoadEvent} from "../events/LoadEvents";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";

export class LoadConfigAction extends BaseAction{

    /**
     * Dispatch event to load configs and listen for event about finish of loading
     */
    execute(): Promise<any> {
        AppLogger.log(`${this.constructor.name}`);
        return new Promise<any>((resolve) => {
            this.dispatcher.addListener(LoadEvent.CONFIG_LOADED, () => {
                resolve();
            });
            this.dispatcher.dispatch(LoadEvent.LOAD_CONFIGS);

        });
    }
}