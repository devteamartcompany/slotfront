import {BaseAction} from "../../../coreEngine/fsm/BaseAction";
import {LoadEvent} from "../events/LoadEvents";
import {LoaderConstants} from "../constants/LoaderConstants";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";

export class LoadAssetLazyAction extends BaseAction{
    execute(): Promise<any> {
        AppLogger.log(`${this.constructor.name}`);
        return new Promise<any>((resolve) => {
            this.dispatcher.dispatch(LoadEvent.LOAD_ASSETS, LoaderConstants.LAZY_PRIORITY);
            resolve();
        });
    }
}