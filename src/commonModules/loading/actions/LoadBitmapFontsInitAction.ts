import {BaseAction} from "../../../coreEngine/fsm/BaseAction";
import {LoadEvent} from "../events/LoadEvents";
import {LoaderConstants} from "../constants/LoaderConstants";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";

export class LoadBitmapFontsInitAction extends BaseAction {
    execute(): Promise<any> {
        AppLogger.log(`${this.constructor.name}`);
        return new Promise<any>((resolve) => {
            this.dispatcher.dispatch(LoadEvent.RESET_LOADER_VIEW);
            this.dispatcher.once(LoadEvent.BITMAP_FONTS_LOADED, () => {
                resolve();
            });
            this.dispatcher.dispatch(LoadEvent.LOAD_BITMAP_FONTS, LoaderConstants.INITIAL_PRIORITY);
        });
    }
}