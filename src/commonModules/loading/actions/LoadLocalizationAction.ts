import {BaseAction} from "../../../coreEngine/fsm/BaseAction";
import {LoadEvent} from "../events/LoadEvents";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";

export class LoadLocalizationAction extends BaseAction{
    execute(): Promise<any> {
        AppLogger.log(`${this.constructor.name}`);
        return new Promise<any>((resolve) => {
            this.dispatcher.addListener(LoadEvent.LOCALIZATION_LOADED, () => {
                resolve();
            });
            this.dispatcher.dispatch(LoadEvent.LOAD_LOCALIZATION);
        });
    }

}