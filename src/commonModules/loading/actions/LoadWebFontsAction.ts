import * as FontFaceObserver from "fontfaceobserver";
import {BaseAction} from "../../../coreEngine/fsm/BaseAction";

export class LoadWebFontsAction extends BaseAction {
    execute(): Promise<any> {
        return new Promise<any>((resolve) => {
            if( window.navigator.userAgent.indexOf("Edge") > -1 ) {
                resolve();
            }

            let sheet = document.styleSheets;
            const observers: FontFaceObserver[] = [];
            for (let i: number = 0; i < sheet.length; i++) {
                let rule = sheet[i]["rules"] || sheet[i]["cssRules"];
                if (!rule) {
                    continue;
                }
                for (let j: number = 0; j < rule.length; j++) {
                    if (rule[j].constructor.name === 'CSSFontFaceRule') {
                        let fontFamily: string = (rule[j].style.cssText.match(/font-family: *([^;]+);/i))[1].toString().replace('"', '').replace('"', '');
                        let fontWeight: string = (rule[j].style.cssText.match(/font-weight: *([^;]+);/i) || ["", ""])[1].toString();
                        let fontStyle: string = (rule[j].style.cssText.match(/font-style: *([^;]+);/i) || ["", ""])[1].toString();
                        observers.push(new FontFaceObserver(fontFamily, {
                            weight: fontWeight,
                            style: fontStyle
                        }));
                    }
                }
            }
            if (observers.length > 0) {
                Promise.all(observers.map((observer) => {
                    console.log(observer)
                    return observer.load(null, 200000);
                })).then(() => {
                    resolve();
                });
            } else {
                resolve();
            }
        });
    }
}
