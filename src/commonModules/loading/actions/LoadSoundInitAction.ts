import {BaseAction} from "../../../coreEngine/fsm/BaseAction";
import {LoadEvent} from "../events/LoadEvents";
import {LoaderConstants} from "../constants/LoaderConstants";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";

export class LoadSoundInitAction extends BaseAction{
    execute(): Promise<any> {
        AppLogger.log(`${this.constructor.name}`);
        return new Promise<any>((resolve) => {
            this.dispatcher.addListener(LoadEvent.SOUND_LOADED, () => {
                resolve();
            });
            this.dispatcher.dispatch(LoadEvent.LOAD_SOUNDS, LoaderConstants.INITIAL_PRIORITY);
        });
    }
}