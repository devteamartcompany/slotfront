import {BaseController} from "../../../coreEngine/baseClasses/BaseController";
import {ConfigLoader} from "../loaders/ConfigLoader";
import {AssetsLoader} from "../loaders/AssetsLoader";
import {SoundLoader} from "../loaders/SoundLoader";
import {createInstance, getModel} from "../../../coreEngine/exports";
import {LoadEvent} from "../events/LoadEvents";
import {ConfigPathData} from "../../../ConfigPathData";
import {GameConfig} from "../model/GameConfig";
import {BitmapFontsLoader} from "../loaders/BitmapFontsLoader";
import {LoaderModel} from "../model/LoaderModel";

export class LoadManager extends BaseController {

    protected configLoader: ConfigLoader;
    protected assetsLoader: AssetsLoader;
    protected soundLoader: SoundLoader;
    protected bitmapFontsLoader: BitmapFontsLoader;

    constructor() {
        super();
        this.configLoader = createInstance(ConfigLoader);
        this.assetsLoader = createInstance(AssetsLoader);
        this.soundLoader = createInstance(SoundLoader);
        this.bitmapFontsLoader = createInstance(BitmapFontsLoader);
    }

    protected addListeners() {
        this.dispatcher.addListener(LoadEvent.LOAD_LOCALIZATION, this.loadI18N.bind(this));
        this.dispatcher.addListener(LoadEvent.LOAD_CONFIGS, this.loadConfigs.bind(this));
        this.dispatcher.addListener(LoadEvent.LOAD_ASSETS, this.loadAssets.bind(this));
        this.dispatcher.addListener(LoadEvent.LOAD_BITMAP_FONTS, this.loadBitmapFonts.bind(this));
        this.dispatcher.addListener(LoadEvent.LOAD_SOUNDS, this.loadSounds.bind(this));
        this.dispatcher.addListener(LoadEvent.FORCE_LOAD, this.forceLoad.bind(this));
    }

    protected loadConfigs(): void {
        Promise.all([
            this.configLoader.loadAssetsConfig(ConfigPathData.LOADING_CONFIG_PATH),
            this.configLoader.loadBitmapFontsConfig(ConfigPathData.BITMAP_FONTS_CONFIG_PATH),
            this.configLoader.loadSoundConfig(ConfigPathData.SOUND_CONFIG_PATH),
            this.configLoader.loadGameConfig(ConfigPathData.GAME_CONFIG_PATH),
            this.configLoader.loadGameParams(ConfigPathData.GAME_PARAMS_PATH),
            this.configLoader.loadViewConfig(ConfigPathData.VIEW_CONFIG_PATH)
        ]).then(() => {
            this.dispatcher.dispatch(LoadEvent.CONFIG_LOADED);
        })
    }

    protected loadAssets(priority: string): void {
        this.assetsLoader.load(priority).then(() => {
            this.dispatcher.dispatch(LoadEvent.ASSET_LOADED, priority);
        })
    }

    protected loadBitmapFonts(priority: string): void {
        this.bitmapFontsLoader.load(priority).then(() => {
            this.dispatcher.dispatch(LoadEvent.BITMAP_FONTS_LOADED, priority);
        })
    }

    protected forceLoad(group: string): void {
        Promise.all([
            this.forceLoadAssets(group),
            this.forceLoadBitmaps(group),
            this.forceLoadSounds(group)
        ]).then(() => {
            this.dispatcher.dispatch(LoadEvent.FORCE_LOAD_COMPLETE)
        });
    }

    private forceLoadAssets(groupName: string): Promise<any> {
        const loaderModel: LoaderModel = getModel(this, LoaderModel) as LoaderModel;
        if (loaderModel.isAssetsLoaded(groupName)) {
            return Promise.resolve();
        }
        return new Promise<any>((resolve) => {
            this.dispatcher.once(LoadEvent.ASSET_LOADED, () => {
                resolve();
            });
            this.assetsLoader.forceLoad(groupName);
        })
    }

    private forceLoadBitmaps(groupName: string): Promise<any> {
        const loaderModel: LoaderModel = getModel(this, LoaderModel) as LoaderModel;
        if (loaderModel.isBitmapFontsLoaded(groupName)) {
            return Promise.resolve();
        }
        return new Promise<any>((resolve) => {
            this.dispatcher.once(LoadEvent.BITMAP_FONTS_LOADED, () => {
                resolve();
            });
            this.bitmapFontsLoader.forceLoad(groupName);
        })
    }

    private forceLoadSounds(groupName: string): Promise<any> {
        const loaderModel: LoaderModel = getModel(this, LoaderModel) as LoaderModel;
        if (loaderModel.isSoundsLoaded(groupName)) {
            return Promise.resolve();
        }
        return new Promise<any>((resolve) => {
            this.dispatcher.once(LoadEvent.SOUND_LOADED, () => {
                resolve();
            });
            this.soundLoader.forceLoad(groupName);
        })
    }

    protected loadSounds(priority: string): void {
        this.soundLoader.load(priority).then(() => {
            this.dispatcher.dispatch(LoadEvent.SOUND_LOADED);
        })
    }

    protected loadI18N(): void {
        const gameConfig: GameConfig = getModel(this, GameConfig) as GameConfig;
        const loadLocalization = (lang) => {
            this.configLoader.loadI18N(`${ConfigPathData.I18N_PREFIX_PATH}${lang}.json`).then(() => {
                this.dispatcher.dispatch(LoadEvent.LOCALIZATION_LOADED);
            }).catch(() => loadLocalization('en'));
        };
        loadLocalization(gameConfig.lang);
    }

}