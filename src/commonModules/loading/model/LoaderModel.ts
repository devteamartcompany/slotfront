import {IAssetsToLoad, IAssetsGroup} from "../interface/IAssetsLoader";
import {ISoundsGroup, ISoundsLoader} from "../../sound/interface/ISoundsLoader";
import {IBitmapFontGroup, IBitmapFontsLoader} from "../interface/IBitmapFontsLoader";
import {LoaderConstants} from "../constants/LoaderConstants";

export class LoaderModel {
    get totalLazyGroupSize(): number {
        return this._totalLazyGroupSize;
    }

    set totalLazyGroupSize(value: number) {
        this._totalLazyGroupSize = value;
    }

    public get preloadSoundsToLoad(): ISoundsGroup[] {
        return this._preloadSoundsToLoad;
    }

    public get lazySoundsToLoad(): ISoundsGroup[] {
        return this._lazySoundsToLoad;
    }

    public get initialSoundsToLoad(): ISoundsGroup[] {
        return this._initialSoundsToLoad;
    }

    public get lazyAssetToLoad(): IAssetsGroup[] {
        return this._lazyAssetToLoad;
    }

    public get preloadAssetToLoad(): IAssetsGroup[] {
        return this._preloadAssetToLoad;
    }

    public get initialAssetToLoad(): IAssetsGroup[] {
        return this._initialAssetToLoad;
    }

    public get lazyBitmapFontsToLoad(): IBitmapFontGroup[] {
        return this._lazyBitmapFontsToLoad;
    }

    public get preloadBitmapFontsToLoad(): IBitmapFontGroup[] {
        return this._preloadBitmapFontsToLoad;
    }

    public get initialBitmapFontsToLoad(): IBitmapFontGroup[] {
        return this._initialBitmapFontsToLoad;
    }

    private _initialAssetToLoad: IAssetsGroup[] = [];
    private _preloadAssetToLoad: IAssetsGroup[] = [];
    private _lazyAssetToLoad: IAssetsGroup[] = [];

    private _initialSoundsToLoad: ISoundsGroup[] = [];
    private _preloadSoundsToLoad: ISoundsGroup[] = [];
    private _lazySoundsToLoad: ISoundsGroup[] = [];

    private _initialBitmapFontsToLoad: IBitmapFontGroup[] = [];
    private _preloadBitmapFontsToLoad: IBitmapFontGroup[] = [];
    private _lazyBitmapFontsToLoad: IBitmapFontGroup[] = [];

    private _loadedGroups: string[] = [];
    private _soundLoadedGroups: string[] = [];
    private _bitmapFontsLoadedGroups: string[] = [];
    private _assetsLoadedGroups: string[] = [];

    private _totalLazyGroupSize: number = 0;

    //BITMAP_FONTS
    public initialBitmapFonts(data: IBitmapFontsLoader): void {
        this._initialBitmapFontsToLoad = data.groups.initial;
    }

    public preloadBitmapFonts(data: IBitmapFontsLoader): void {
        this._preloadBitmapFontsToLoad = data.groups.preload;
    }

    public lazyBitmapFonts(data: IBitmapFontsLoader): void {
        this._lazyBitmapFontsToLoad = data.groups.lazy;
    }

    //ASSETS
    public initialAssets(data: IAssetsToLoad): void {
        this._initialAssetToLoad = data.groups.initial;
    }

    public preloadAssets(data: IAssetsToLoad): void {
        this._preloadAssetToLoad = data.groups.preload;
    }

    public lazyAssets(data: IAssetsToLoad): void {
        this._lazyAssetToLoad = data.groups.lazy;
    }

    public isGroupLoaded(groupId: string): boolean {
        return this._loadedGroups.indexOf(groupId) !== -1;
    }

    public addLoadedGroup(groupId: string): void {
        if (this.isAssetsLoaded(groupId) && this.isBitmapFontsLoaded(groupId) && this.isSoundsLoaded(groupId)) {
            this.totalLazyGroupSize = 0;
            this._loadedGroups.push(groupId);
        }
    }

    public addSoundLoadedGroup(groupId: string) {
        this._soundLoadedGroups.push(groupId);
    }

    public addAssetsLoadedGroup(groupId: string) {
        this._assetsLoadedGroups.push(groupId);
    }

    public addBitmapFontsLoadedGroup(groupId: string) {
        this._bitmapFontsLoadedGroups.push(groupId);
    }

    //SOUNDS
    public initialSounds(data: ISoundsLoader) {
        this._initialSoundsToLoad = data.groups.initial;
    }

    public preloadSounds(data: ISoundsLoader) {
        this._preloadSoundsToLoad = data.groups.preload;
    }

    public lazySounds(data: ISoundsLoader) {
        this._lazySoundsToLoad = data.groups.lazy;
    }

    public isSoundsLoaded(groupId): boolean {
        if (!this._lazySoundsToLoad.find( (elem) => {
            return elem.group.indexOf(groupId) !== -1;
        })) {
            return  true;
        }
        return this._soundLoadedGroups.indexOf(groupId) !== -1;
    }

    public isAssetsLoaded(groupId): boolean {
        if (!this._lazyAssetToLoad.find( (elem) => {
            return elem.group.indexOf(groupId) !== -1;
        })) {
            return true;
        }
        return this._assetsLoadedGroups.indexOf(groupId) !== -1;
    }

    public isBitmapFontsLoaded(groupId): boolean {
        if (!this._lazyBitmapFontsToLoad.find( (elem) => {
            return elem.group.indexOf(groupId) !== -1;
        })) {
            return true;
        }
        return this._bitmapFontsLoadedGroups.indexOf(groupId) !== -1;
    }

    public currentGroupSize(currentPriorityID: string): number {
        switch (currentPriorityID) {
            case LoaderConstants.PRELOAD_PRIORITY:
                return (this._preloadBitmapFontsToLoad.length + this._preloadAssetToLoad.length + this._preloadSoundsToLoad.length);
                break;
            case LoaderConstants.INITIAL_PRIORITY:
                return (this._initialAssetToLoad.length + this._initialBitmapFontsToLoad.length + this._initialSoundsToLoad.length);
                break;
            case LoaderConstants.LAZY_PRIORITY:
                return (this.totalLazyGroupSize);
                break;

        }
    }
}