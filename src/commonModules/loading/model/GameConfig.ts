import {IBaseModel} from "../../../coreEngine/interfaces/IBaseModel";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";
import {AppLoggerMessageType} from "../../../coreEngine/general/utils/loger/AppLoggerMessageType";
import {GameInfo} from "../../../coreEngine/general/GameInfo";
import {URLParams} from "../../../coreEngine/general/URLParams";

export class GameConfig implements IBaseModel{

    private _serverUrl: string;
    private _playerId: number;
    private _currency: string;
    private _lang: string;
    private _gameName: string;
    private _platform_token: string;

    constructor() {
        this.parseURL();
    }

    public parseConfig(data: any): void {
        if (this._serverUrl === undefined) {
            this._serverUrl = data.serverUrl;
        }
        if (this._playerId === undefined) {
            this._playerId = new Date().getTime();
        }
        if (this._currency === undefined) {
            this._currency = data.currency;
        }
        if (this._lang === undefined) {
            this._lang = data.lang;
        }
        if (this._gameName === undefined) {
            this._gameName = data.gameName;
            GameInfo.init(this._gameName);
        }
    }

    /**
     * Fill game config depending on real url parameters.
     * player_id, lang, currency fields are temporary, and will be switched after server platform is finished.
     * So this is a temporary solution.
     */
    public parseURL() {
       /* const serverUrl: string = /^(localhost|\d{1,3}\.\d{1,3}\.\d{1,3}\.(\d{1,3}|0\/([1-9]|1[0-9]|2[0-4])))$/.test(window.location.hostname) ?
            this.serverUrl:
            `${window.location.protocol}//${window.location.host}`;*/

        this._lang = URLParams.getParam("lang") || "en";
        this._currency = URLParams.getParam("currency") || this._currency;
        this._playerId = Number(URLParams.getParam("player_id")) || this._playerId;
        this._platform_token = URLParams.getParam("platform_token") || "";
        this._serverUrl = URLParams.getParam("server_url") || this.serverUrl;
        AppLogger.log(this._lang, AppLoggerMessageType.DEFAULT);
        AppLogger.log(this._currency, AppLoggerMessageType.DEFAULT);
    }

    get serverUrl(): string {
        return this._serverUrl;
    }
    get playerId(): number {
        return this._playerId;
    }
    get currency(): string {
        return this._currency;
    }
    get lang(): string {
        return this._lang;
    }
    get gameName(): string {
        return this._gameName;
    }
    set platform_token(value: string) {
        this._platform_token = value;
    }
    get platform_token(): string {
        return this._platform_token;
    }
    set currency(value: string) {
        this._currency = value;
    }
    set lang(value: string) {
        this._lang = value;
    }
}