import {ApplicationModule} from "../../coreEngine/baseClasses/ApplicationModule";
import {addModel, createInstance} from "../../coreEngine/exports";
import {LoaderModel} from "./model/LoaderModel";
import {LoadManager} from "./manager/LoadManager";
import {GameConfig} from "./model/GameConfig";

export class LoadingModule extends ApplicationModule{

    public execute(): void {
        super.execute();
        addModel(createInstance(LoaderModel));
        addModel(createInstance(GameConfig));
        createInstance(LoadManager);
    }
}