export interface IAssetsToLoad {
    groups: {
        initial: IAssetsGroup[], preload: IAssetsGroup[], lazy: IAssetsGroup[]
    }
}

export interface IAssetsGroup {
    jsonUrl?: string,
    assetUrl?: string,
    platform: string[],
    group: string[],
    isCommon: boolean
}