export interface IBitmapFontsLoader {
    groups: {
        initial: IBitmapFontGroup[], preload: IBitmapFontGroup[], lazy: IBitmapFontGroup[]
    }
}

export interface IBitmapFontGroup {
    id: string,
    fontUrl: string,
    atlasUrl: string,
    group: string[],
    isCommon: true
}