export const LoaderConstants = {
    LAZY_PRIORITY: "lazy",
    PRELOAD_PRIORITY: "preload",
    INITIAL_PRIORITY: "initial"
}