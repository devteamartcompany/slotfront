import {LoaderConstants} from "../constants/LoaderConstants";
import * as PIXI from "pixi.js"
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";
import {AppLoggerMessageType} from "../../../coreEngine/general/utils/loger/AppLoggerMessageType";
import {LoaderModel} from "../model/LoaderModel";
import { getModel} from "../../../coreEngine/exports";
import {BaseController} from "../../../coreEngine/baseClasses/BaseController";
import {GameConfig} from "../model/GameConfig";
import {LoadEvent} from "../events/LoadEvents";
import {IBitmapFontGroup} from "../interface/IBitmapFontsLoader";

export class BitmapFontsLoader extends BaseController {
    private loaderModel: LoaderModel = getModel(this, LoaderModel) as LoaderModel;
    protected gameConfig: GameConfig = getModel(this, GameConfig) as GameConfig;
    private lazyQueue: ILazyGroup[] = [];
    private initialQueue: IBitmapFontGroup[];
    private currentLazyGroup: ILazyGroup;
    private forceGroupId: string;
    private currentPriorityID: string;
    private currentProgress: number = 0;
    private mainLazyResolve: Function;
    private mainInitialResolve: Function;
    private loadCurrentGroupComplete: boolean;

    public load(priority: string): Promise<any> {
        return new Promise<any>((loadComplete) => {
            switch (priority) {
                case LoaderConstants.PRELOAD_PRIORITY:
                    return new Promise<any>((resolve) => {
                        this.currentPriorityID = LoaderConstants.PRELOAD_PRIORITY;
                        if (this.loaderModel.preloadBitmapFontsToLoad.length <= 0) {
                            this.dispatcher.dispatch(LoadEvent.LOADING_COMPLETE);
                            loadComplete();
                            return;
                        }
                        this.loadQueue(this.loaderModel.preloadBitmapFontsToLoad)
                            .then((resolve) => {
                                AppLogger.log("ALL " + LoaderConstants.PRELOAD_PRIORITY + " bitmap fonts loaded!", AppLoggerMessageType.SUCCESS);
                                loadComplete();
                            });
                    })
                    break;
                case LoaderConstants.INITIAL_PRIORITY:
                    return new Promise<any>((resolve) => {
                        this.currentPriorityID = LoaderConstants.INITIAL_PRIORITY;
                        if (this.loaderModel.initialBitmapFontsToLoad.length <= 0) {
                            this.dispatcher.dispatch(LoadEvent.LOADING_COMPLETE);
                            loadComplete();
                            return;
                        }
                        this.loadQueue(this.loaderModel.initialBitmapFontsToLoad)
                            .then((resolve) => {
                                AppLogger.log("ALL " + LoaderConstants.INITIAL_PRIORITY + " bitmap fonts loaded!", AppLoggerMessageType.SUCCESS);
                                loadComplete();
                            });
                    })
                    break;
                case LoaderConstants.LAZY_PRIORITY:
                    this.currentPriorityID = LoaderConstants.LAZY_PRIORITY;
                    if (this.loaderModel.lazyBitmapFontsToLoad.length <= 0) {
                        this.dispatcher.dispatch(LoadEvent.LOADING_COMPLETE);
                        loadComplete();
                        return;
                    }
                    return new Promise<any>((resolve) => {
                        this.loadLazyQueue(this.loaderModel.lazyBitmapFontsToLoad)
                            .then((resolve) => {
                                AppLogger.log("ALL " + LoaderConstants.LAZY_PRIORITY + " bitmap fonts loaded!", AppLoggerMessageType.SUCCESS);
                                loadComplete();
                            });
                    })
                    break;
            }
        })

    }

    /**
     *@groupId parameter for force load assets group.
     * For example feature assets group need to be load faster then other lazy load groups
     * **/
    public forceLoad(groupId: string): void {
        if (this.loaderModel.lazyBitmapFontsToLoad.length <= 0) {
            return;
        }
        if (this.loaderModel.isGroupLoaded(groupId) || this.currentLazyGroup.id === this.forceGroupId) {
            return;
        }
        this.forceGroupId = groupId;

    }

    /**
     *preload and initial asset groups loader
     * **/
    private loadQueue(assets: IBitmapFontGroup[]): Promise<any> {
        return new Promise<any>((resolve) => {
            this.mainInitialResolve = resolve;
            this.loadCurrentGroupComplete = false;
            this.initialQueue = assets;
            assets.forEach((asset) => {
                PIXI.loader.add(asset.id + "_font", this.getPath(asset.atlasUrl, asset.isCommon));
                PIXI.loader.add(asset.id, this.getPath(asset.fontUrl, asset.isCommon));
            });
            PIXI.loader.load(() => {
                this.loadProgressHandler();
                this.mainInitialResolve();
            });
            PIXI.loader.on("error", this.onError);
        })
    }

    /**
     *lazy asset groups loader
     * **/
    private loadLazyQueue(assets: IBitmapFontGroup[]): Promise<any> {
        return new Promise<any>((resolve) => {
            this.loadCurrentGroupComplete = false;
            this.mainLazyResolve = resolve;
            const groupsId: string[] = [];
            this.lazyQueue = [];
            assets.forEach((asset) => {
                asset.group.forEach((groupName) => {
                    if (groupsId.indexOf(groupName) === -1) {
                        groupsId.push(groupName);
                    }
                })
            });
            groupsId.forEach((groupId: string) => {
                this.lazyQueue.push(
                    {
                        id: groupId,
                        assets: assets.filter((asset) => {
                            return asset.group.indexOf(groupId) !== -1;
                        })
                    }
                );
            });
            this.loadLazyGroup();
        })
    }

    private loadLazyGroup(): void {
        if (this.lazyQueue.length > 0) {
            this.currentLazyGroup = this.lazyQueue.shift();
            this.loaderModel.totalLazyGroupSize += this.currentLazyGroup.assets.length;
            this.loadLazyAsset(this.currentLazyGroup.assets.pop());
        } else {
            this.loadCurrentGroupComplete = true;
            this.mainLazyResolve();
        }
    }

    private loadLazyAsset(asset: IBitmapFontGroup): void {
        PIXI.loader.add(asset.id + "_png", this.getPath(asset.atlasUrl, asset.isCommon));
        PIXI.loader.add(asset.id + "_fnt", this.getPath(asset.fontUrl, asset.isCommon));
        PIXI.loader.onComplete.once(this.checkNextLazyAsset.bind(this));
        PIXI.loader.load(() => {
            this.loadProgressHandler();
        });
    }

    private checkNextLazyAsset(): void {
        if (this.forceGroupId) {
            let index: number = this.lazyQueue.findIndex((item) => {
                return item.id === this.forceGroupId;
            })
            if (this.currentLazyGroup.assets.length > 0) {
                this.lazyQueue.unshift(this.currentLazyGroup);
                index++;
            } else {
                this.onLazyGroupEnd();
            }

            this.currentLazyGroup = this.lazyQueue.splice(index, 1)[0];
            this.forceGroupId = null;
            this.loadLazyAsset(this.currentLazyGroup.assets.pop());
        } else {
            if (this.currentLazyGroup.assets.length > 0) {
                this.loadLazyAsset(this.currentLazyGroup.assets.pop());
            } else {
                this.onLazyGroupEnd();
                this.loadLazyGroup();
            }
        }
    }

    private onLazyGroupEnd(): void {
        AppLogger.log(`assets group is loaded: ${this.currentLazyGroup.id}`);
        this.dispatcher.dispatch(LoadEvent.ASSET_LOADED, this.currentLazyGroup.id);
        this.loaderModel.addBitmapFontsLoadedGroup(this.currentLazyGroup.id);
        this.loaderModel.addLoadedGroup(this.currentLazyGroup.id);
    }

    private getPath(data: string, isCommon: boolean): string {
        const langFolder: string = isCommon ? "common" : this.gameConfig.lang;
        return ("data/bitmapFonts/" + langFolder + "/" + data);
    }

    private onError(error) {
        AppLogger.log(error.toString(), AppLoggerMessageType.ERROR);
    }

    private loadProgressHandler() {
        if (!this.loadCurrentGroupComplete) {
            this.dispatcher.dispatch(LoadEvent.LOAD_PROGRESS, this.loaderModel.currentGroupSize(this.currentPriorityID));
        }
    }
}

interface ILazyGroup {
    id: string,
    assets: IBitmapFontGroup[]
}