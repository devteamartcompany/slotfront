import {LoaderConstants} from "../constants/LoaderConstants";
import {IAssetsGroup} from "../interface/IAssetsLoader";
import * as PIXI from "pixi.js"
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";
import {AppLoggerMessageType} from "../../../coreEngine/general/utils/loger/AppLoggerMessageType";
import {LoaderModel} from "../model/LoaderModel";
import {getDeviceManager, getModel} from "../../../coreEngine/exports";
import {BaseController} from "../../../coreEngine/baseClasses/BaseController";
import {DeviceManager} from "../../../coreEngine/general/utils/deviceManager/DeviceManager";
import {GameConfig} from "../model/GameConfig";
import {TextureHolder} from "../../../coreEngine/general/utils/layouts/TextureHolder";
import {LoadEvent} from "../events/LoadEvents";

export class AssetsLoader extends BaseController {
    private loaderModel: LoaderModel = getModel(this, LoaderModel) as LoaderModel;
    protected gameConfig: GameConfig = getModel(this, GameConfig) as GameConfig;
    private deviceManager: DeviceManager = getDeviceManager() as DeviceManager;
    private lazyQueue: ILazyGroup[] = [];
    private initialQueue: IAssetsGroup[];
    private currentLazyGroup: ILazyGroup;
    private forceGroupId: string;
    private currentPriorityID: string;
    private currentProgress: number = 0;
    private mainLazyResolve: Function;
    private mainInitialResolve: Function;

    public load(priority: string): Promise<any> {
        return new Promise<any>((loadComplete) => {
            switch (priority) {
                case LoaderConstants.PRELOAD_PRIORITY:
                    return new Promise<any>((resolve) => {
                        this.currentPriorityID = LoaderConstants.PRELOAD_PRIORITY;
                        if (this.loaderModel.preloadAssetToLoad.length <= 0) {
                            loadComplete();
                            return;
                        }
                        this.loadQueue(this.loaderModel.preloadAssetToLoad)
                            .then((resolve) => {
                                AppLogger.log("ALL " + LoaderConstants.PRELOAD_PRIORITY + " assets loaded!", AppLoggerMessageType.SUCCESS);
                                loadComplete();
                            });
                    })
                    break;
                case LoaderConstants.INITIAL_PRIORITY:
                    return new Promise<any>((resolve) => {
                        this.currentPriorityID = LoaderConstants.INITIAL_PRIORITY;
                        if (this.loaderModel.initialAssetToLoad.length <= 0) {
                            loadComplete();
                            return;
                        }
                        this.loadQueue(this.loaderModel.initialAssetToLoad)
                            .then((resolve) => {
                                AppLogger.log("ALL " + LoaderConstants.INITIAL_PRIORITY + " assets loaded!", AppLoggerMessageType.SUCCESS);
                                loadComplete();
                            });
                    })
                    break;
                case LoaderConstants.LAZY_PRIORITY:
                    this.currentPriorityID = LoaderConstants.LAZY_PRIORITY;
                    if (this.loaderModel.lazyAssetToLoad.length <= 0) {
                        loadComplete();
                        return;
                    }
                    return new Promise<any>((resolve) => {
                        this.loadLazyQueue(this.loaderModel.lazyAssetToLoad)
                            .then((resolve) => {
                                AppLogger.log("ALL " + LoaderConstants.LAZY_PRIORITY + " assets loaded!", AppLoggerMessageType.SUCCESS);
                                loadComplete();
                            });
                    })
                    break;
            }
        })

    }

    /**
     *@groupId parameter for force load assets group.
     * For example feature assets group need to be load faster then other lazy load groups
     * **/
    public forceLoad(groupId: string): void {
        if (this.loaderModel.lazyAssetToLoad.length <= 0) {
            return;
        }
        if (this.loaderModel.isGroupLoaded(groupId) || this.currentLazyGroup.id === this.forceGroupId) {
            return;
        }
        this.forceGroupId = groupId;

    }

    /**
     *preload and initial asset groups loader
     * **/
    private loadQueue(assets: IAssetsGroup[]): Promise<any> {
        return new Promise<any>((resolve) => {
            this.mainInitialResolve = resolve;
            this.initialQueue = assets;
            this.onLoadProgress(this.initialQueue);
        })
    }

    private onLoadProgress(initialQueue: IAssetsGroup[]) {
        let asset = initialQueue[this.currentProgress];
        if (this.addToPixiLoader(asset)) {
            PIXI.loader.load(() => {
                this.loadProgressHandler(initialQueue);
            });
        } else {
            this.loadProgressHandler(initialQueue);
        }

        PIXI.loader.on("error", this.onError);

    }


    /**
     *lazy asset groups loader
     * **/
    private loadLazyQueue(assets: IAssetsGroup[]): Promise<any> {
        return new Promise<any>((resolve) => {
            this.mainLazyResolve = resolve;
            const groupsId: string[] = [];
            this.lazyQueue = [];
            assets.forEach((asset) => {
                asset.group.forEach((groupName) => {
                    if (groupsId.indexOf(groupName) === -1) {
                        groupsId.push(groupName);
                    }
                })
            });
            groupsId.forEach((groupId: string) => {
                this.lazyQueue.push(
                    {
                        id: groupId,
                        assets: assets.filter((asset) => {
                            return asset.group.indexOf(groupId) !== -1;
                        })
                    }
                );
            });
            this.loadLazyGroup();
        })
    }

    private loadLazyGroup(): void {
        if (this.lazyQueue.length > 0) {
            this.currentLazyGroup = this.lazyQueue.shift();
            this.loaderModel.totalLazyGroupSize += this.currentLazyGroup.assets.length;
            this.loadLazyAsset(this.currentLazyGroup.assets.pop());
        } else {
            this.mainLazyResolve();
        }
    }

    private loadLazyAsset(asset: IAssetsGroup): void {

        if (!this.addToPixiLoader(asset)) {
            this.checkNextLazyAsset();
            return;
        }

        PIXI.loader.load(() => {
            this.dispatcher.dispatch(LoadEvent.LOAD_PROGRESS, this.loaderModel.currentGroupSize(this.currentPriorityID));
            this.checkNextLazyAsset();
        });
    }

    private checkNextLazyAsset(): void {
        if (this.forceGroupId) {
            let index: number = this.lazyQueue.findIndex((item) => {
                return item.id === this.forceGroupId;
            })
            if (this.currentLazyGroup.assets.length > 0) {
                this.lazyQueue.unshift(this.currentLazyGroup);
                index++;
            } else {
                this.onLazyGroupEnd();
            }

            this.currentLazyGroup = this.lazyQueue.splice(index, 1)[0];
            this.forceGroupId = null;
            this.loadLazyAsset(this.currentLazyGroup.assets.pop());
        } else {
            if (this.currentLazyGroup.assets.length > 0) {
                this.loadLazyAsset(this.currentLazyGroup.assets.pop());
            } else {
                this.onLazyGroupEnd();
                this.loadLazyGroup();
            }
        }
    }

    private onLazyGroupEnd(): void {
        AppLogger.log(`assets group is loaded: ${this.currentLazyGroup.id}`);
        this.dispatcher.dispatch(LoadEvent.ASSET_LOADED, this.currentLazyGroup.id);
        this.loaderModel.addAssetsLoadedGroup(this.currentLazyGroup.id);
        this.loaderModel.addLoadedGroup(this.currentLazyGroup.id);
    }

    protected addToPixiLoader(asset: IAssetsGroup): boolean {
        let configName: string;
        let urlParam: string;
        if (asset.jsonUrl) {
            configName = asset.jsonUrl.split("/")[0];
            urlParam = this.getAssetsJsonPath(asset.jsonUrl, asset.isCommon);
        } else if (asset.assetUrl) {
            configName = asset.assetUrl;
            urlParam = this.getAssetsUrlPath(asset.assetUrl, asset.isCommon);
        } else {
            AppLogger.log(`IAssetsGroup doesn't have correct url parameter`, AppLoggerMessageType.ERROR)
        }


        if (DeviceManager.getInstance().isMobile ) {
            if (asset.platform.indexOf("mobile") === -1) {
                return false;
            }

        } else if (asset.platform.indexOf("desktop") === -1) {
            return false;
        }

        if (PIXI.loader.resources[configName]) {
            AppLogger.log(configName + " already added to Loader");
            return false;
        }
        TextureHolder.setLayoutConfig(configName);
        PIXI.loader.add(configName, urlParam);
        return true;
    }

    private getAssetsJsonPath(data: string, isCommon: boolean): string {
        let dpiPostfix = data.replace(".json", "-" + this.deviceManager.dpi + ".json");
        const langFolder: string = isCommon ? "common" : this.gameConfig.lang;
        return ("data/assets/" + langFolder + "/" + dpiPostfix);
    }

    private getAssetsUrlPath(data: string, isCommon: boolean): string {
        const langFolder: string = isCommon ? "common" : this.gameConfig.lang;
        return ("data/assets/" + langFolder + "/" + data);
    }

    private onError(error) {
        AppLogger.log(error.toString(), AppLoggerMessageType.ERROR);
    }

    private loadProgressHandler(initialQueue: IAssetsGroup[]) {
        this.dispatcher.dispatch(LoadEvent.LOAD_PROGRESS, this.loaderModel.currentGroupSize(this.currentPriorityID));
        this.currentProgress += 1;
        if (this.currentProgress < initialQueue.length) {
            this.onLoadProgress(initialQueue);
        } else {
            this.currentProgress = 0;
            this.mainInitialResolve();
        }
    }
}

interface ILazyGroup {
    id: string,
    assets: IAssetsGroup[]
}