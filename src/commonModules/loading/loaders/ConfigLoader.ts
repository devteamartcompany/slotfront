import {IAssetsToLoad} from "../interface/IAssetsLoader";
import {ISoundsLoader} from "../../sound/interface/ISoundsLoader";
import {BaseController} from "../../../coreEngine/baseClasses/BaseController";
import {LoaderModel} from "../model/LoaderModel";
import {getModel} from "../../../coreEngine/exports";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";
import {AppLoggerMessageType} from "../../../coreEngine/general/utils/loger/AppLoggerMessageType";
import {GameConfig} from "../model/GameConfig";
import {ILayoutListConfig} from "../../../coreEngine/viewComponents/interfaces/ILayoutListConfig";
import {ViewComponentsFactory} from "../../../coreEngine/viewComponents/factories/ViewComponentsFactory";
import {GameParams} from "../../../coreEngine/GameParams";
import {SpineFactory} from "../../../coreEngine/viewComponents/spine/SpineFactory";
import {IBitmapFontsLoader} from "../interface/IBitmapFontsLoader";
import {LocalUtil} from "../../../coreEngine/general/localization/LocalUtil";

export class ConfigLoader extends BaseController {
    private loaderModel: LoaderModel = getModel(this, LoaderModel) as LoaderModel;
    protected gameConfig: GameConfig = getModel(this, GameConfig) as GameConfig;

    public loadSpineData(data: { url: string, isCommon: boolean }[]): Promise<any> {
        return Promise.all(data.map((spinePath) => {
            return new Promise<any>((resolve) => {
                let id = spinePath.url.replace(".json", "");
                const langFolder: string = spinePath.isCommon ? "common" : this.gameConfig.lang;
                spinePath.url = "data/spine/" + langFolder + "/" + spinePath.url;
                this.loadJson(spinePath.url)
                    .then((data: JSON) => {
                        this.parseSpineData(JSON.stringify(data), id);
                        resolve();
                    })
            });
        }))
    }

    public loadViewConfig(data: string): Promise<any> {
        return new Promise<any>((resolve) => {
            this.loadJson(data)
                .then((data: JSON) => {
                    this.parseViewConfig(JSON.stringify(data));
                })
                .then(resolve)
        })
    }

    public loadGameConfig(data: any): Promise<any> {
        return new Promise<any>((resolve) => {
            this.loadJson(data)
                .then((data: JSON) => {
                    const gameConfig: GameConfig = getModel(this, GameConfig) as GameConfig;
                    gameConfig.parseConfig(data);
                    resolve();
                })
        })
    }

    public loadGameParams(data: any): Promise<any> {
        return new Promise<any>((resolve) => {
            this.loadJson(data)
                .then((data: JSON) => {
                    GameParams.parseJson(data);
                    resolve();
                })
        })
    }

    public loadI18N(data: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.loadJson(data)
                .then((data: JSON) => {

                    LocalUtil.setDictionary(data);
                    resolve();
                }).catch(() => {
                reject();
                AppLogger.log("Can't load " + data, AppLoggerMessageType.ERROR);
            });

        })
    }

    public loadAssetsConfig(data: string): Promise<any> {
        return new Promise<any>((resolve) => {
            this.loadJson(data)
                .then((data: JSON) => {
                    this.parseAssetsConfig(JSON.stringify(data));
                })
                .then(resolve)
        })
    }

    public loadBitmapFontsConfig(data: string): Promise<any> {
        return new Promise<any>((resolve) => {
            this.loadJson(data)
                .then((data: JSON) => {
                    this.parseBitmapFontsConfig(JSON.stringify(data));
                })
                .then(resolve)
        })
    }

    public loadSoundConfig(data: string): Promise<any> {
        return new Promise<any>((resolve) => {
            this.loadJson(data)
                .then((data: JSON) => {
                    this.parseSoundConfig(JSON.stringify(data));
                })
                .then(resolve)
        })
    }

    private loadJson(data: string): Promise<JSON> {
        return new Promise((resolve, reject) => {
                fetch(data)
                    .then(
                        response =>
                            response.json()).catch(() => {
                    reject();
                    AppLogger.log("Can't load " + data, AppLoggerMessageType.ERROR);
                })
                    .then(
                        jsonData => {
                            resolve(jsonData)
                        })
                    .catch(() => {
                        reject();
                        AppLogger.log("Can't load " + data, AppLoggerMessageType.ERROR);
                    });
            }
        )
    }

    private parseAssetsConfig(data: any): void {
        let dataParse: IAssetsToLoad = JSON.parse(data);
        this.loaderModel.initialAssets(dataParse);
        this.loaderModel.preloadAssets(dataParse);
        this.loaderModel.lazyAssets(dataParse);

    }

    private parseBitmapFontsConfig(data: any): void {
        let dataParse: IBitmapFontsLoader = JSON.parse(data);
        this.loaderModel.initialBitmapFonts(dataParse);
        this.loaderModel.preloadBitmapFonts(dataParse);
        this.loaderModel.lazyBitmapFonts(dataParse);

    }

    private parseSoundConfig(data: any): void {
        let dataParse: ISoundsLoader = JSON.parse(data);
        this.loaderModel.initialSounds(dataParse);
        this.loaderModel.preloadSounds(dataParse);
        this.loaderModel.lazySounds(dataParse);

    }

    private parseSpineData(data: any, id: string): void {
        let dataParse: any = JSON.parse(data);
        SpineFactory.getInstance().mapSpine(dataParse, id);

    }

    private parseViewConfig(data: any): void {
        let dataParse: ILayoutListConfig = JSON.parse(data);
        ViewComponentsFactory.getInstance().parseViewList(dataParse);
    }
}

