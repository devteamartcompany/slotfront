import {LoaderConstants} from "../constants/LoaderConstants";
import {ISoundsGroup} from "../../sound/interface/ISoundsLoader";
import {BaseController} from "../../../coreEngine/baseClasses/BaseController";
import {LoaderModel} from "../model/LoaderModel";
import {getModel} from "../../../coreEngine/exports";
import {Sound} from "../../sound/Sound";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";
import {SoundEvents} from "../../sound/events/SoundEvents";
import {LoadEvent} from "../events/LoadEvents";

export class SoundLoader extends BaseController {
    private mainInitialResolve: Function;
    private lazyQueue: ILazyGroup[] = [];
    private mainLazyResolve: Function;
    private currentLazyGroup: ILazyGroup;
    private loaderModel: LoaderModel = getModel(this, LoaderModel) as LoaderModel;
    private forceGroupId: string;
    private currentPriorityID: string;

    public load(priority: string): Promise<any> {

        return new Promise<any>((loadComplete) => {
            switch (priority) {
                case LoaderConstants.PRELOAD_PRIORITY:
                    this.currentPriorityID = LoaderConstants.PRELOAD_PRIORITY;
                    if (this.loaderModel.preloadSoundsToLoad.length <= 0) {
                        loadComplete();
                        return;
                    }
                    return Promise.all([
                        this.loadQueue(this.loaderModel.preloadSoundsToLoad)
                    ]).then(() => {
                        AppLogger.log("ALL " + LoaderConstants.PRELOAD_PRIORITY + " sounds loaded!");
                        loadComplete();
                    });
                    break;
                case LoaderConstants.INITIAL_PRIORITY:
                    this.currentPriorityID = LoaderConstants.INITIAL_PRIORITY;
                    if (this.loaderModel.initialSoundsToLoad.length <= 0) {
                        loadComplete();
                        return;
                    }
                    return Promise.all([
                        this.loadQueue(this.loaderModel.initialSoundsToLoad)
                    ]).then(() => {
                        AppLogger.log("ALL " + LoaderConstants.INITIAL_PRIORITY + " sounds loaded!");
                        loadComplete();
                    });
                    break;
                case LoaderConstants.LAZY_PRIORITY:
                    this.currentPriorityID = LoaderConstants.LAZY_PRIORITY;
                    if (this.loaderModel.lazySoundsToLoad.length <= 0) {
                        loadComplete();
                        return;
                    }
                    return Promise.all([
                        this.loadLazyQueue(this.loaderModel.lazySoundsToLoad)
                    ]).then(() => {
                        AppLogger.log("ALL " + LoaderConstants.LAZY_PRIORITY + " sounds loaded!");
                        loadComplete();
                    });
                    break;
            }
        })
    }

    public forceLoad(groupId: string): void {
        if (this.loaderModel.lazySoundsToLoad.length <= 0) {
            return;
        }
        if (this.loaderModel.isGroupLoaded(groupId) || this.currentLazyGroup.id === this.forceGroupId) {
            return;
        }
        this.forceGroupId = groupId;

    }

    private loadLazyQueue(soundsData: ISoundsGroup[]): Promise<any> {
        return new Promise<any>((resolve) => {
            this.mainLazyResolve = resolve;
            const groupsId: string[] = [];
            this.lazyQueue = [];
            soundsData.forEach((sound) => {
                sound.group.forEach((groupName) => {
                    if (groupsId.indexOf(groupName) === -1) {
                        groupsId.push(groupName);
                    }
                })
            });
            groupsId.forEach((groupId: string) => {
                this.lazyQueue.push(
                    {
                        id: groupId,
                        assets: soundsData.filter((sound) => {
                            return sound.group.indexOf(groupId) !== -1;
                        })
                    }
                );
            });
            this.loadLazyGroup();
        })
    }

    private loadLazyGroup(): void {
        if (this.lazyQueue.length > 0) {
            this.currentLazyGroup = this.lazyQueue.shift();
            this.loaderModel.totalLazyGroupSize += this.currentLazyGroup.assets.length;
            this.loadLazySounds(this.currentLazyGroup.assets.pop());
        } else {
            this.mainLazyResolve();
        }
    }

    private loadLazySounds(group: ISoundsGroup): void {
        let sound = new Sound({
            src: [group.url],
        });
        sound.id = group.id;
        sound.on("load", () => {
            this.dispatcher.dispatch(LoadEvent.LOAD_PROGRESS, this.loaderModel.currentGroupSize(this.currentPriorityID));
            this.dispatcher.dispatch(SoundEvents.MAP_SOUND, sound);
            this.checkNextLazyAsset();
        });
    }

    private checkNextLazyAsset(): void {
        if (this.forceGroupId) {
            let index: number = this.lazyQueue.findIndex((item) => {
                return item.id === this.forceGroupId;
            })
            if (this.currentLazyGroup.assets.length > 0) {
                this.lazyQueue.unshift(this.currentLazyGroup);
                index++;
            } else {
                this.onLazyGroupEnd();
            }

            this.currentLazyGroup = this.lazyQueue.splice(index, 1)[0];
            this.forceGroupId = null;
            this.loadLazySounds(this.currentLazyGroup.assets.pop());
        } else {
            if (this.currentLazyGroup.assets.length > 0) {
                this.loadLazySounds(this.currentLazyGroup.assets.pop());
            } else {
                this.onLazyGroupEnd();
                this.loadLazyGroup();
            }
        }
    }

    private onLazyGroupEnd(): void {
        AppLogger.log(`assets group is loaded: ${this.currentLazyGroup.id}`);
        this.dispatcher.dispatch(LoadEvent.SOUND_LOADED, this.currentLazyGroup.id);
        this.loaderModel.addSoundLoadedGroup(this.currentLazyGroup.id);
        this.loaderModel.addLoadedGroup(this.currentLazyGroup.id);
    }

    private loadQueue(soundsData: ISoundsGroup[]): Promise<any> {
        return new Promise<any>((resolve) => {
            this.mainInitialResolve = resolve;

            let count: number = 0;
            soundsData.map((item) => {
                let sound = new Sound({
                    src: [item.url],
                });
                sound.id = item.id;
                sound.on("load", () => {
                    count += 1;
                    this.dispatcher.dispatch(LoadEvent.LOAD_PROGRESS, this.loaderModel.currentGroupSize(this.currentPriorityID));
                    this.dispatcher.dispatch(SoundEvents.MAP_SOUND, sound);
                    if (count >= soundsData.length - 1) {
                        this.mainInitialResolve();
                    }
                });
            });

        });
    }

}

interface ILazyGroup {
    id: string,
    assets: ISoundsGroup[]
}