import {BaseAction} from "../../../coreEngine/fsm/BaseAction";

import {LoadEvent} from "../../loading/events/LoadEvents";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";

export class HideLoaderViewAction extends BaseAction {
    execute(): Promise<any> {
        AppLogger.log(`${this.constructor.name}`);
        return new Promise<any>((resolve) => {
            this.dispatcher.dispatch(LoadEvent.HIDE_LOADER);
            resolve();
        });
    }
}