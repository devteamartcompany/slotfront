import {ApplicationModule} from "../../coreEngine/baseClasses/ApplicationModule";
import {bindMediator} from "../../coreEngine/exports";
import {PreloaderMediator} from "./mediator/PreloaderMediator";
import {PreloaderView} from "./view/PreloaderView";

export class PreloaderModule extends ApplicationModule {
    public execute(): void {
        super.execute();
        bindMediator(PreloaderMediator, PreloaderView, "loaderContainer");
    }
}