import {BaseView} from "../../../coreEngine/baseClasses/BaseView";
import Graphics = PIXI.Graphics;
import {TweenLite} from "gsap";
import {CoreRenderer} from "../../../coreEngine/general/utils/renderer/CoreRenderer";
import {LoadEvent} from "../../loading/events/LoadEvents";
import {IRenderViewParams} from "../../renderModule/IRenderViewParams";
import {BaseSprite} from "../../../coreEngine/viewComponents/BaseSprite";
import {BaseRectangle} from "../../../coreEngine/viewComponents/BaseRectangle";
import {BaseLabel} from "../../../coreEngine/viewComponents/BaseLabel";

export class PreloaderView extends BaseView {
    protected progressCounter: number = 0;
    protected bar: BaseSprite;
    protected rect: BaseRectangle;
    protected progressMask: Graphics;
    protected progressText: BaseLabel;
    protected count: any = {val: 0};
    protected progressTween: TweenLite;
    protected countTween: TweenLite;
    private isComplete: boolean = false;

    public init(): void {
        super.init();

        this.bar = this.source.getChildByName("loaderProgress");
        this.rect = this.source.getChildByName("progressMask");
        this.progressText = this.source.getChildByName("progressLabel");
        this.progressMask = this.rect.rect;
        this.bar.mask = this.progressMask;
        this.progressMask.width = 1;
        this.source.addChild(this.progressMask);
        this.setMaskPosition(this.bar.x, this.bar.y);
        this.hide();
    }

    public removeLoader(): void {
        if (!this.countTween) {
            return;
        }
        this.countTween.kill();
        this.countTween = null;
        this.isComplete = true;
        this.progressText.setText("100%");
        this.progressMask.width = this.bar.width;
        this.progressTween = TweenLite.to(this.source, 0.4, {
            alpha: 0,
            onComplete: () => {
                this.source.visible = false;
                this.progressText.visible = false;
                this.progressCounter = 0;
                this.emit(LoadEvent.LOADING_COMPLETE);
            }
        });
    }

    public updateProgress(totalSize: number) {
        if (this.progressMask) {
            this.progressCounter += 1;
            if (this.progressCounter > totalSize) {
                return;
            }
            let progress: number = Math.floor((this.progressCounter / totalSize) * 100);
            this.progressTween = TweenLite.to(this.progressMask, 0.6, {
                width: this.getWidthPosition(progress)
            })
            this.countTween = TweenLite.to(this.count, 0.3, {
                val: progress,
                roundProps: "val",
                onUpdate: () => {
                    if (!this.isComplete) {
                        this.progressText.setText(this.count.val.toString() + "%");
                    }
                }
            });
        }
    }

    public resetLoaderView(): void {
        CoreRenderer.stage.addChild(this.source);
        this.progressCounter = 0;
        this.source.alpha = 1;
        this.source.visible = true;
        this.progressMask.width = 1;
        this.isComplete = false;
    }

    public onResize(params: IRenderViewParams) {
        if (!this.source) {
            return;
        }
        if (params.width && params.height) {
            this.source.scale.x = this.source.scale.y = params.scale;
        }
        this.source.position.set(params.positionX, params.positionY);
        this.setMaskPosition(this.bar.x, this.bar.y);
    }

    private getWidthPosition(progress: number): number {
        let result: number;
        result = Math.floor(progress * (this.bar.width) / 100);
        return result;
    }

    private setMaskPosition(x: number, y: number) {
        this.progressMask.position.set(x, y);
    }
}