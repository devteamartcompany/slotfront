import {BaseMediator} from "../../../coreEngine/baseClasses/BaseMediator";
import {LoadEvent} from "../../loading/events/LoadEvents";
import {PreloaderView} from "../view/PreloaderView";
import {RendererManagerEvents} from "../../renderModule/events/RendererManagerEvents";
import {IRenderViewParams} from "../../renderModule/IRenderViewParams";

export class PreloaderMediator extends BaseMediator {

    protected view: PreloaderView;

    public addListeners(): void {
        super.addListeners();
        this.dispatcher.addListener(LoadEvent.RESET_LOADER_VIEW, this.resetLoaderView.bind(this));
        this.dispatcher.addListener(LoadEvent.LOAD_PROGRESS, this.updateProgress.bind(this));
        this.dispatcher.addListener(LoadEvent.HIDE_LOADER, this.hideView.bind(this));

        this.dispatcher.addListener(RendererManagerEvents.RESIZE, this.onResize.bind(this));
        this.registerViewListener(LoadEvent.LOADING_COMPLETE);
    }

    /**
     * @LoadEvent.LOAD_PROGRESS
     * update progress bar for each loaded asset**/

    private hideView(): void {
        this.view.removeLoader();
    }

    private resetLoaderView(): void {
        this.view.resetLoaderView();
    }

    private updateProgress(progress: number): void {
        this.view.updateProgress(progress);
    }

    private onResize(params: IRenderViewParams): void {
        this.view.onResize(params);
    }
}