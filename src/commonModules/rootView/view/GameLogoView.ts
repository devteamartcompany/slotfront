import {BaseView} from "../../../coreEngine/baseClasses/BaseView";

export class GameLogoView extends BaseView {

    public init(): void {
        super.init();
        this.hide();
    }
}