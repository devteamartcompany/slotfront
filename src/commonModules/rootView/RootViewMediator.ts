import {BaseMediator} from "../../coreEngine/baseClasses/BaseMediator";
import {RootView} from "./RootView";
import {RendererManagerEvents} from "../renderModule/events/RendererManagerEvents";
import {IRenderViewParams} from "../renderModule/IRenderViewParams";
import {RootViewEvents} from "./events/RootViewEvents";

export class RootViewMediator extends BaseMediator {
    protected view: RootView;
    addListeners(): void {
        super.addListeners();
        this.dispatcher.addListener(RendererManagerEvents.RESIZE, this.onResize.bind(this));
        this.dispatcher.addListener(RootViewEvents.ADD_ROOT_VIEW_TO_STAGE, this.addOnStage.bind(this));
    }

    private onResize(params: IRenderViewParams): void {
        this.view.onResize(params);
    }
    private addOnStage(): void {
        this.view.addToStage();
    }
}