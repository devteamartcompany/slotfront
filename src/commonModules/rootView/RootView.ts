import {BaseView} from "../../coreEngine/baseClasses/BaseView";
import {CoreRenderer} from "../../coreEngine/general/utils/renderer/CoreRenderer";
import {IRenderViewParams} from "../renderModule/IRenderViewParams";
import {TweenLite} from "gsap";

export class RootView extends BaseView {

    public addToStage(): void {
        CoreRenderer.stage.addChild(this.source);
        this.source.alpha = 0;
        TweenLite.to(this.source, 0.8, {
            alpha: 1
        });
    }

    public onResize(params: IRenderViewParams) {
        if (!this.source) {
            return;
        }
        if (params.width && params.height) {
            this.source.scale.x = this.source.scale.y = params.scale;
        }
        this.source.position.set(params.positionX, params.positionY);

    }
}