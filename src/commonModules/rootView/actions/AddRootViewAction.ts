import {BaseAction} from "../../../coreEngine/fsm/BaseAction";
import {RootViewEvents} from "../events/RootViewEvents";
import {RendererManagerEvents} from "../../renderModule/events/RendererManagerEvents";
import {EventDispatcher} from "../../../coreEngine/general/EventDispatcher";

export class AddRootViewAction extends BaseAction {
    execute(): Promise<any> {
        return new Promise<any>((resolve) => {
            this.dispatcher.dispatch(RootViewEvents.ADD_ROOT_VIEW_TO_STAGE);
            EventDispatcher.getInstance().dispatch(RootViewEvents.ADD_ROOT_VIEW_TO_STAGE);
            this.dispatcher.dispatch(RootViewEvents.ADD_FOOTER_TO_STAGE);
            this.dispatcher.dispatch(RendererManagerEvents.GET_SAFE_AREA_VIEW);
            resolve();
        });
    }
}