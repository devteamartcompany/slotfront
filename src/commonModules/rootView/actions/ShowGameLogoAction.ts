import {BaseAction} from "../../../coreEngine/fsm/BaseAction";
import {RootViewEvents} from "../../rootView/events/RootViewEvents";

export class ShowGameLogoAction extends BaseAction {
    execute(): Promise<any> {
        return new Promise<any>((resolve) => {
            this.dispatcher.dispatch(RootViewEvents.SHOW_GAME_LOGO);
            resolve();
        });
    }
}