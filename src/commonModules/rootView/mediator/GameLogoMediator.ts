import {GameLogoView} from "../view/GameLogoView";
import {BaseMediator} from "../../../coreEngine/baseClasses/BaseMediator";
import {RootViewEvents} from "../events/RootViewEvents";

export class GameLogoMediator extends BaseMediator {
    protected view: GameLogoView;

    addListeners(): void {
        super.addListeners();
        this.dispatcher.addListener(RootViewEvents.SHOW_GAME_LOGO, this.showView.bind(this));
    }

    protected showView(): void {
        this.view.show();
    }
}