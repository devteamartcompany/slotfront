import {ApplicationModule} from "../../coreEngine/baseClasses/ApplicationModule";
import {bindMediator} from "../../coreEngine/exports";
import {RootViewMediator} from "./RootViewMediator";
import {RootView} from "./RootView";
import {GameLogoMediator} from "./mediator/GameLogoMediator";
import {GameLogoView} from "./view/GameLogoView";

export class RootViewModule extends ApplicationModule {

    public execute(): void {
        super.execute();
        bindMediator(RootViewMediator, RootView, "rootContainer");
        bindMediator(GameLogoMediator, GameLogoView, "logoContainer");
    }
}