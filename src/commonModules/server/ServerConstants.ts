export const ServerConstants = {
    INIT: "init",
    SIGN_UP: "users/signup",
    LOGIN: "users/login",
    BALANCE: "users/updateBalance",
    POST: "post",
    PATCH: "PATCH"
}