import {AuthorizationModel} from "./models/AuthorizationModel";
import {ApplicationModule} from "../../coreEngine/baseClasses/ApplicationModule";
import {addServerModel, createInstance} from "../../coreEngine/exports";
import {CreateUserServerService} from "./services/CreateUserServerService";
import {LoginServerService} from "./services/LoginServerService";
import {ErrorServerModel} from "../modalPopup/models/ErrorServerModel";
import {UserModel} from "./models/UserModel";
import {ChangeBalanceServerService} from "./services/ChangeBalanceServerService";

export class ServerModule extends ApplicationModule{

   public execute(): void {
        super.execute();
        createInstance(CreateUserServerService);
        createInstance(LoginServerService);
        createInstance(ChangeBalanceServerService);
        addServerModel(createInstance(AuthorizationModel));
        addServerModel(createInstance(ErrorServerModel));
        addServerModel(createInstance(UserModel));
   }

}