import {IBaseModel} from "../../../coreEngine/interfaces/IBaseModel";


export interface IServerModel extends IBaseModel{
    parseResponse(data: any): void;
}