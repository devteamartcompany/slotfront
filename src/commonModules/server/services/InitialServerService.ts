import {ServerServiceEvents} from "../events/ServerServiceEvents";
import {BaseServerService} from "./BaseServerService";
import {ServerConstants} from "../ServerConstants";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";
import {AppLoggerMessageType} from "../../../coreEngine/general/utils/loger/AppLoggerMessageType";

export class InitialServerService extends BaseServerService{

    constructor() {
        super();
        this.dispatcher.addListener(ServerServiceEvents.SEND_INIT_REQUEST, this.send.bind(this));
    }

    private send(): void {
        this.fetch({}, ServerConstants.INIT)
            .then( (data:any) => {
                this.onRequest(data);
                this.dispatcher.dispatch(ServerServiceEvents.RECEIVE_INIT_RESPONSE);
            });
    }

    protected onRequest(data: any): void {
        super.onRequest(data);
        AppLogger.log("Init response recieved", AppLoggerMessageType.DEFAULT)
    }
}