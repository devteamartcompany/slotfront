import {BaseServerService} from "./BaseServerService";
import {ServerServiceEvents} from "../events/ServerServiceEvents";
import {ILoginViewData} from "../../../gameModules/ui/interfaces/ILoginViewData";
import {ServerConstants} from "../ServerConstants";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";
import {AppLoggerMessageType} from "../../../coreEngine/general/utils/loger/AppLoggerMessageType";

export class ChangeBalanceServerService extends BaseServerService {
    constructor() {
        super();
        this.dispatcher.addListener(ServerServiceEvents.SEND_BALANCE_REQUEST, this.send.bind(this));
    }

    private send(data: ILoginViewData): void {
        this.fetch(data, ServerConstants.BALANCE, ServerConstants.PATCH)
            .then( (data:any) => {
                this.onRequest(data);
                this.dispatcher.dispatch(ServerServiceEvents.RECEIVE_BALANCE_RESPONSE);
            });
    }

    protected onRequest(data: any): void {
        super.onRequest(data);
        AppLogger.log("BALANCE response received", AppLoggerMessageType.DEFAULT)
    }
}