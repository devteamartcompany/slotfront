import {BaseServerService} from "./BaseServerService";
import {ServerServiceEvents} from "../events/ServerServiceEvents";
import {ServerConstants} from "../ServerConstants";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";
import {AppLoggerMessageType} from "../../../coreEngine/general/utils/loger/AppLoggerMessageType";
import {ICreateUserViewData} from "../../../gameModules/ui/interfaces/ICreateUserViewData";

export class CreateUserServerService extends BaseServerService {
    constructor() {
        super();
        this.dispatcher.addListener(ServerServiceEvents.SEND_CREATE_USER_REQUEST, this.send.bind(this));
    }

    private send(data: ICreateUserViewData): void {
        this.fetch(data, ServerConstants.SIGN_UP)
            .then( (data:any) => {
                this.onRequest(data);
                this.dispatcher.dispatch(ServerServiceEvents.RECEIVE_CREATE_USER_RESPONSE);
            });
    }

    protected onRequest(data: any): void {
        super.onRequest(data);
        AppLogger.log("SIGN_UP response received", AppLoggerMessageType.DEFAULT)
    }
}