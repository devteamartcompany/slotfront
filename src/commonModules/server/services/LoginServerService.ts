import {BaseServerService} from "./BaseServerService";
import {ServerServiceEvents} from "../events/ServerServiceEvents";
import {ServerConstants} from "../ServerConstants";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";
import {AppLoggerMessageType} from "../../../coreEngine/general/utils/loger/AppLoggerMessageType";
import {ILoginViewData} from "../../../gameModules/ui/interfaces/ILoginViewData";

export class LoginServerService extends BaseServerService {
    constructor() {
        super();
        this.dispatcher.addListener(ServerServiceEvents.SEND_LOGIN_REQUEST, this.send.bind(this));
    }

    private send(data: ILoginViewData): void {
        this.fetch(data, ServerConstants.LOGIN)
            .then( (data:any) => {
                this.onRequest(data);
                this.dispatcher.dispatch(ServerServiceEvents.RECEIVE_LOGIN_RESPONSE);
            });
    }

    protected onRequest(data: any): void {
        super.onRequest(data);
        AppLogger.log("LOGIN response received", AppLoggerMessageType.DEFAULT)
    }
}