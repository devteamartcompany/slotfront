
import {GameConfig} from "../../loading/model/GameConfig";

import {ErrorServerModel} from "../../modalPopup/models/ErrorServerModel";
import {ModalPopupEvents} from "../../modalPopup/events/ModalPopupEvents";
import {ModalPopupVO} from "../../modalPopup/events/ModalPopupVO";
import {ModalPopupUtil} from "../../modalPopup/util/ModalPopupUtil";
import {BaseController} from "../../../coreEngine/baseClasses/BaseController";
import {getModel} from "../../../coreEngine/exports";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";
import {AppLoggerMessageType} from "../../../coreEngine/general/utils/loger/AppLoggerMessageType";
import {Facade} from "../../../coreEngine/Facade";
import {LocalUtil} from "../../../coreEngine/general/localization/LocalUtil";
import {URLParams} from "../../../coreEngine/general/URLParams";
import {UserModel} from "../models/UserModel";
import { ServerConstants } from '../ServerConstants';

export class BaseServerService extends BaseController{

    protected inProgress: any = {};
    protected readonly SERVER_TIMEOUT: number = 10000;
    protected readonly MAX_REPEATS: number = 3;

    protected timeout;
    protected repeatsCounter: number;
    protected requestBody: any;
    protected responseType: string;
    protected fetchParams: any;
    protected gameConfig: GameConfig = getModel(this, GameConfig) as GameConfig;

    /**
     * This is a common method to fetch responses from server.
     * We have 3 possibilities to fetch with timeout 20 sec
     * @param requestBody - post parameters (Depends on server API)
     * @param responseType ("init", "spin", etc. Depends on server API)
     */
    protected fetch(requestBody: any, responseType: string, method?: string): Promise<any> {
        this.requestBody = requestBody;
        this.responseType = responseType;

        if (this.inProgress[requestBody.type]) {
            AppLogger.log("The same request is in progress", AppLoggerMessageType.ERROR);
            return Promise.reject({
                inProgress: true
            });
        }
        return new Promise( (resolve, reject)=> {

            this.fetchParams = {
                method: method || ServerConstants.POST,
                headers: { "Content-Type": "application/json" },

                body: JSON.stringify(this.requestBody)
            };
            const userModel: UserModel = getModel(this, UserModel) as UserModel;
            if (userModel.token) {
                this.fetchParams.headers["Authorization"] = 'Bearer ' + userModel.token;
            }
            this.inProgress[requestBody.type] = true;
            this.repeatsCounter = 0;

            this.doNetworkCall(resolve, reject );
        });
    }

    /**
     *
     * @param route - it's a fetch response type ("init", "spin", etc. Depends on server API)
     * @param gameName - get from game config
     * @param getParams - obligatory url parameters + additional url parameters from operator / platform
     */
    protected doNetworkCall(resolve, reject): void {
        const route: string = `\\${this.responseType}`;
        const gameName: string = `\\${this.gameConfig.gameName}`;
        // const getParams: string = this.takeGetParams();
        const errorServerModel: ErrorServerModel = getModel(this, ErrorServerModel) as ErrorServerModel;
        errorServerModel.clear();

        fetch(`${this.gameConfig.serverUrl}${gameName}${route}`, this.fetchParams)
            .then((response) => {
                this.resolveFn(response,  resolve, reject)
            }, (error) => {
                this.rejectFn(error,  resolve, reject )
            });
    }

    protected resolveFn(response, resolve, reject): void {
        this.fetchFinally();
        response.json().then( (data)=> {
            if (response.ok) {
                resolve(data);
            }
            else {
                this.checkServerErrorMessages(data);
                reject(data);
            }
        });
    }

    protected rejectFn(error, resolve, reject): void {
        this.repeatsCounter++;
        if (error.code !== undefined) {
            this.fetchFinally();
            this.checkServerErrorMessages(error);
            reject(error);
        }
        else if (this.repeatsCounter < this.MAX_REPEATS){
            this.timeout = setTimeout( () => {
                this.doNetworkCall(resolve, reject);
            }, this.SERVER_TIMEOUT);

        } else {
            this.showTimeoutError();
        }
    }

    protected fetchFinally(): void {
        clearTimeout(this.timeout);
        this.inProgress[this.requestBody.type] = false;
    }

    protected onRequest(data: any): void {
        Facade.instance().getServerModels(this).forEach((serverModel) => {
            serverModel.parseResponse(data);
        });
        this.checkServerErrorMessages();
    }

    protected checkServerErrorMessages(errror?: any): void {

        const errorServerModel: ErrorServerModel = getModel(this, ErrorServerModel) as ErrorServerModel;
        if (errror) {
            errorServerModel.parseResponse(errror);
        }
        if (errorServerModel.status === "error" || errorServerModel.code) {
            const data: ModalPopupVO = ModalPopupUtil.getModalPopupData(errorServerModel.code, errorServerModel.messageHeader, errorServerModel.messageBody)
            this.dispatcher.dispatch(ModalPopupEvents.SHOW_MODAL_POPUP, data);
        }
    }

    protected showTimeoutError(): any {
        const data: ModalPopupVO = ModalPopupUtil.getModalPopupData("", LocalUtil.text("SERVER_TIMEOUT"), LocalUtil.text("SERVER_TIMEOUT body"));
        this.dispatcher.dispatch(ModalPopupEvents.SHOW_MODAL_POPUP, data);
    }

    /**
     * Get obligatory url parameters depending on real url parameters.
     *
     * This is a temporary solution,  and will be switched after server platform is finished.
     */
    protected getObligatoryParams(): string {
        const res: any = {};
        if (!URLParams.getParam("platform_token")) {
            res.platform_token =  this.gameConfig.platform_token ;
        }

        return this.stringifyJSON(res);
    }

    protected takeGetParams(): string {
        const getParams: string = `${this.getObligatoryParams()}&${URLParams.toString()}`;
        return getParams;
    }

    protected stringifyJSON(obj:any): string {
        let str = [];
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                str.push(encodeURIComponent(key) + "=" + encodeURIComponent(obj[key]))
            }
        }
        return str.join("&");
    }
}