import {IServerModel} from "../interfaces/IServerModel";

export class UserModel implements IServerModel {
    get token(): string {
        return this._token;
    }
    get balance(): number {
        return this._balance;
    }
    get userName(): string {
        return this._userName;
    }

    private _userName: string;
    private _balance: number;
    private _token: string;

    parseResponse(data: any): void {
        this._balance = data.data.user.balance || 0;
        this._userName = data.data.user.name;
        if (data.token) {
            this._token = data.token;
        }
    }
}