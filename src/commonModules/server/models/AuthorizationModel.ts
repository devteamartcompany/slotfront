import {IServerModel} from "../interfaces/IServerModel";

export class AuthorizationModel implements IServerModel {


    private _signupName: string;
    private _signupEmail: string;
    private _signupPassword: string;
    private _signupPasswordConfirm: string;

    private _loginEmail: string;
    private _loginPassword: string;

    parseResponse(data: any): void {
    }

    set signupPasswordConfirm(value: string) {
        this._signupPasswordConfirm = value;
    }
    get signupPasswordConfirm(): string {
        return this._signupPasswordConfirm;
    }
    get signupPassword(): string {
        return this._signupPassword;
    }
    set signupPassword(value: string) {
        this._signupPassword = value;
    }
    get signupEmail(): string {
        return this._signupEmail;
    }
    set signupEmail(value: string) {
        this._signupEmail = value;
    }
    set signupName(value: string) {
        this._signupName = value;
    }
    get signupName(): string {
        return this._signupName;
    }
    get loginPassword(): string {
        return this._loginPassword;
    }
    set loginPassword(value: string) {
        this._loginPassword = value;
    }
    get loginEmail(): string {
        return this._loginEmail;
    }
    set loginEmail(value: string) {
        this._loginEmail = value;
    }

}