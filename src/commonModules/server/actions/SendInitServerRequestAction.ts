import {ServerServiceEvents} from "../events/ServerServiceEvents";
import {BaseAction} from "../../../coreEngine/fsm/BaseAction";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";

export class SendInitServerRequestAction extends BaseAction {
    execute(): Promise<any> {
        AppLogger.log(`${this.constructor.name}`);
        return new Promise<any>((resolve) => {
            this.dispatcher.addListener(ServerServiceEvents.RECEIVE_INIT_RESPONSE, () => {
                resolve();
            });
            this.dispatcher.dispatch(ServerServiceEvents.SEND_INIT_REQUEST);
        });
    }

}