import {ModalPopupEvents} from "./events/ModalPopupEvents";
import {ModalPopupVO} from "./events/ModalPopupVO";
import {ModalPopupView} from "./ModalPopupView";
import {BaseMediator} from "../../coreEngine/baseClasses/BaseMediator";

export class ModalPopupMediator extends BaseMediator{

    protected view: ModalPopupView;

    addListeners(): void {
        super.addListeners();
        this.dispatcher.addListener(ModalPopupEvents.SHOW_MODAL_POPUP, this.showModalPopup.bind(this))
    }

    protected showModalPopup(data: ModalPopupVO): void {
        this.view.showModalPopup(data);
    }
}