import {ModalPopupVO} from "./events/ModalPopupVO";
import {BaseView} from "../../coreEngine/baseClasses/BaseView";

export class ModalPopupView extends BaseView {
    public showModalPopup(data: ModalPopupVO): void {

        const popup = document.getElementsByClassName('error-popup')[0];
        popup.getElementsByClassName('header')[0].innerHTML = data.header;
        popup.getElementsByClassName('body')[0].innerHTML = data.body;
        popup.classList.add('active');

    }
}