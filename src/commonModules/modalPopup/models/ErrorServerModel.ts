import {IServerModel} from "../../server/interfaces/IServerModel";

export class ErrorServerModel implements IServerModel{

    private _status: string;
    private _code: string;
    private _messageHeader: string;
    private _messageBody: string;

    public parseResponse(data: any): void {
        this._status = data.status;
        this._messageHeader = data.message;
        if (data.errors) {
            this._messageBody = data.errors[0].message;
            this._code = data.errors[0]._code;
        }
    }

    public clear(): void {
        this._status = null;
        this._code = null;
        this._messageBody = null;
        this._messageHeader = null;
    }

    get messageBody(): string {
        return this._messageBody;
    }
    get messageHeader(): string {
        return this._messageHeader;
    }
    get code(): string {
        return this._code;
    }
    get status(): string {
        return this._status;
    }
}