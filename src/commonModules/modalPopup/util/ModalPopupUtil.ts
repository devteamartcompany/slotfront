import {ModalPopupVO} from "../events/ModalPopupVO";

export class ModalPopupUtil {

    public static getModalPopupData(code: string, header?: string, body?: string): ModalPopupVO {
        if (header && body) {
            return {header, body}
        }

        //TODO some data from code errors map
        return {
            header: "",
            body: ""
        }
    }

}