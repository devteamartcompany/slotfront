
import {ErrorServerModel} from "./models/ErrorServerModel";
import {ModalPopupMediator} from "./ModalPopupMediator";
import {ModalPopupView} from "./ModalPopupView";
import {ApplicationModule} from "../../coreEngine/baseClasses/ApplicationModule";
import {addServerModel, bindMediator, createInstance} from "../../coreEngine/exports";

export class ModalPopupModule extends ApplicationModule{
    public execute(): void {
        super.execute();
        addServerModel(createInstance(ErrorServerModel));
        bindMediator(ModalPopupMediator, ModalPopupView, "htmlDummyHolder");
    }
}