import {Howl} from 'howler';

export class Sound extends Howl {

    private _isActive: boolean = false;
    private _isOnPause: boolean = false;
    private _isLoop: boolean = false;
    private _id: string;

    get isLoop(): boolean {
        return this._isLoop;
    }

    set isLoop(value: boolean) {
        this._isLoop = value;
    }

    get isOnPause(): boolean {
        return this._isOnPause;
    }

    set isOnPause(value: boolean) {
        this._isOnPause = value;
    }

    get isActive(): boolean {
        return this._isActive;
    }

    set isActive(value: boolean) {
        this._isActive = value;
    }

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }
}