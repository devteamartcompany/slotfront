import {Sound} from "../Sound";
import {AppLogger} from "../../../coreEngine/general/utils/loger/AppLogger";
import {AppLoggerMessageType} from "../../../coreEngine/general/utils/loger/AppLoggerMessageType";
import {BaseController} from "../../../coreEngine/baseClasses/BaseController";
import {ISoundData} from "../interface/ISoundData";
import {SoundEvents} from "../events/SoundEvents";
import {IAssociative} from "../../../coreEngine/interfaces/IAssociative";
import {EventDispatcher} from "../../../coreEngine/general/EventDispatcher";

export class SoundManager extends BaseController {
    private soundsMap: IAssociative<Sound> = {};
    private allSounds: Sound[] = [];

    protected addListeners() {
        EventDispatcher.getInstance().addListener(SoundEvents.FREEZE_ALL_SOUNDS, this.freezeSounds.bind(this));
        this.dispatcher.addListener(SoundEvents.PLAY_SOUND, this.playSound.bind(this));
        this.dispatcher.addListener(SoundEvents.STOP_SOUND, this.stopSound.bind(this));
        this.dispatcher.addListener(SoundEvents.PAUSE_SOUND, this.pauseSound.bind(this));
        this.dispatcher.addListener(SoundEvents.UNPAUSE_SOUND, this.unPauseSound.bind(this));
        this.dispatcher.addListener(SoundEvents.MUTE_SOUND, this.muteSound.bind(this));
        this.dispatcher.addListener(SoundEvents.UNMUTE_SOUND, this.unMuteSound.bind(this));
        this.dispatcher.addListener(SoundEvents.MUTE_ALL_SOUNDS, this.muteAllSounds.bind(this));
        this.dispatcher.addListener(SoundEvents.MAP_SOUND, this.mapSounds.bind(this));
    }

    private getSoundDuration(id: string): number {
        return this.soundsMap[id].duration();
    }

    protected playSound(data: ISoundData) {
        if (this.soundsMap[data.id]) {
            this.soundsMap[data.id].isLoop = data.loop;
            this.soundsMap[data.id].loop(data.loop);
            this.soundsMap[data.id].isActive = true;
            this.soundsMap[data.id].play();
            this.soundsMap[data.id].on("end", () => {
                if (!data.loop && data.onComplete) {
                    data.onComplete(data.id);
                }
                if (!data.loop) {
                    this.onSoundComplete(data.id);
                    this.soundsMap[data.id].off("end");
                }

            })
        }
    }

    protected stopSound(id: string) {
        if (this.soundsMap[id] && this.soundsMap[id].isActive) {
            this.soundsMap[id].off("end");
            this.soundsMap[id].stop();
            this.soundsMap[id].isActive = false;
        }
    }

    protected pauseSound(id: string) {
        if (this.soundsMap[id] && this.soundsMap[id].isActive && !this.soundsMap[id].isOnPause) {
            this.soundsMap[id].isOnPause = this.soundsMap[id].isActive;
            this.soundsMap[id].pause();
        }
    }

    protected unPauseSound(id: string) {
        if (this.soundsMap[id] && this.soundsMap[id].isOnPause) {
            this.soundsMap[id].isOnPause = !this.soundsMap[id].isActive;
            this.soundsMap[id].play();
        }
    }

    protected muteSound(id: string) {
        if (this.soundsMap[id]) {
            this.soundsMap[id].mute(true);
        }
    }

    protected unMuteSound(id: string) {
        if (this.soundsMap[id]) {
            this.soundsMap[id].mute(false);
        }
    }

    private muteAllSounds(value: boolean) {
        Howler.mute(!value);
    }

    protected mapSounds(sound: Sound) {
        this.soundsMap[sound.id] = sound;
        this.allSounds.push(sound);
    }

    protected onSoundComplete(id: string) {
        this.soundsMap[id].isActive = false;
        AppLogger.log("Sound: " + id + " PLAYING COMPLETE", AppLoggerMessageType.DEFAULT);
    }

    protected freezeSounds(value): void {
        this.muteAllSounds(!value)

    }
}