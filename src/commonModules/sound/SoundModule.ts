import {ApplicationModule} from "../../coreEngine/baseClasses/ApplicationModule";
import {addModel, createInstance} from "../../coreEngine/exports";
import {SoundManager} from "./managers/SoundManager";
import {SoundModel} from "./model/SoundModel";

export class SoundModule extends ApplicationModule{

    public execute(): void {
        super.execute();
        createInstance(SoundManager);
        addModel(createInstance(SoundModel));
    }
}