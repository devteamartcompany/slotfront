import {IBaseModel} from "../../../coreEngine/interfaces/IBaseModel";

export class SoundModel implements IBaseModel {
    private _currentAmbientSound: string;

    get currentAmbientSound(): string {
        return this._currentAmbientSound;
    }

    set currentAmbientSound( value: string ) {
        this._currentAmbientSound = value;
    }
}