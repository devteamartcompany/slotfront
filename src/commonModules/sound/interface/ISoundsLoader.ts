export interface ISoundsLoader {
    groups: {
        initial: ISoundsGroup[], preload: ISoundsGroup[], lazy: ISoundsGroup[]
    }
}

export interface ISoundsGroup {
    id: string,
    url: string,
    type: string
    group: string[]
}