export interface ISoundData {
    id: string;
    loop?: boolean;
    onComplete?: Function;

}