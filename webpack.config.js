const path = require('path');
const CopyPlugin  = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
    mode: "development",
    entry: './src/index.ts',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'bin')
    },
    devServer: {
        host: '0.0.0.0'
    },
    plugins: [
        new CopyPlugin ([
            { from: 'data', to: 'data' },
            { from: 'index.html', to: 'index.html' },
            { from: 'general.css', to: 'general.css' }

        ]),
        new CompressionPlugin({
            compressionOptions: { level: 6 },
        }),
        /*new HtmlWebpackPlugin({
            favicon: "data/favicon/favicon.ico",
            //hash: true,
            title: 'someName',
            filename: 'index.html',
        })*/
    ],
    module: {
        rules: [
            {test: /\.css$/, loader: 'style!css'},
            {
                test: /\.scss$/,
                loaders: ['style-loader', 'raw-loader', 'sass-loader']
            },
            {test: /\.tsx?$/, loader: "ts-loader"},
            {
                enforce: 'pre',
                test: /\.js$/,
                loader: "source-map-loader"
            },
            {
                enforce: 'pre',
                test: /\.tsx?$/,
                use: "source-map-loader"
            },
            {
                test: /\.(png|jpg|json)$/,
                use: "file-loader?name=[path][name].[ext]"
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'file-loader'
                ]
            }
        ]/*,
        loaders: [
            {test: /\.json$/, loader: 'json-loader'},
            {test: /\.css$/, loader: 'style!css'},
            {
                test: /\.scss$/,
                loaders: ['style-loader', 'raw-loader', 'sass-loader']
            },
            {
                test: /\.js$/,
                include: [
                    path.resolve(__dirname, 'src')
                ],
                loader: 'babel-loader',
                query: {
                    presets: [
                        'babel-preset-es2015'
                    ].map(require.resolve),
                }
            }
        ]*/
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js", '.css']
    },
    externals: {
        pixi: "PIXI"
    },

    devtool: 'inline-source-map'
};